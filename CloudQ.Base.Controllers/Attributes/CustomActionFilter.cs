﻿using System;
using System.IO;
using System.Web.Mvc;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Core.Service.Services.Core;
using System.Web.Routing;
using CloudQ.Screte;

namespace CloudQ.Base.Controllers.Attributes
{
    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {

            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var actionName = filterContext.ActionDescriptor.ActionName;
            var ip = filterContext.HttpContext.Request.UserHostAddress;
            var httpSessionStateBase = (((filterContext)).HttpContext).Session;
            //if (actionName != "AboutUs")
            //{
            //    if (httpSessionStateBase != null)
            //    {
            //var user = (CoreUsers)httpSessionStateBase["CurrentUser"];
            //if (user != null)
            //{
            //    var rawUrl = ".." + filterContext.HttpContext.Request.Url.LocalPath;
            //    rawUrl = rawUrl.Replace(".mvc", "");
            //    if (rawUrl.EndsWith("/"))
            //    {
            //        rawUrl = rawUrl.Remove(rawUrl.Length - 1, 1);
            //    }
            //var pcName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            //var checkMenuPermission = _groupRepository.CheckMenuPermission(rawUrl, user);

            //var objAuditTrailService = new AuditTrailService();
            //var objAuditTrail = objAuditTrailService.CreateAuditTrailObjectForBrowsing(user, ip, rawUrl, pcName, actionName);
            //if (checkMenuPermission != null)
            //{
            //    IMenuRepository _menuRepo = new MenuService();
            //    var menuList = _menuRepo.GetHierarckyMenuByMenuId(checkMenuPermission.MenuId);
            //    string breadCumbs = "";
            //    //for (int i = menuList.Count - 1; i > menuList.Count - i; i--)
            //    //{
            //    //    breadCumbs += menuList[i].MenuName + ">>";
            //    //}
            //   menuList= menuList.OrderByDescending(o=>o.SortOrder).ToList();
            //   StringWriter stringWriter = new StringWriter();
            //   HtmlTextWriter htmWriter = new HtmlTextWriter(stringWriter);
            //   foreach (var menu in menuList)
            //   {
            //       htmWriter.AddAttribute(HtmlTextWriterAttribute.Href, menu.MenuPath);
            //       htmWriter.RenderBeginTag(HtmlTextWriterTag.A); // Begin #2
            //       htmWriter.Write(menu.MenuName);
            //       htmWriter.RenderEndTag();
            //       htmWriter.Write(">>");
            //      // breadCumbs += string.Format("<a href='{1}'>{0}</a>&rArr;", menu.MenuName, menu.MenuPath);
            //   }
            //   httpSessionStateBase["breadCumbs"] =  stringWriter.ToString();
            //}


            //if (checkMenuPermission == null)
            //{
            //    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
            //    redirectTargetDictionary.Add("action", "Logoff");
            //    redirectTargetDictionary.Add("controller", "Home");
            //    filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            //    objAuditTrail.AUDIT_STATUS = "Failed";
            //    objAuditTrail.Shortdescription = "No privilege for this users";
            //}
            //var aService = new AuditTrailDataService();
            //aService.SendAudit(objAuditTrail);
            //    }
            //}
            //}
            if(!NugetLib.InitiateNuget())
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                redirectTargetDictionary.Add("action", "Expire");
                redirectTargetDictionary.Add("controller", "Home");
                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);

                this.OnActionExecuting(filterContext);
            }
            
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method,
    Inherited = true, AllowMultiple = true)]
    public class Authorization : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            
        }
    }
}