﻿using System.Collections.Generic;
using System.Reflection;

namespace CloudQ.Base.Controllers.References
{
    public static class ReferencedAssemblies
    {
        //public static Assembly CoreServices
        //{
        //    get { return Assembly.Load("CloudQ.Core.Service"); }
        //}
        //public static Assembly PatientServices
        //{
        //    get { return Assembly.Load("CloudQ.PatientQ.Service"); }
        //}
        public static List<Assembly> ListOfServices
        {
            get
            {
                List<Assembly> list =new List<Assembly>();
                list.Add(Assembly.Load("CloudQ.PatientQ.Service"));
                //list.Add(Assembly.Load("CloudQ.ServiceQ.Service"));
                list.Add(Assembly.Load("CloudQ.Core.Service"));
                //list.Add(Assembly.Load("CloudQ.ProductQ.Service"));

                return list;
            }
        }
        //public static Assembly CoreDataService
        //{
        //    get { return Assembly.Load("CloudQ.Core.DataService"); }
        //}
        //public static Assembly ProductDataService
        //{
        //    get { return Assembly.Load("CloudQ.ProductQ.DataService"); }
        //}
        public static List<Assembly> ListOfDataServices
        {
            get
            {
                List<Assembly> list = new List<Assembly>();
                list.Add(Assembly.Load("CloudQ.Core.DataService"));
                list.Add(Assembly.Load("CloudQ.ProductQ.DataService"));
                list.Add(Assembly.Load("CloudQ.Core.DataService"));
                list.Add(Assembly.Load("CloudQ.ProductQ.DataService"));

                return list;
            }
        }
        //public static Assembly Models
        //{
        //    get
        //    {
        //        return Assembly.Load("CloudQ.Core.Model");
        //    }
        //}
        public static List<Assembly> Models
        {
            get
            {
                List<Assembly> list = new List<Assembly>();
                list.Add(Assembly.Load("CloudQ.Core.Model"));
                list.Add(Assembly.Load("CloudQ.PatientQ.Model"));
                return list;
              //  return Assembly.Load("CloudQ.Core.Model");
            }
        }

        //public static Assembly Domain
        //{
        //    get
        //    {
        //        return Assembly.Load("KiksApp.Core");
        //    }
        //}

        public static Assembly Infrastructure
        {
            get
            {
                return Assembly.Load("CloudQ.Infrastructure");
            }
        }
    }
}
