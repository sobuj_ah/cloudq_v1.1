﻿using System;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CloudQ.Base.Controllers.References;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Base.Controllers.Mapping
{
    public class MappingDefinitions
    {
        public void Initialise()
        {
            _autoRegistrations();
        }

        private void _autoRegistrations()
        {
            var assemblies = ReferencedAssemblies.Models;
            foreach (var assembly in assemblies)
            {
                var dataEntities = assembly.
                 GetTypes().Where(x => typeof(IEntity).IsAssignableFrom(x)).ToList();

                var dtos = assembly.
                    GetTypes().Where(x => x.Name.EndsWith("Dto", true, CultureInfo.InvariantCulture)).ToList();

                foreach (var entity in dataEntities)
                {
                    if (Mapper.GetAllTypeMaps().FirstOrDefault(m => m.DestinationType == entity || m.SourceType == entity) == null)
                    {
                        var matchingDto =
                            dtos.FirstOrDefault(x => x.Name.Replace("Dto", string.Empty).Equals(entity.Name, StringComparison.InvariantCultureIgnoreCase));

                        if (matchingDto != null)
                        {
                            Mapper.CreateMap(entity, matchingDto);
                            Mapper.CreateMap(matchingDto, entity);
                        }
                    }
                }
            }
           

        }

        private void ManualRegistrations()
        {
          


        }
        //public static Type GetModelInstance<T>(T fromData)
        //{

        //    var prop = fromData.GetType();
        //    var dataEntities =
        //        ReferencedAssemblies.Models.
        //            GetTypes().Where(x => typeof(IEntity).IsAssignableFrom(x)).ToList();

           
        //    var entity = dataEntities.FirstOrDefault(s => s.Name.Equals(prop.Name.Replace("Dto", string.Empty)));


        //    Type type = Type.GetType(entity.AssemblyQualifiedName);
              






        //    //foreach (var entity in dataEntities)
        //    //{
        //    //    if (Mapper.GetAllTypeMaps().FirstOrDefault(m => m.DestinationType == entity || m.SourceType == entity) == null)
        //    //    {
        //    //        var matchingDto =
        //    //            dtos.FirstOrDefault(x => x.Name.Replace("Dto", string.Empty).Equals(entity.Name, StringComparison.InvariantCultureIgnoreCase));

        //    //        if (matchingDto != null)
        //    //        {
        //    //            return matchingDto;
        //    //        }
        //    //    }
        //    //}
        //       return type;
        //}

    }
}