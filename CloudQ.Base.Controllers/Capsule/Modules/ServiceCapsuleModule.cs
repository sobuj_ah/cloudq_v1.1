﻿using System.Reflection;
using Autofac;
using CloudQ.Base.Controllers.References;
using CloudQ.Infrastructure.Services;

namespace CloudQ.Base.Controllers.Capsule.Modules
{
    public class ServiceCapsuleModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterAssemblyTypes(ReferencedAssemblies.CoreServices).Where(_ => _.Name.EndsWith("Service"))
            //    .AsImplementedInterfaces().InstancePerLifetimeScope();

            // builder.RegisterGeneric(typeof(BaseService<>)).As(typeof(IService<>)).InstancePerDependency();

            //builder.RegisterAssemblyTypes(ReferencedAssemblies.PatientServices).
            //   Where(_ => _.Name.EndsWith("Service")).
            //   AsImplementedInterfaces().
            //   InstancePerLifetimeScope();
            foreach (Assembly assemly in ReferencedAssemblies.ListOfServices)
            {
                builder.RegisterAssemblyTypes(assemly).
             Where(_ => _.Name.EndsWith("Service")).
             AsImplementedInterfaces().
             InstancePerLifetimeScope();
            }

            builder.RegisterGeneric(typeof(BaseService<>)).As(typeof(IService<>)).InstancePerRequest();
        }

    }
}