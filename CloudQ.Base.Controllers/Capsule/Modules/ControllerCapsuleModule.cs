﻿using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

namespace CloudQ.Base.Controllers.Capsule.Modules
{

    public class ControllerCapsuleModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            // Register the MVC Controllers
            //builder.RegisterControllers(Assembly.Load("KiksApp.Web"));

            // Register the Web API Controllers
            //builder.RegisterApiControllers(Assembly.GetCallingAssembly());
            builder.RegisterApiControllers(Assembly.Load("CloudQ.Web"));
            builder.RegisterControllers(Assembly.Load("CloudQ.Web"));
            builder.RegisterControllers(Assembly.Load("CloudQ.Core.Controllers"));
            builder.RegisterControllers(Assembly.Load("CloudQ.Base.Controllers"));
            builder.RegisterControllers(Assembly.Load("CloudQ.PatientQ.Controllers"));
            //builder.RegisterControllers(Assembly.Load("CloudQ.ServiceQ.Controllers"));


        }
    }
}