﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using CloudQ.Base.Controllers.Capsule.Modules;
using CloudQ.Core.DataService.DB;
using CloudQ.Infrastructure.Context;
using CloudQ.Infrastructure.UOW;
using CloudQ.Logging.Logging;
using CloudQ.PatientQ.DataService.DB;


//[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebCapsule), "Initialise")]
namespace CloudQ.Base.Controllers.Capsule
{
    public class WebCapsule
    {
        public void Initialise(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // builder.RegisterType(typeof(CoreUnitOfWork)).As(typeof(IUnitOfWork)).InstancePerLifetimeScope();


            builder.RegisterFilterProvider();

            const string nameOrConnectionString = "name=SqlConnectionString";
           
            builder.RegisterType<CoreUnitOfWork>()
                .As<ICoreUnitOfWork>()
                .InstancePerDependency();
            builder.RegisterType<PatientQUnitOfWork>()
                .As<IPatientQUnitOfWork>()
                .InstancePerDependency();
            //builder.RegisterType<ServiceQUnitOfWork>()
            //   .As<IServiceUnitOfWork>()
            //   .InstancePerDependency();

            builder.Register(b => NLogLogger.Instance).SingleInstance();

            builder.RegisterModule<RepositoryCapsuleModule>();
            builder.RegisterModule<ServiceCapsuleModule>();
            builder.RegisterModule<ControllerCapsuleModule>();
            //builder.RegisterModule<UnitOfWorkCapsuleModule>();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            var resolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = resolver;
        }
    }
}