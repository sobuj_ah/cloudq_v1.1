﻿using System;
using System.Web.Mvc;
using AutoMapper;
using CloudQ.Base.Controllers.Base;
using CloudQ.Base.Controllers.Mapping;
using CloudQ.Infrastructure.Entity;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;
using Newtonsoft.Json;

namespace CloudQ.Base.Controllers
{
    public class GenericController<T> : BaseController where T : BaseModel
    {
        public IUnitOfWork _unitOfWork = null;

        public GenericController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }
        public ActionResult Save(T formData)
        {
            var oResult = new Result();
            try
            {
                if (!ModelState.IsValid)
                {
                    // return BadRequest(ModelState);
                }
                IService<T> service = new BaseService<T>(_unitOfWork);
                if (formData.Id == Guid.Empty)
                {
                    service.Save(formData);
                }
                else
                {
                    service.Update(formData);
                }

                oResult.Operation = Operation.Success;
            }
            catch (Exception ex)
            {
                oResult.Status = false;
                oResult.Operation = Operation.Failed;
                oResult.Message = ex.Message;
            }

            return Json(oResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(T formData)
        {
            var oResult = new Result();
            try
            {
                if (!ModelState.IsValid)
                {
                    // return BadRequest(ModelState);
                }
                IService<T> service = new BaseService<T>(_unitOfWork);
                var model = service.GetById(formData.Id);
                if (model != null)
                {
                    service.Delete(model);
                }
                oResult.Operation = Operation.Success;
            }
            catch (Exception ex)
            {
                oResult.Status = false;
                oResult.Operation = Operation.Failed;
                oResult.Message = ex.Message;
            }

            return Json(oResult, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DataSource(GridOptions options)
        {
            IService<T> service = new BaseService<T>(_unitOfWork);
            var result = service.DataSource(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
