﻿
var DesignationManager = {
    init: function () {
        DesignationManager.designationGrid();
        _km.initWindow('wndDesignation', 'Customer Information Form');
        var wnd = $("#wndDesignation").data('kendoWindow');

        $("#btnSaveChangesDesignation").click(function () {
            if (DesignationManager.isValidation()) {
                $('#divDesignation').saveTo("../Designation/SaveDesignation", function (response) {
                    showSuccessMessage(response.Message);
                    if (response.Status) {

                        $("#gridDesignation").data('kendoGrid').dataSource.read();
                    }
                });
            }
        });

        $("#btnAddDesignation").click(function () {

            wnd.open().center();

        });

        $("#btnCloseWindowDesignation").click(function () {
            wnd.close();
        });
        $("#btnClearDesignation").click(function () {
            _ah.clearAllFields();
        });

    },
    designationGrid: function () {

        _km.initGrid_DS("gridDesignation", DesignationManager.GenerateDesignationColumns(), '../Designation/DesignationGridDataSource');
    },
    GenerateDesignationColumns: function () {
        return columns = [
        { field: "DesignationName", title: "Designation Name", width: 100 },
        { field: "IsActive", title: "IsActive", width: 100 },
            { field: "Edit", title: "Edit Designation", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="DesignationManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridDesignation").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);
            var wnd = $("#wndDesignation").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divDesignation", selectedItem);
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divDesignation")) {
            return true;
        }
        return false;
    }
}