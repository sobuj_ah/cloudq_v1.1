﻿
var wnd;

var companyManager = {

    init: function () {

        companyManager.companyGrid();
        
        _km.initWindow('wndCompanyInfo', 'Company Information');
        wnd = $("#wndCompanyInfo").data('kendoWindow');
        
        $("#btnSaveChanges").click(function () {
            if (companyManager.isValidation()) {
                $('#divCompany').saveTo("../Company/SaveCompany", function (response) {
                    if (response.Status) {
                        swal({
                            title: "Saved",
                            text: "Company is saved successfully",
                            type: "success",
                            timer: 1000,
                            showConfirmButton: false
                        });

                        $("#gridCompany").data('kendoGrid').dataSource.read();
                        wnd.close();
                    }
                });
            }
          
        });

        $("#btnAddNewCompany").click(function() {
            wnd.open().center();
            _ah.generateAutoIncrementCode("txtCompanyCode", "COM", "CompanyCode", "CQM.OrgCompany");
        });

        $("#btnPrintCompanySummary").click(function() {
            
            var url = "../Reports/CompanyInfoPrint";
            var param = "" + JSON.stringify();
            reportViewer(url, param);

        });


        

        $("#btnCloseWindow").click(function() {
            wnd.close();
        });

        $("#btnClearFields").click(function() {
            _ah.clearAllFields();
        });

    },

 
    companyGrid: function() {

        _km.initGrid_DS("gridCompany", companyManager.GenerateCompanyColumns(), '../Company/CompanyGridDataSource');
    },
    
    GenerateCompanyColumns: function () {
        
        return columns = [
            
            { field: "CompanyId", hidden: true },
            { field: "CompanyCode", title: "Company Code", width: 100 },
            { field: "CompanyName", title: "Company", width: 100 },
            { field: "Email", title: "Email", width: 100 },
            { field: "Phone", title: "Phone", width: 100 },
            { field: "OwnerName", title: "Owner Name", width: 100 },
            { field: "OwnerAddress", title: "Address", width: 100 },
            { field: "Edit", title: "Edit Company", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="companyManager.clickEventForEditButton()"/>', sortable: false }
        ];
    },
    
    clickEventForEditButton: function () {
        
        var grid = $("#gridCompany").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        
        if (selectedItem != null) {
            _ah.populate("#divCompany", selectedItem);
            wnd.open().center();
        }
    },
    
    isValidation: function () {
        
        if (_km.kendoValidator("divCompany")) {
            return true;
        }
        return false;
    }

};