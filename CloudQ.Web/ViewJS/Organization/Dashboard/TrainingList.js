﻿var TrainingListManager = {


    trainingBookingDataSource: function () {
        var gridDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,

            serverSorting: false,

            serverFiltering: false,

            allowUnsort: true,

            pageSize: 10,

            transport: {
                read: {
                    url: '../TrainingBooking/GetTrainingScheduleForOpenApply/',

                    type: "POST",

                    dataType: "json",

                    contentType: "application/json; charset=utf-8"
                },


                parameterMap: function (options) {

                    return JSON.stringify(options);

                }
            },
            schema: { data: "Items", total: "TotalCount" }
        });
        return gridDataSource;
    },

    ApplyForTrainingRecomendation: function (trainingScheduleId, trainingId) {
        Message.Confirm("Do you want to apply for this training?", function () {
            var jsonParam = 'trainingScheduleId:' + trainingScheduleId + ',trainingId:' + trainingId;
            var serviceUrl = "../TrainingBooking/ApplyForTrainingRecomendation/";
            AjaxManager.SendJson2(serviceUrl, jsonParam, onSuccess, onFailed);

        }, function () {
            return false;
        });


        function onSuccess(jsonData) {
            if (jsonData == "Success") {

                AjaxManager.MsgBox('success', 'center', 'Success', 'Applied for Recommendation Successfully',
                    [{
                        addClass: 'btn btn-primary',
                        text: 'Ok',
                        onClick: function ($noty) {
                            $noty.close();
                            $("#divTrainingGrid").data('kendoGrid').dataSource.read();
                        }
                    }]);

            } else {
                AjaxManager.MsgBox('warning', 'center', 'Warning', 'Warning:' + jsonData,
                    [{
                        addClass: 'btn btn-primary',
                        text: 'Ok',
                        onClick: function ($noty) {
                            $noty.close();
                        }
                    }]);
            }
        }

        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                [{
                    addClass: 'btn btn-primary',
                    text: 'Ok',
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }]);

        }


    },

};


var TrainingListHelper = {

    init:function() {
        TrainingScheduleViewManager.init();
        TrainingListHelper.GenerateTrainingBookingGrid();
    },

    GenerateTrainingBookingGrid: function () {
        $("#divTrainingGrid").kendoGrid({
            dataSource: TrainingListManager.trainingBookingDataSource(),
            pageable: {
                refresh: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            filterable: false,
            sortable: true,
            columns: TrainingListHelper.trainingBookingColumns(),
            editable: false,
            navigatable: true,
            selectable: "row",
        });
    },

    trainingBookingColumns: function () {
        return columns = [
            { field: "TrainingInfo.TrainingName", title: "Training  Title", width: 100 },
            { field: "TrainingInfo.LearnObjective", title: "Learning  Objective", width: 120, hidden:true, headerAttributes: { style: "white-space: normal;word-wrap: break-word;" } },
            { field: "Status", title: "Status", width: 30, hidden: true },
            { field: "TrainingSchedule.Place", title: "Venue", width: 30, hidden: false },
            { field: "FeedbackId", title: "FeedbackId", width: 10, hidden: true },
            { field: "FeedbackTitle", title: "FeedbackTitle", width: 10, hidden: true },
            { field: "TrainingInfo.Duration", title: "Duration", width: 30, template: "#=TrainingInfo.Duration# (hours)" },
            { field: "TrainingSchedule.StartDate", title: "Start Date", width: 40, template: "#=kendo.toString(kendo.parseDate(TrainingSchedule.StartDate),'dd-MMM-yyyy')#" },
            { field: "TrainingSchedule.EndDate", title: "End Date", width: 40, template: "#=kendo.toString(kendo.parseDate(TrainingSchedule.EndDate),'dd-MMM-yyyy')#" },
            { field: "Action", title: "Status", filterable: false, width: 45, template: '#=TrainingListHelper.templateApplyButton(data)#', sortable: false },
            { field: "Action", title: "View Details", filterable: false, width: 40, template: '<input type="button" class="k-button" value="View Details" onClick="TrainingListHelper.ViewTraining()"  />', sortable: false }
        ];

    },

    templateApplyButton: function (dataItem) {
       
        if (dataItem.Status == -3) {
            return '<input type="button" class="k-button" value="Apply" onClick="TrainingListHelper.ApplyForTrainigRecomendation(' + dataItem.FeedbackId + ')"  />';
        }
        else if (dataItem.Status == -2) {
            return '<span>Waiting For Recomendation</span>';
        }
        else if (dataItem.Status == -1) {
            return '<span>Recomended</span>';
        }
        else {
            return '<span class="k-icon k-cancel"></span>Canceled';
        }
    },

    ViewTraining: function () {
        var grid = $("#divTrainingGrid").data().kendoGrid;
        var selectItem = grid.dataItem(grid.select());
        if (selectItem != null) {
            var scheduleId = selectItem.TrainingScheduleId;
            var date = kendo.parseDate(selectItem.TrainingSchedule.StartDate);
            TrainingScheduleViewManager.SetSchedulerEventData(scheduleId, date);
        }
    },

    ApplyForTrainigRecomendation: function () {

        //Message.Confirm("Do you want to apply for the trainig?", function () {
            var grid = $("#divTrainingGrid").data().kendoGrid;
            var selectItem = grid.dataItem(grid.select());
            if (selectItem != null) {
                var scheduleId = selectItem.TrainingScheduleId;
                var trainingId = selectItem.TrainingSchedule.TrainingInfoId;
                TrainingListManager.ApplyForTrainingRecomendation(scheduleId, trainingId);
            }

        //}, function () {
        //    return false;
        //});
    },

};

