﻿ 
var BankBranchManager = {
    init: function () {
        BankBranchManager.BankBranchGrid();
        _ah.GenerateCommonCombo("cmbBank", "BankId", "BankName", "BankInfo", "");

        _km.initWindow('wndBankBranch', 'Client Information Form');

        $("#btnSaveChangesBankBranch").click(function () {
            if (BankBranchManager.isValidation()) {
                $('#divBankBranch').saveTo("../BankBranch/SaveBankBranch", function (response) {
                    showSuccessMessage(response.Message);
                    if (response.Status) {

                        $("#gridBankBranch").data('kendoGrid').dataSource.read();
                    }
                });
            }
        });

        $("#btnAddBankBranch").click(function () {
            var wnd = $("#wndBankBranch").data('kendoWindow');

            wnd.open().center();

        });

        $("#btnCloseWindowBankBranch").click(function () {
            var wnd = $("#wndBankBranch").data('kendoWindow');

            wnd.close();
        });

        $("#btnClearBankBranch").click(function () {
            _ah.clearAllFields();
        });

    },
    BankBranchGrid: function () {

        _km.initGrid_DS("gridBankBranch", BankBranchManager.GenerateBankBranchColumns(), '../BankBranch/BankBranchGridDataSource');
    },
    GenerateBankBranchColumns: function () {
        return columns = [
            { field: "BankBranchId", hidden: true },
            { field: "Bank.BankName", title: "Bank Name", width: 100 },
            { field: "BranchName", title: "Branch Name", width: 100 },
            { field: "BranceAddress", title: "Branch Address", width: 100 },
            { field: "PhoneNo", title: "Phone No", width: 100 },
            { field: "Edit", title: "Edit", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="BankBranchManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridBankBranch").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {

            var wnd = $("#wndBankBranch").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divBankBranch", selectedItem);
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divBankBranch")) {
            return true;
        }
        return false;
    }
}