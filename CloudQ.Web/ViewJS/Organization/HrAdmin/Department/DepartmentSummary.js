﻿

var departmentSummaryManager = {
    
    GenerateDepartmentGridSummary: function () {
        var url = "../Department/GetDepartmentSummaryAll/";
        var column = departmentSummaryHelper.GenerateDepartmentColumns();
        empressCommonManager.GenerateCommonGridWithPaging("gridDepartment", url, column, 20);
    }
};

var departmentSummaryHelper = {

    GenerateDepartmentColumns: function () {
        return columns = [
            { field: "DepartmentCode", title: "Department Code", width: 100 },
            { field: "DepartmentName", title: "Department Name", width: 200 },
            { field: "DepartmentId", hidden: true },
            { field: "Edit", title: "Edit", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="departmentSummaryHelper.clickEventForEditButton()"  />', sortable: false }
        ];
    },

    clickEventForEditDepartment: function () {
        $('#gridDepartment table tr').live('dblclick', function () {
            var entityGrid = $("#gridDepartment").data("kendoGrid");

            var selectedItem = entityGrid.dataItem(entityGrid.select());

            departmentDetailsHelper.populateDepartmentDetails(selectedItem);

        });
    },

    clickEventForEditButton: function () {
        
        var entityGrid = $("#gridDepartment").data("kendoGrid");

        var selectedItem = entityGrid.dataItem(entityGrid.select());

        departmentDetailsHelper.populateDepartmentDetails(selectedItem);
    },


};