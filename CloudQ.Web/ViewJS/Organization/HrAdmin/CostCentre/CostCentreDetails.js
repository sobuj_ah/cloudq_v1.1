﻿
var CostCentreManager = {
    SaveCostCentre: function () {

        var validator = $("#CCostCentreDetailsDiv").kendoValidator().data("kendoValidator"),
           status = $(".status");
        
        if (validator.validate()) {
            var objCostCentre = CostCentreHelper.CreateCostCentreObject();
            var objobjCostCentreinfo = JSON.stringify(objCostCentre).replace(/&/g, "^");

            var jsonParam = "strCostCentre=" + objobjCostCentreinfo;
            var serviceUrl = "../CostCentre/SaveAndUpdateCostCentre/";
            AjaxManager.SendJson(serviceUrl, jsonParam, onSuccess, onFailed);
        }


        function onSuccess(jsonData) {
            if (jsonData == "Success") {

                AjaxManager.MsgBox('success', 'center', 'Success:', 'Cost Centre Saved Successfully',
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            CostCentreHelper.populateParentCostCentreCombo();
                            $("#gridCostCentre").data("kendoGrid").dataSource.read();
                            CostCentreHelper.clearCostCentre();
                        }
                    }]);
            }
            else if (jsonData == "Exist") {

                AjaxManager.MsgBox('warning', 'center', 'Alresady Exist:', 'Cost Centre Name Already Exist !!!',
                      [{
                          addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                              $noty.close();
                          }
                      }]);
            }
            else {
                AjaxManager.MsgBox('error', 'center', 'Error', jsonData,
                        [{
                            addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                                $noty.close();
                            }
                        }]);
            }
        }

        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Error', error.statusText,
                        [{
                            addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                                $noty.close();
                            }
                        }]);
        }
    },

    GenerateParentCostCentreCombobyCompanyId: function () {

        var objparentCostCentre = "";
        var jsonParam = "";
        var serviceUrl = "../CostCentre/GetCostCentreDataForComboByCompanyId/";
        AjaxManager.GetJsonResult(serviceUrl, jsonParam, false, false, onSuccess, onFailed);
        function onSuccess(jsonData) {

            objparentCostCentre = jsonData;
        }
        function onFailed() {
            window.alert(error.statusText);
        }

        return objparentCostCentre;
    }

};

var CostCentreHelper = {


    initiateCostCentre: function () {

        //empressCommonHelper.GenerareHierarchyCompanyCombo("cmbCompanyNameDetails");

        //if (CurrentUser.CompanyId != null) {
        //    var companyData = $("#cmbCompanyNameDetails").data("kendoComboBox");
        //    companyData.value(CurrentUser.CompanyId);
        //    CostCentreHelper.changeCompanyName();
        //    // CostCentreHelper.populateParentCostCentreCombo();
        //}


    },


    //changeCompanyName: function () {

    //    var comboboxbranch = $("#cmbBranchDetails").data("kendoComboBox");
    //    var comboboxDep = $("#cmbDepartmentNameDetails").data("kendoComboBox");



    //    var companyData = $("#cmbCompanyNameDetails").data("kendoComboBox");
    //    var companyId = companyData.value();
    //    var companyName = companyData.text();

    //    if (companyId == companyName) {
    //        companyData.value('');
    //        comboboxbranch.value('');
    //        comboboxbranch.destroy();

    //        //comboboxDep.value('');
    //        //comboboxDep.destroy();


    //        empressCommonHelper.GenerateBranchCombo(0, "cmbBranchDetails");
    //        empressCommonHelper.GetDepartmentByCompanyId(0, "cmbDepartmentNameDetails");

    //        return false;
    //    }
    //    if (comboboxbranch != undefined) {
    //        comboboxbranch.value('');
    //        comboboxbranch.destroy();
    //    }
    //    if (comboboxDep != undefined) {
    //        comboboxDep.value('');
    //        comboboxDep.destroy();
    //    }


    //    empressCommonHelper.GenerateBranchCombo(companyId, "cmbBranchDetails");

    //    if (CurrentUser != null) {
    //        comboboxbranch = $("#cmbBranchDetails").data("kendoComboBox");
    //        comboboxbranch.value(CurrentUser.BranchId);
    //        CostCentreHelper.changeBranchName();
    //    }

    //    empressCommonHelper.GetDepartmentByCompanyId(companyId, "cmbDepartmentNameDetails");


    //},

    //changeBranchName: function () {

    //    var comboboxbranch = $("#cmbBranchDetails").data("kendoComboBox");
    //    var comboboxDep = $("#cmbDepartmentNameDetails").data("kendoComboBox");


    //    var companyData = $("#cmbCompanyNameDetails").data("kendoComboBox");
    //    var companyId = companyData.value();


    //    var branchId = comboboxbranch.value();
    //    var branchName = comboboxbranch.text();
    //    if (branchId == branchName) {
    //        comboboxbranch.value('');
    //        //comboboxDep.value('');
    //        //comboboxDep.destroy();


    //        empressCommonHelper.GetDepartmentByCompanyId(companyId, "cmbDepartmentNameDetails");

    //        return false;
    //    }

    //    if (comboboxDep != undefined) {
    //        comboboxDep.value('');
    //        comboboxDep.destroy();
    //    }


    //    empressCommonHelper.GetDepartmentByCompanyId(companyId, "cmbDepartmentNameDetails");




    //},

    //changeDepartmentName: function () {

    //    var companyData = $("#cmbCompanyNameDetails").data("kendoComboBox");
    //    var companyId = companyData.value();
    //    var companyName = companyData.text();

    //    var comboboxbranch = $("#cmbBranchDetails").data("kendoComboBox");
    //    var branchId = comboboxbranch.value();
    //    var branchName = comboboxbranch.text();

    //    var comboboxDep = $("#cmbDepartmentNameDetails").data("kendoComboBox");
    //    var departmentId = comboboxDep.value();
    //    var departmentName = comboboxDep.text();


    //},

    populateParentCostCentreCombo: function () {

        var objCostCentre = new Object();
        //var companyData = $("#cmbCompanyNameDetails").data("kendoComboBox");
        //var companyId = companyData.value();

        //objCostCentre = CostCentreManager.GenerateParentCostCentreCombobyCompanyId(companyId);
        objCostCentre = CostCentreManager.GenerateParentCostCentreCombobyCompanyId();
        $("#cmbParentCostCentre").kendoComboBox({
            placeholder: "Select Parent Cost Centre Name",
            dataTextField: "CostCentreName",
            dataValueField: "CostCentreId",
            dataSource: objCostCentre,

        });

    },

    //IsCostCentreCheckEvent: function () {

    //    $('#chkIsCostCentreCompany').live('click', function (e) {

    //        var companyData = $("#cmbCompanyNameDetails").data("kendoComboBox");
    //        var companyName = companyData.text();
    //        var $cb = $(this);
    //        if ($cb.is(":checked")) {
    //            $("#txtCostCentreName").val(companyName);
    //            $('#chkIsCostCentreBranch').removeAttr('checked');
    //            $('#chkIsCostCentreDepartment').removeAttr('checked');
    //        } else {
    //            $("#txtCostCentreName").val("");
    //        }

    //    });

    //    $('#chkIsCostCentreBranch').live('click', function (e) {

    //        var companyData = $("#cmbBranchDetails").data("kendoComboBox");
    //        var companyName = companyData.text();
    //        var $cb = $(this);
    //        if ($cb.is(":checked")) {
    //            $("#txtCostCentreName").val(companyName);
    //            $('#chkIsCostCentreCompany').removeAttr('checked');
    //            $('#chkIsCostCentreDepartment').removeAttr('checked');
    //        } else {
    //            $("#txtCostCentreName").val("");
    //        }

    //    });

    //    $('#chkIsCostCentreDepartment').live('click', function (e) {

    //        var companyData = $("#cmbDepartmentNameDetails").data("kendoComboBox");
    //        var companyName = companyData.text();
    //        var $cb = $(this);
    //        if ($cb.is(":checked")) {
    //            $("#txtCostCentreName").val(companyName);
    //            $('#chkIsCostCentreCompany').removeAttr('checked');
    //            $('#chkIsCostCentreBranch').removeAttr('checked');
    //        } else {
    //            $("#txtCostCentreName").val("");
    //        }

    //    });

    //},

    clearCostCentre: function () {
        $("#btnSave").text("Save");
        
        $("#hdnCostCentreId").val(0);

        $("#txtCostCentreName").val("");

        $("#txtCostCentreCode").val("");
        $("#txtCrossCentreDescription").val("");

        $('input[type = "checkbox"]').attr('checked', false);
        


        $("#CCostCentreDetailsDiv > form").kendoValidator();
        $("#CCostCentreDetailsDiv").find("span.k-tooltip-validation").hide();

        var status = $(".status");
        status.text("").removeClass("invalid");

    },

    ValidateCostCentreInfoForm: function () {
        var data = [];

        var validator = $("#CCostCentreDetailsDiv").kendoValidator().data("kendoValidator"),
            status = $(".status");
        if (validator.validate()) {
            status.text("").addClass("valid");
            return true;

        } else {
            status.text("Oops! There is invalid data in the form.").addClass("invalid");
            return false;
        }
    },



    CreateCostCentreObject: function () {
        var objDataSource = new Object();
        objDataSource.CostCentreId = $("#hdnCostCentreId").val();
        objDataSource.CostCentreName = $("#txtCostCentreName").val();
        objDataSource.CostCentreCode = $("#txtCostCentreCode").val();
        var parentCostCentreId = $("#cmbParentCostCentre").val();
        objDataSource.ParentCostCentreId = parentCostCentreId == "" ? 0 : parentCostCentreId;
        objDataSource.CcDescription = $("#txtCrossCentreDescription").val();
        objDataSource.IsActive = $("#chkIsActive").is(":checked") == true ? 1 : 0;
        return objDataSource;
    },

    FillCostCentreDetailsForm: function (selectedItem) {
        CostCentreHelper.clearCostCentre();
        $("#btnSave").text("Update");
        
        $("#CostcentreDetailsPopupWindow").data("kendoWindow").open().center();
        $("#hdnCostCentreId").val(selectedItem.CostCentreId);
        $("#txtCostCentreName").val(selectedItem.CostCentreName);
        $("#txtCostCentreCode").val(selectedItem.CostCentreCode);
        if (selectedItem.ParentCostCentreId != 0) {
            var parentCostCentreId = $("#cmbParentCostCentre").data("kendoComboBox");
            parentCostCentreId.value(selectedItem.ParentCostCentreId);
        }

        if (selectedItem.IsActive == 1) {
            $("#chkIsActive").prop('checked', 'checked');
        } else {
            $("#chkIsActive").removeProp('checked', 'checked');
        }

        $("#txtCrossCentreDescription").val(selectedItem.CcDescription);

        //CostCentreSummaryHelper.hideFields();
    },

};