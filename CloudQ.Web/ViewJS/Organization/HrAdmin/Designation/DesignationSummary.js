﻿
var designationSummaryManager = {
    
    GenerateDesignationGridSummary: function () {
        var url = "../Designation/GetDesignationSummaryAll/";
        var column = designationSummaryHelper.GeneratedDesignationColumns();
        empressCommonManager.GenerateCommonGridWithPaging("gridDesignationSummary", url, column, 20);
    },
    
};

var designationSummaryHelper = {
    
    GeneratedDesignationColumns: function () {
        return columns = [
        { filed: "DesignationId", title: "DesignationId", width: 50, hidden: true },
        { filed: "ParentDesignationId", title: "ParentDesignationId", width: 50, hidden: true },
        { field: "DesignationCode", title: "Designation Code", width: 120, sortable: true },
        { field: "DesignationName", title: "Designation Name", width: 200, sortable: true },
        { field: "DSortOrder", title: "Sequence", width: 100, sortable: true },
        { field: "Status", title: "Status", width: 100, sortable: true, template: "#= (Status==1) ? 'Active' : 'Inactive' #" },
        { field: "Edit", title: "Edit", filterable: false, width: 70, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="designationSummaryHelper.clickEventForEditButton()"  />', sortable:false }
        ];
    },
    
    clickEventForEditButton:function () {
        var entityGrid = $("#gridDesignationSummary").data("kendoGrid");

        var selectedItem = entityGrid.dataItem(entityGrid.select());
        
        if (selectedItem != null) {
            designationDetailsHelper.populateDesignationDetails(selectedItem);
        }
    }
};