﻿var wnd;
var BankInfoManager = {
    init: function () {
        BankInfoManager.BankInfoGrid();
        _km.initWindow('wndBankInfo', 'Client Information Form');
        wnd = $("#wndBankInfo").data('kendoWindow');
      
        $("#btnSaveChangesBank").click(function () {
            if (BankInfoManager.isValidation()) {
                $('#divBankInfo').saveTo("../BankInfo/SaveBankInfo", function(response) {
                    showSuccessMessage(response.Message);
                    if (response.Status) {

                        $("#gridBankInfo").data('kendoGrid').dataSource.read();
                    }
                });
            }
        });

        $("#btnAddBankInfo").click(function () {

            wnd.open().center();
            _ah.generateAutoIncrementCode("txtBrandCode", "BC", "BrandCode", "Brand");

        });

        $("#btnCloseWindowBank").click(function () {
            wnd.close();
        });

        $("#btnClearBank").click(function () {
            _ah.clearAllFields();
        });

    },
    BankInfoGrid: function () {

        _km.initGrid_DS("gridBankInfo", BankInfoManager.GenerateBankInfoColumns(), '../BankInfo/BankInfoGridDataSource');
    },
    GenerateBankInfoColumns: function () {
        return columns = [
            { field: "BankId", hidden: true },
            { field: "BankName", title: "Bank Name", width: 100 },
            { field: "BankAddress", title: "Bank Address", width: 100 },
            { field: "PhoneNo", title: "Phone No", width: 100 },
            { field: "Edit", title: "Edit", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="BankInfoManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridBankInfo").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {

            var wnd = $("#wndBankInfo").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divBankInfo", selectedItem);
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divBankInfo")) {
            return true;
        }
        return false;
    }
}