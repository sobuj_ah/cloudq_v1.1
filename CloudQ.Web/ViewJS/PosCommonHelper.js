﻿ var _pos = {
    productCombo: function (identity) {

        var obj = getData("../ProductInfo/GetProductCombo", "");
        $("#" + identity).kendoComboBox({
            placeholder: "Select",
            dataTextField: "ProductName",
            dataValueField: "ProductId",
            dataSource: obj
        });
    },
    GetProductInfoByBarcode: function (barcode) {
        var data = getData('../ProductSales/GetProductByBarcode', 'barcode:' +JSON.stringify(barcode));
        return data;
    },
    barcodeAutoComplete:function(ctrl) {
        _km.autoComplete(ctrl, "Barcode", "../ProductSales/GetProductByBarcodeAutoComplete", "");
    }
    

}