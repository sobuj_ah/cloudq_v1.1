﻿$(document).ready(function () {

    var data = _navHelper.getMenuList();
    _navHelper.setMenus(data);

    //var template = kendo.template($("#tmplNavigationMenu").html());
    //var previewHtml = template(data);
    //$("#NavigateMenu").html(previewHtml);
    $.AdminSwift.leftSideBar.activate();
});

var _navHelper = {
    getMenuList: function () {
        return getData('../Menu/GetMenuByUserPermission', '');
    },
    getMenuListByMenuId: function (menuId) {
        return getData('../Menu/GetMenuByParentId', 'menuId:' + menuId);
    },
    setMenus: function (menus) {
        

        var ParentIdbyMenuId = [];
        var pathName = window.location.pathname;
        ParentIdbyMenuId = _navHelper.getMenuListByMenuId(1);//This parent menu is to make active css to parent menu
        //for (var j = 0; j < menus.length; j++) {
        //    if (menus[j].MenuPath == ".." + pathName) {
        //        ParentIdbyMenuId = _navHelper.getMenuListByMenuId(menus[j].ParentId);//This parent menu is to make active css to parent menu
        //    }
        //}
        var menulink = "<div class='menu'>";
        menulink += " <ul class='list'>";
        menulink += " <li class='header'>MAIN NAVIGATION</li>";
        menulink += " <li class='active open'><a href='../Home/index'><i class='zmdi zmdi-home'></i><span>Dashboard</span></a></li>";

        for (var i = 0; i < menus.length; i++) {
            var haveParentId = 0;
            if (menus[i].ParentId == null || menus[i].ParentId == 0) {
                for (var k = 0; k < ParentIdbyMenuId.length; k++) {
                    if (ParentIdbyMenuId[k].MenuId == menus[i].MenuId) {
                        haveParentId = 1;
                    }
                }
                if (haveParentId == 1) {
                    //menulink += menus[i].MenuName;
                    menulink += "<li><a href='javascript:void(0);' class='menu-toggle'><i class='zmdi'></i><span>" + menus[i].MenuName + "</span> </a>";
                    menulink += _navHelper.addchiledMenu(menus[i], menus[i].MenuId, menus, ParentIdbyMenuId);

                }
                else {

                    menulink += "<li  class='header'><a href='" + menus[i].MenuPath + "'>" + menus[i].MenuName + "</a>";
                }


                menulink += "</li>";


            }
        }
        menulink += "</ul></div>";

        $("#NavigateMenu").html(menulink);
        //$.AdminSwift.leftSideBar.activate();

    },
    addchiledMenu: function (objMenuOrginal, menuId, objMenuList, parentIdbyMenuId) {


        var menulink = "<ul  class='ml-menu' >";
        var added = false;
        var haveChildsParentId = 0;
        for (var j = 0; j < objMenuList.length; j++) {
            if (objMenuList[j].ParentId == menuId) {
                menulink += "<li id='" + objMenuList[j].MenuId + "' >";
                if (objMenuList[j].MenuPath == null || objMenuList[j].MenuPath == "") {

                    menulink += "<a href='#'>" + objMenuList[j].MenuName + "</a>";
                }
                else {
                    menulink += "<a href='" + objMenuList[j].MenuPath + "'>" + objMenuList[j].MenuName + "</a>";
                }
                menulink += _navHelper.addchiledMenu(objMenuList[j], objMenuList[j].MenuId, objMenuList);
                menulink += "</li>";
                added = true;
            }
        }
        menulink += "</ul>";
        if (added == false) {
            menulink = "";
        }

        return menulink;
    }



};