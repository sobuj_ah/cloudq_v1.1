﻿var _codeGen = {
    verificationCode: function (mobileNo) {
        sendData('../VarificationCodeGen/GenerateCode', 'mobileNo=' + mobileNo, function (response) {
            swal({
                title: "Verification Code",
                text: "Verification code sent to you mobile no.!",
                type: "success",
                timer: 1000,
                showConfirmButton: false
            });
            return response;

        });
    }


}