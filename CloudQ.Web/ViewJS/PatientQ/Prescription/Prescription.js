﻿var patient = new Object();

var prescriptionHelper = {
    setPationInfo: function (obj) {
        patient = obj;
        $("#lblPatientNo").html(obj.PatientNo);
        $("#lblPatientName").html(obj.PatientName);

    },
    init: function () {

        _km.ComboBox('cmbFormulation', 'FormulationId', 'Formulation', '../Medicine/GetFormulation/');
        _km.ComboBox('cmbGeneric', 'GenericId', 'GenericName', '../Medicine/GetGeneric/');
        _km.ComboBox('cmbCategory', 'CategoryId', 'CategoryName', '../Medicine/GetCategory/');

        prescriptionHelper.initSysmtoms();
        prescriptionHelper.initMedicineGrid();
        prescriptionHelper.initPrescriptionGrid();

        _km.initWindow('windMedicineSearch', 'Select Medicine', false);

        $("#cmbFormulation").change(function () {

            prescriptionHelper.medicineFilter();


        });

        $("#cmbGeneric").change(function () {

            prescriptionHelper.medicineFilter();
        });

        $("#cmbCategory").change(function () {

            prescriptionHelper.medicineFilter();

        });

        $("#btnClose").click(function () {
            $("#windMedicineSearch").data('kendoWindow').close();
        });

        $("#btnSaveChanges").click(function () {
            prescriptionHelper.SavePrecscription();
        });
        $("#txtSearchMedicine").keyup(function () {
            prescriptionHelper.medicineFilter();
        });
    },

    initSysmtoms: function () {


        $("#msSymptom").kendoMultiSelect({
            placeholder: "Select Symptoms...",
            dataTextField: "SymptomTitle",
            dataValueField: "SymptomId",
            autoBind: false,
            ignoreCase: false,
            filter: "startwith",
            dataSource: {
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url: "../Medicine/GetSymptoms",

                        type: "POST",

                        dataType: "json",

                        async: false,

                        cache: true,

                        contentType: "application/json; charset=utf-8"
                    }
                }
            },
            change: function () {
                prescriptionHelper.medicineFilter();
            }

        });
    },
    medicineFilter: function () {
        var data = $("#msSymptom").data('kendoMultiSelect').value();
        var catId = $("#cmbCategory").data('kendoComboBox').value();
        var formsId = $("#cmbFormulation").data('kendoComboBox').value();
        var genId = $("#cmbGeneric").data('kendoComboBox').value();
        if (catId == "")
            catId = 0;
        if (formsId == "")
            formsId = 0;
        if (genId == "")
            genId = 0;

        var searchKey = $("#txtSearchMedicine").val();


        var grid = $("#gridMedicine").data('kendoGrid');
        var ds = _km.gridDataSource('../Medicine/GetMedicine/?symptomIds=' + JSON.stringify(data) + '&formulationId=' + formsId + '&genericeId=' + genId + '&categoryId=' + catId + '&searchKey=' + searchKey);
        grid.setDataSource(ds);

    },

    SavePrecscription: function () {
        //var symptoms = $("#msSymptom").data('kendoMultiSelect').dataItems();
        debugger;

        var oPres = new Object();
        oPres.PrescriptionId = 0;
        oPres.PatientId = patient.PatientId;
        oPres.DoctorId = patient.DoctorId;
        oPres.AppointmentId = patient.AppointmentId;
        oPres.PrescribeMedicines = prescriptionHelper.getPrescribeMedicine();
        oPres.PrescribeSymptoms = $("#msSymptom").data('kendoMultiSelect').value();
        //oPres.PrescribeDieases = [];
        //oPres.PrescribeTests = [];

        sendObject('../Presscription/SavePrescription', 'prescription:' + JSON.stringify(oPres), function (response) {
            // alert('Prescription Submitted');

            reportViewer('../Presscription/PrintPrescription', 'prescriptionId:' + response.Data.PrescriptionId);
        });
    },


    initMedicineGrid: function () {

        _km.initGrid_DS("gridMedicine", prescriptionHelper.columns(), '../Medicine/GetMedicine/');
        //_km.setGridOption({
        //    scrollable: {
        //        virtual: true
        //    },
        //    height: 300
        //});
    },
    columns: function () {
        return cols = [
            //{ field: "MedicineName", title: "Medicine", width: 10, template: '#= prescriptionHelper.MedicineFirstLetter(data) #', filterable: false },
            { field: "MedicineName", title: "Medicine", width: 25, filterable: false },
            { field: "PharmaCompanyName", title: "Company", width: 25, filterable: false },
            { field: "Formulation", title: "Formulation", width: 10, filterable: false },
            //{ field: "GenericName", title: "Generic", width: 10, filterable: false },
            { field: "Select", title: "Action", filterable: false, width: 10, template: '<input type="button" class="k-button" value="Add" onClick="prescriptionHelper.clickEventForSelect()"  />', sortable: false }

        ];
    },
    MedicineFirstLetter: function (data) {

        var firstLetter = "";

        firstLetter = data.MedicineName.substring(0, 1);
        return '<h2>' + firstLetter.toUpperCase() + '</h2>';

    },
    clickEventForSelect: function () {
        var grid = $("#gridMedicine").data('kendoGrid');
        var item = grid.dataItem(grid.select());
        if (item != null) {

            var obj = new PrescibeMedicine();
            //obj.Strengths = "5 mg";
            obj.MedicineId = item.MedicineId;
            obj.MedicineName = item.MedicineName;

            //var dd = new Object();
            //dd.DosageId = 0;
            //dd.Dosage = "-Select-";
            //obj.DrugDosage = dd;

            var gridPres = $("#gridPrescription").data('kendoGrid');
            var dataSource = gridPres.dataSource;
            var data = dataSource.data();
            var existData = jQuery.grep(data, function (n, i) {
                return (n.MedicineId == obj.MedicineId);
            });
            if (existData.length > 0) {
                alert('The medicine already added');
            } else {

                dataSource.insert(0, obj);
                //   gridPres.refresh();

            }

        }
    },

    initPrescriptionGrid: function () {


        $("#gridPrescription").kendoGrid({
            dataSource: prescriptionHelper.gridPrescriptionDS(),
            pageable: true,
            filterable: false,
            sortable: false,
            columns: prescriptionHelper.columnsPrescription(),
            editable: true,
            navigatable: true,
            selectable: "row",
            //rowTemplate: kendo.template($("#rowTemplate").html()),
        });

    },
    columnsPrescription: function () {
        return cols = [
            { field: "MedicineName", title: "Medicine", width: 50 },
            { field: "Strengths", title: "Weight", width: 50, template: '#= Strengths #' },
            { field: "NumberOfDosage", title: "Number Of Dosages", width: 50, editor: prescriptionHelper.numberOfDosageDropDown },
            { field: "DrugDosage", title: "Dosage", width: 50, editor: prescriptionHelper.DugDosageEditor, template: '#= DrugDosage.Dosage #' },
            { field: "MaxDays", title: "Dosage Days", width: 50, },

            { field: "destroy", title: "Action", filterable: false, width: 40, template: '<button type="button" onClick="prescriptionHelper.clickToDeleteRow()" class="k-button">Remove</button>', sortable: false }
            //{ field: "MedicineName", title: "Medicine", width: 20 },


            //{ field: "Select", title: "#", filterable: false, width: 10, template: '<input type="button" class="k-button" value="Select" onClick="pqHelper.clickEventForSelect()"  />', sortable: false }

            //{ field: "View", title: "#", filterable: false, width: 10, template: '<input type="button" class="k-button" value="View" onClick="pqHelper.clickEventView()"  />', sortable: false }
        ];
    },

    gridPrescriptionDS: function () {
        var gridDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,

            serverSorting: false,

            serverFiltering: false,

            allowUnsort: true,

            pageSize: 15,

            batch: true,
            //data:data,
            transport: {
                read: {
                    url: '../Presscription/GetPrescription?appointmentId=0',

                    type: "POST",

                    dataType: "json",

                    async: false,

                    contentType: "application/json; charset=utf-8"
                },

                parameterMap: function (options) {

                    return JSON.stringify(options);

                }
            },
            schema: {
                model: PrescibeMedicine,

                data: "Items", total: "TotalCount"

            }
        });
        return gridDataSource;
    },

    DugDosageEditor: function (container, options) {
        $('<input data-text-field="Dosage" data-value-field="DosageId" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                optionLabel: "--Select--",
                //dataTextField: "Dosage",
                //dataValueField: "DosageId",
                autoBind: false,
                dataSource: {
                    type: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "../Medicine/GetDrugDosage",

                            type: "POST",

                            dataType: "json",

                            async: false,

                            contentType: "application/json; charset=utf-8"
                        }
                    }
                },
            });
    },
    numberOfDosageDropDown: function (container, options) {
        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                optionLabel: "--Select--",
                autoBind: false,
                dataSource: [1, 2, 3, 4]

            });
    },
    clickToDeleteRow: function () {
        var grid = $("#gridPrescription").data('kendoGrid');
        var item = grid.dataItem(grid.select());
        if (item != null) {
            swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, remove it!",
                closeOnConfirm: false,
                //timer: 2000,
            }, function () {

                grid.dataSource.remove(item);
                swal({
                    title: "Removed",
                    text: "The item is removed!",
                    type: "success",
                    timer: 500,
                    showConfirmButton: false
                });



            });
        }



    },
    getPrescribeMedicine: function () {
        var grid = $("#gridPrescription").data('kendoGrid');
        var data = grid.dataSource.data();
        var medicine = [];
        for (var i = 0; i < data.length; i++) {
            var obj = new Object();
            obj.MedicineId = data[i].MedicineId;
            obj.DosageId = data[i].DrugDosage.DosageId;
            obj.NumberOfDosage = data[i].NumberOfDosage;
            obj.MaxDays = data[i].MaxDays;
            medicine.push(obj);
        }
        return medicine;
    }


}

var PrescibeMedicine = kendo.data.Model.define({
    id: "MedicineId", // the identifier of the model
    fields: {
        "MedicineName": {
            type: 'string', editable: false
        },
        "Strengths": {
            type: 'string', editable: false
        },
        "NumberOfDosage": {
            type: 'number', editable: true, defaultValue: 1
        }
        ,
        "DrugDosage": {
            editable: true, defaultValue: { DosageId: 0, Dosage: "-Select-" }
        },
        "MaxDays": {
            editable: true, type: 'number'
        },
        destroy: { editable: false },
    }
});