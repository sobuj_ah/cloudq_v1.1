﻿$(document).ready(function () {
    pqHelper.init();

});


var pqHelper = {
    init: function () {
        pqHelper.initGrid();
        prescriptionHelper.init();
    },


    initGrid: function () {

        _km.initGrid_DS("gridPatient", pqHelper.columns(), '../Presscription/GetPatientWaiting');
    },
    columns: function () {
        return cols=[
         //{ field: "DoctorName", title: "Doctor Name", width: 100 },
         { field: "SerialNo", title: "Serial No", width: 10 },
         { field: "SerialDate", title: "Entry Time", width: 10, template: '#:kendo.toString(kendo.parseDate(SerialDate),"hh:mm tt")#' },
         { field: "PatientNo", title: "Patient No", width: 10 },
         { field: "PatientName", title: "Patient Name", width: 20 },
         { field: "Age", title: "Age", width: 5 },
         { field: "Sex", title: "Sex", width: 7 },
         { field: "ContactNo", title: "ContactNo", width: 10 },
         { field: "Status", title: "Status", width: 10, template: "#= Status==0?'Waiting':'Completed' #" },

         { field: "Edit", title: "#", filterable: false, width: 10, template: '<input type="button" class="k-button" value="Add Prescription" onClick="pqHelper.clickEventAddPrescription()"  />', sortable: false }
        ];
    },
    clickEventAddPrescription: function () {
        
        var grid = $("#gridPatient").data('kendoGrid');
        var item = grid.dataItem(grid.select());
        if (item != null) {
            $("#divPataintQueue").hide();
            $("#divPrescription").show();
            prescriptionHelper.setPationInfo(item);
        }
    }

}

