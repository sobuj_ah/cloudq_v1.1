﻿var serialBookingManager = {
    init: function () {
        serialBookingManager.SerialGrid();
        _ah.GenerateCommonCombo("cmbDoctorName", "DoctorId", "DoctorName", "Pqm.Doctors", "");
        _km.datePicker('txtSerialDate');
        _km.initWindow('wndDoctorInfo', 'Patient Serial Booking Form');

        $("#btnSaveChanges").click(function () {
            if (serialBookingManager.isValidation()) {
                var obj = serialBookingManager.CreateObject();
                sendObject("../Appointment/BookSerial", 'formData:' + JSON.stringify(obj), function (response) {
                    $("#gridSerialBooking").data('kendoGrid').dataSource.read();
                    var wnd = $("#wndDoctorInfo").data('kendoWindow');
                    wnd.close();
                });
                //$('#divDoctor').saveTo("../SerialBooking/BookSerial", function (response) {
                //    MessageBox.show(response, function () {
                //        $("#gridSerialBooking").data('kendoGrid').dataSource.read();
                //        var wnd = $("#wndDoctorInfo").data('kendoWindow');
                //        wnd.close();
                //    });

                //});
            }
        });

        $("#btnAddSerial").click(function () {
            var wnd = $("#wndDoctorInfo").data('kendoWindow');
            wnd.open().center();
            // _ah.generateAutoIncrementCode("txtDoctorCode", "DC", "DoctorCode", "PQM.Doctors");

        });

        $("#btnPrintDoctorSummary").click(function () {

            var url = "../Reports/DoctorDetailsSummaryPrint";
            var param = "" + JSON.stringify();
            reportViewer(url, param);

        });

        $("#btnCloseWindow").click(function () {
            var wnd = $("#wndDoctorInfo").data('kendoWindow');
            wnd.close();
        });
        $("#btnClear").click(function () {
            _ah.clearAllFields();
        });

        $("#btnGetSerial").click(function () {
            var date = $("#txtSerialDate").data('kendoDatePicker').value();

            var obj = getData('../Appointment/GetSerial', 'date:' + JSON.stringify(date));
            $("#txtSerialNo").val(obj);

        });

    },
    SerialGrid: function () {

        _km.initGrid_DS("gridSerialBooking", serialBookingManager.GenerateDoctorColumns(), '../Appointment/GridDataSource');
    },
    GenerateDoctorColumns: function () {
        return columns = [
        { field: "DoctorName", title: "Doctor Name", width: 100 },
        { field: "SerialDate", title: "Serial Date", width: 100, template: '#:kendo.toString(kendo.parseDate(SerialDate),"dd-MMM-yyyy")#' },
        { field: "SerialNo", title: "Serial No", width: 100 },
        { field: "PatientName", title: "Patient Name", width: 100 },
        { field: "ContactNo", title: "Contact No", width: 100 },
        { field: "Edit", title: "Edit", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" onClick="serialBookingManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridSerialBooking").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {

            var wnd = $("#wndDoctorInfo").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divDoctor", selectedItem);
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divDoctor")) {
            return true;
        }
        return false;
    },
    CreateObject: function () {
        var obj = new Object();
        obj.DoctorId = $("#cmbDoctorName").val();
        obj.SerialDate = $("#txtSerialDate").data('kendoDatePicker').value();
        obj.SerialNo = $("#txtSerialNo").val();
        obj.ContactNo = $("#txtContactNo").val();
        obj.PatientName = $("#txtPatientName").val();
        return obj;
    }


}