﻿

var code = "";

$(document).ready(function () {
    _pregHelper.init();
});

var _pregHelper = {
    init: function () {
        $("#txtVerificationCode").keyup(function () {
            var iCode = $("#txtVerificationCode").val();
            if (iCode != "") {
                if (iCode == code) {
                    swal({
                        title: "Verification Code",
                        text: "Verified!",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            }
        });
        $("#btnSubmit").click(function () {
            _pregHelper.regsigtration();
        })
    },
    sendVerificationCode: function () {
        var mobile = $("#txtMobile").val();
        code = _codeGen.verificationCode(mobile);


    },
    regsigtration: function () {
        debugger;
        var param = JSON.stringify(_pregHelper.getPatientObject());
        sendObject('../PatientInfo/SavePatientInfo', "formData:" + param, function (response) {
            if (response.Status) {

                swal({
                    title: "Saved",
                    text: "Patient Save Successfully",
                    type: "success",
                    timer: 1000,
                    showConfirmButton: false
                });
                window.location.href = "../Home/LoginPatient";

            }
        });

        //sendObject('../PatientInfo/SavePatientInfo', "formData:" + param, function (response) {
        //    if (response.Status) {

        //        swal({
        //            title: "Saved",
        //            text: "Patient Save Successfully",
        //            type: "success",
        //            timer: 1000,
        //            showConfirmButton: false
        //        });
        //        window.location.href = "../Home/LoginPatient";

        //    }
        //};

    },
    getPatientObject: function () {
        var obj = new Object();
        obj.PatientName = $("#txtPatientName").val();
        obj.Sex = $("#txtGender").val();
        obj.ContactNo = "01" + $("#txtMobile").val();
        obj.EmailAddress = $("#txtEmail").val();
        obj.Password = $("#txtPassword").val();
        obj.DateOfBirth = kendo.toString($("#txtDob").val(), 'MM/dd/yyyy');
        obj.PhoneNo = $("#txtPhone").val();
        obj.Description = $("description").html();

        return obj;

    }


}