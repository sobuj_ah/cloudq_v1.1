﻿ 
var customerManager = {
    init: function () {
        customerManager.customerGrid();
       // _ah.GenerateCommonCombo("cmbCustomerTypeName", "CustomerTypeId", "CustomerTypeName", "CustomerType", "");
        _km.initWindow('wndCustomerInfo', 'Patient Information Form');
      
        
        $("#btnSaveChanges").click(function () {
            if (customerManager.isValidation()) {
                $('#divCustomer').saveTo("../PatientInfo/SavePatientInfo", function (response) {
                    MessageBox.show(response, function () {
                        $("#gridCustomer").data('kendoGrid').dataSource.read();
                        var wnd = $("#wndCustomerInfo").data('kendoWindow');
                        wnd.close();
                    });
                    
                });
            }
        });
       
        $("#btnAddCustomer").click(function () {
          var  wnd = $("#wndCustomerInfo").data('kendoWindow');
            wnd.open().center();
            _ah.generateAutoIncrementCode("txtPatientNo", "PT", "PatientNo", "PQM.PatientInfo");
            
        });

        $("#btnPrintCustomerSummary").click(function () {

            var url = "../Reports/CustomerDetailsSummaryPrint";
            var param = "" + JSON.stringify();
            reportViewer(url, param);

        });

        $("#btnCloseWindow").click(function () {
            var wnd = $("#wndCustomerInfo").data('kendoWindow');
            wnd.close();
        });
        $("#btnClear").click(function () {
            _ah.clearAllFields();
        });
        
    },
    customerGrid: function () {

        _km.initGrid_DS("gridCustomer", customerManager.GenerateCustomerColumns(), '../PatientInfo/GridDataSource');
    },
    GenerateCustomerColumns: function () {
        return columns = [
        { field: "PatientNo", title: "Patient Code", width: 100 },
        { field: "PatientName", title: "Patient Name", width: 100 },
        { field: "Age", title: "Age", width: 100 },
        { field: "Sex", title: "Sex", width: 100 },
        { field: "ContactNo", title: "ContactNo", width: 100 },
            { field: "Edit", title: "Edit", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" onClick="customerManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridCustomer").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            var wnd = $("#wndCustomerInfo").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divCustomer", selectedItem);
          }
    },
    isValidation: function() {
        if (_km.kendoValidator("divCustomer")) {
            return true;
        }
        return false;
    }
}