﻿ 
var doctorManager = {
    init: function () {
        doctorManager.DoctorGrid();
       // _ah.GenerateCommonCombo("cmbdoctorTypeName", "doctorTypeId", "doctorTypeName", "doctorType", "");
        _km.initWindow('wndDoctorInfo', 'Doctor Information Form');
      
        
        $("#btnSaveChanges").click(function () {
            if (doctorManager.isValidation()) {
                $('#divDoctor').saveTo("../Doctor/SaveDoctorInfo", function (response) {
                    //MessageBox.show(response, function () {
                        $("#gridDoctor").data('kendoGrid').dataSource.read();
                        var wnd = $("#wndDoctorInfo").data('kendoWindow');
                        wnd.close();
                    //});
                    
                });
            }
        });
       
        $("#btnAddDoctor").click(function () {
          var  wnd = $("#wndDoctorInfo").data('kendoWindow');
            wnd.open().center();
            _ah.generateAutoIncrementCode("txtDoctorCode", "DC", "DoctorCode", "PQM.Doctors");
            
        });

        $("#btnPrintDoctorSummary").click(function () {

            var url = "../Reports/DoctorDetailsSummaryPrint";
            var param = "" + JSON.stringify();
            reportViewer(url, param);

        });

        $("#btnCloseWindow").click(function () {
            var wnd = $("#wndDoctorInfo").data('kendoWindow');
            wnd.close();
        });
        $("#btnClear").click(function () {
            _ah.clearAllFields();
        });
        

        
    },
    DoctorGrid: function () {

        _km.initGrid_DS("gridDoctor", doctorManager.GenerateDoctorColumns(), '../Doctor/DoctorGridDataSource');
    },
    GenerateDoctorColumns: function () {
        return columns = [
        { field: "DoctorId", hidden: true },
        { field: "DoctorCode", title: "Doctor Code", width: 100, hidden: true },
        { field: "DoctorName", title: "Doctor Name", width: 100 },
        { field: "Degree", title: "Title", width: 100 },
        { field: "ContactNo", title: "ContactNo", width: 100 },
        //{ field: "DoctorEmail", title: "Email", width: 100 },
        { field: "DoctorTypeName", hidden: true, title: "Doctor Type", width: 100 },
        { field: "Edit", title: "Edit", filterable: false, width: 30, template: '<input type="button" class="k-button" value="Edit" onClick="doctorManager.clickEventForEditButton()"  />', sortable: false },
        { field: "Delete", title: "Delete", filterable: false, width: 30, template: '<input type="button" class="k-button" value="Delete" onClick="doctorManager.clickEventForDeleteButton()"  />', sortable: false }

        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridDoctor").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            var wnd = $("#wndDoctorInfo").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divDoctor", selectedItem);
          }
    },

    clickEventForDeleteButton: function () {
        var grid = $("#gridDoctor").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            if (confirm("Are you sure you delete it?")) {
                var url = "../Doctor/Delete";
                var param = "doctorId:" + JSON.stringify(selectedItem.DoctorID);
                sendObject(url, param, function (response) {

                    $("#gridDoctor").data('kendoGrid').dataSource.read();

                });
            }
            //$('#divDoctor').saveTo("../Doctor/Delete", function (response) {
            //    MessageBox.show(response, function () {
            //        $("#gridDoctor").data('kendoGrid').dataSource.read();
            //        var wnd = $("#wndDoctorInfo").data('kendoWindow');
            //        wnd.close();
            //    });

            //});
          }
    },
    isValidation: function() {
        if (_km.kendoValidator("divDoctor")) {
            return true;
        }
        return false;
    }
}