﻿ 


var ProductInfoManager = {

    init: function () {

        ProductInfoManager.ProductInfoManagerGrid();
        //"html-Id","Entity Id","Entity Id Name","Database tableName or className"
        _ah.GenerateCommonCombo("cmbCategoryId", "TestCategoryId", "CategoryName", "pqm.TestCategories", "");

        // _ah.GenerateCommonCombo("cmbBrand", "BrandId", "BrandName", "Brand", "");
        //  _km.initWindow('wndProductInfo', 'Test Information', false);
        // _km.numericTextbox("ntVatRate");
        _km.numericTextbox("ntCostPrice");
        // _km.numericTextbox("ntSalesPrice");
        //  _km.numericTextbox("ntCommissionRate");
        // _km.numericTextbox("ntExpireDuration");
        // _km.numericTextbox("ntExpireBeforDays");
        // _km.numericTextbox("ntLimitStockAlter");
        //_km.checkedAfterHideShow('chkIsApplicableExpire', "divIsApplicableExpire");
        //_km.checkedAfterHideShow('chkUseManufacturerBarcode', "divManufacturerBarcode");
        //_km.checkedAfterHideShow('chkIsVatApplicable', "divIsVatApplicable");
        _km.initWindow('wndProductCategory', 'Add new category', false);
        //_km.initWindow('wndBrand', 'Add new brand', false);
        _km.initWindow('wndUploadPicture', 'Add new picture', false);


        $("#btnAddProductInfo").click(function () {
            //var wnd = $("#wndProductInfo").data('kendoWindow');

            //wnd.open().maximize();

            $("#divGridTestSummary").hide();
            $("#wndProductInfo").show();

            _ah.clearAllFields();
            _ah.generateAutoIncrementCode("txtTestCode", "TR", "TestCode", "pqm.TestInfoes");
        });


        $("#btnPrintProductSummary").click(function () {

            var url = "../Reports/ProductInfoDetailsSummaryPrint";
            var param = "" + JSON.stringify();
            reportViewer(url, param);

        });

        $("#btnSaveChangesProductInfo").click(function () {

            if (ProductInfoManager.isValidation()) {
                var product = $('#divProductInfo').getFormData();

                sendObject('../LabTest/SaveTestInfo', 'formData:' + JSON.stringify(product), function (response) {

                   
                    $("#gridProductInfo").data('kendoGrid').dataSource.read();
                    $("#divGridTestSummary").show();
                    $("#wndProductInfo").hide();
                    MessageBox(response)
                });
            }
        });

        $("#btnClearProductInfo").click(function () {
            _ah.clearAllFields();
        });

        $("#btnAddCategory").click(function () {
            var wndProductCategory = $("#wndProductCategory").data('kendoWindow');

            wndProductCategory.open().center();
            // _ah.generateAutoIncrementCode("txtCategoryCode", "PC", "CategoryCode", "ProductCategory");
            _ah.GenerateCommonCombo("cmbParentProductCategory", "TestCategoryId", "CategoryName", "pqm.TestCategories", "");

        });

        $("#btnCloseWindowProductInfo").click(function () {
            // var wnd = $("#wndProductInfo").data('kendoWindow');
            //wnd.close();
            $("#divGridTestSummary").show();
            $("#wndProductInfo").hide();
        });

        //$("#btnAddBrand").click(function () {
        //    var wndBrand = $("#wndBrand").data('kendoWindow');

        //    wndBrand.open().center();
        //    _ah.generateAutoIncrementCode("txtBrandCode", "BC", "BrandCode", "Brand");

        //});

        $("#uploadPictureButton").click(function () {
            var wnd = $("#wndUploadPicture").data('kendoWindow');

            wnd.open().center();
        });

    },

    ProductInfoManagerGrid: function () {
        _km.initGrid_DS("gridProductInfo", ProductInfoManager.GenerateProductInfoColumns(), '../LabTest/GridDataSource');
    },

    GenerateProductInfoColumns: function () {
        return columns = [
            { field: "TestCode", title: "Test Code", width: 60 },
            { field: "CategoryName", title: "Category", width: 80 },
            { field: "TestName", title: "Test Name", width: 100 },
            { field: "Description", title: "Description", width: 200 },
            { field: "Amount", title: "Amount", width: 50, hidden: false },
            { field: "Edit", title: "Edit", filterable: false, width: 40, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="ProductInfoManager.clickEventForEditButton()"/>', sortable: false },
            { field: "Delete", title: "Delete", filterable: false, width: 40, template: '<input type="button" class="k-button" value="Delete" id="btnEdit" onClick="ProductInfoManager.clickEventForDeleteButton()"/>', sortable: false }

        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridProductInfo").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);

           _ah.populate("#divProductInfo", selectedItem);
            //var wnd = $("#wndProductInfo").data('kendoWindow');
            //wnd.open().center();
           $("#divGridTestSummary").hide();
           $("#wndProductInfo").show();
        }
    },
    clickEventForDeleteButton: function () {
        var grid = $("#gridProductInfo").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            if (confirm("Are you sure you delete it?")) {
                var url = "../LabTest/Delete";
                var param = "Id:" + JSON.stringify(selectedItem.Id);
                sendObject(url, param, function (response) {

                    $("#gridProductInfo").data('kendoGrid').dataSource.read();
                    MessageBox(response)
                });
            }
         
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divProductInfo")) {
            return true;
        }
        return false;
    },

}