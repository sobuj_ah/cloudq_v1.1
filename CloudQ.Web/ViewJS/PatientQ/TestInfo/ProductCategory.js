﻿

var ProductCategoryManager = {

    init: function () {

        $("#btnSaveProductCategory").click(function () {

            if (ProductCategoryManager.isValidation()) {
                $('#divProductCategory').saveTo("../TestCategory/SaveTestCategory", function (response) {
                    if (response.Status) {
                        swal("Done!", "Test Category is Saved", "success");
                        $("#wndProductCategory").data('kendoWindow').close();
                       $("#gridProductCategory").data('kendoGrid').dataSource.read();
                    } else {

                    }

                   
                    //MessageBox.show(response, function () {
                    //   
                    //    var wnd = $("#wndProductCategory").data('kendoWindow');
                    //    wnd.close();
                    //});


                });
                // _ah.GenerateCommonCombo("cmbParentProductCategory", "CategoryId", "CategoryName", "ProductCategory", "");
            }
        });



        $("#btnCloseWindowProductCategory").click(function () {
            var wnd = $("#wndProductCategory").data('kendoWindow');

            wnd.close();
        });

        $("#btnClearProductCategory").click(function () {
            _ah.clearAllFields();
        });
    },
    load: function () {
        ProductCategoryManager.ProductCategoryManagerGrid();
        _km.initWindow('wndProductCategory', 'Lab Test Category');
        $("#btnAddProductCategory").click(function () {
            var wnd = $("#wndProductCategory").data('kendoWindow');
            wnd.open().center();
            _ah.GenerateCommonCombo("cmbParentProductCategory", "TestCategoryId", "CategoryName", "pqm.TestCategories", "");


        });
    },

    ProductCategoryManagerGrid: function () {

        _km.initGrid_DS("gridProductCategory", ProductCategoryManager.GenerateProductCategoryColumns(), '../TestCategory/GridDataSource');
    },
    GenerateProductCategoryColumns: function () {
        return columns = [
            { field: "CategoryId", hidden: true },
            { field: "CategoryId", title: "Parent Category", width: 100 },
            { field: "CategoryCode", title: "Category Code", width: 100 },
            { field: "CategoryName", title: "Category Name", width: 100 },
            { field: "Description", title: "Description", width: 100 },
            { field: "Edit", title: "Edit", filterable: false, width: 60, template: '<button type="button" class="btn btn-default" title="Edit" onClick="ProductCategoryManager.clickEventForEditButton()"  ><span class="glyphicon glyphicon-pencil"></span></button>', sortable: false }
        ];
    },

    clickEventForEditButton: function () {
        var grid = $("#gridProductCategory").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {

            _ah.populate("#divProductCategory", selectedItem);
            var wnd = $("#wndProductCategory").data('kendoWindow');
            wnd.open().center();
        }
    },

    isValidation: function () {
        if (_km.kendoValidator("divProductCategory")) {
            return true;
        }
        return false;
    }
}