﻿

var ProductSalesDetailsManager = {
    init: function () {
        ProductSalesDetailsManager.ProductSalesDetailsManagerGrid();
        _km.ComboBox("txtBarcode", "TestId", "TestName", "../LabTest/GetTestComboData", "");
        _km.ComboBox("cmbRefdBy", "DoctorID", "DoctorName", "../Doctor/GetDoctors", "");
        ProductSalesDetailsManager.CombBoxSex();
        // _pos.barcodeAutoComplete('txtBarcode');
        _km.numericTextbox("ntQuantity");
        _km.numericTextbox("ntDiscountRate");
        _km.numericTextbox("ntDiscountedPrice");
        _km.numericTextbox("ntVatAmount");
        _km.numericTextbox("txtDueAmount");
        _km.numericTextbox("ntSalesPrice");
        ProductSalesDetailsManager.init_events();


    },
    init_events: function () {





    },
    ProductSalesDetailsManagerGrid: function () {

        // _km.initGrid_blank("gridProductSalesDetails", ProductSalesDetailsManager.GenerateProductSalesDetailsColumns());
        var grid = $("#gridProductSalesDetails").kendoGrid({
            dataSource: ProductSalesDetailsManager.gridDataSource([]),
            pageable: false,
            filterable: false,
            sortable: false,
            columns: ProductSalesDetailsManager.GenerateProductSalesDetailsColumns(),
            editable: false,
            navigatable: false,
            scrollable: false,
           selectable: "row",
        }).data('kendoGrid');



        //var grid = $("#gridProductSalesDetails").data('kendoGrid');
        //var gridDS = _km.gridDataSource_blank(ProductSalesDetails);
        //grid.setDataSource(gridDS);

    },
    gridDataSource: function (data) {
        var gridDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: false,

            serverSorting: false,

            serverFiltering: false,

            allowUnsort: false,

            pageSize: 10,

            data: data,

            //transport: {
            //    read: {
            //        url: url,

            //        type: "POST",

            //        dataType: "json",

            //        async: true,

            //        cache: true,

            //        contentType: "application/json; charset=utf-8"
            //    },

            //    parameterMap: function (options) {

            //        return JSON.stringify(options);

            //    }
            //},
            schema: {
                model: {
                    id: "DetailsId", // the identifier of the model
                    fields: {
                        "DetailsId": {
                            type: "number"
                        },
                        "TestCode": {
                            type: "string"
                        },
                        "Barcode": {
                            type: "string"
                        },
                        "TestName": {
                            type: "string"
                        },
                        "Quantity": {
                            type: "number"
                        },
                        "Amount": {
                            type: "number"
                        },
                        "DiscountRate": {
                            type: "number"
                        },
                        "DiscountedPrice": {
                            type: "number"
                        },
                        "Price": {
                            type: "number"
                        }
                    }
                },

                //data: "Items", total: "TotalCount"

            }
        });
        return gridDataSource;
    },
    GenerateProductSalesDetailsColumns: function () {
        return columns = [

            { field: "TestCode", title: "Test Code", width: 10, filterable: false },
            { field: "TestName", title: "Test Name", width: 30, filterable: false, },
            { field: "Amount", title: "Charge", width: 7, filterable: false, },
            //{ field: "DiscountAmount", title: "DiscountAmount", width: 100, filterable: false, },
            { field: "Delete", title: "Action", filterable: false, width: 5, template: ' <button onClick="ProductSalesDetailsManager.clickEventForDeleteButton()" type="button" class="btn btn-raised btn-default waves-effect"> <i class="zmdi zmdi-delete"></i></button>', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridProductSalesDetails").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);

            _ah.populate("#divProductSalesDetails", selectedItem);
        }
    },

    isValidation: function () {
        if (_km.kendoValidator("divProductSalesDetails")) {
            return true;
        }
        return false;
    },
    AddProductItem: function () {

        if (ProductSalesDetailsManager.isValidation()) {


            var dataItem = new Object();
            var data = $("#txtBarcode").data().kendoComboBox;
            dataItem = data.dataItem();
            var prdItem = $('#divProductSalesDetails').getFormData();

            if (dataItem != null) {


                var grid = $("#gridProductSalesDetails").data('kendoGrid');
                var data = grid.dataSource.data();

                var obPrd = new Object();
                obPrd.TestId = dataItem.TestId;
                obPrd.TestCode = dataItem.TestCode;
                obPrd.TestName = dataItem.TestName;
                obPrd.Quantity = 1;
                obPrd.Amount = dataItem.Amount;

                grid.dataSource.insert(0, obPrd);

                //ProductSalesDetailsManager.ClearSalesDetails();
            }
        }


    },
    GetProductPurchaseInfo: function (prdCode) {
        var data = getData("../ProductPurchase/GetProductPurchaseByProductCode", "productCode:" + JSON.stringify(prdCode));
        console.log(data);
    },
    CalculationSalesPrice: function () {

        var dataItem = new Object();
        //var barcode = $("#txtBarcode").val();
        //var data = $("#txtBarcode").data().kendoAutoComplete.dataSource.data();
        var data = $("#txtBarcode").data().kendoComboBox;
        dataItem = data.dataItem();
        //var arr = jQuery.grep(data, function (n, i) {
        //    return (n.Barcode == barcode);
        //});
        if (dataItem != null) {

            $("#ntSalesPrice").data('kendoNumericTextBox').value(dataItem.Amount);

        }



    },
    CalculateTotalPrice: function () {

        var grid = $("#gridProductSalesDetails").data('kendoGrid');
        var data = grid.dataSource.data();
        var totalAmt = 0, vatAmt = 0, subTotal = 0, discunt = 0;
        var dueAmt = 0, advAmt = 0, paidAmt = 0, netPay = 0, rcvAmt = 0;
        for (var i = 0; i < data.length; i++) {
            totalAmt += parseFloat(data[i].Amount);
            //vatAmt += parseFloat(data[i].VatAmount);
            //discunt += parseFloat(data[i].DiscountedPrice);

        }
    
        debugger;
        $("#txtTotal").data('kendoNumericTextBox').value(totalAmt);
        $("#txtNetPay").data('kendoNumericTextBox').value(totalAmt);
        $("#txtDueAmount").data('kendoNumericTextBox').value(totalAmt);
         
    },
    ClearSalesDetails: function () {
        //var grid = $("#gridProductSalesDetails").data('kendoGrid');
        ////var gridDS = _km.gridDataSource_blank([]);
        //grid.setDataSource([]);

    },
    GetProductSalesdDetails: function (selectedItem) {
        var salesId = selectedItem.SalesId;

        // var data = getData("../ProductSales/GetProductSalesDetails", "salesId=" + salesId);
        var grid = $("#gridProductSalesDetails").data('kendoGrid');
        var dataSource = _km.gridDataSource('../PatientTest/GetProductTestDetails/?patientTestId=' + salesId, ProductSalesDetails);
        grid.setDataSource(dataSource);

    },
    clickEventForDeleteButton: function () {
        debugger;
        var grid = $("#gridProductSalesDetails").data('kendoGrid');
        var item = grid.dataItem(grid.select());
        if (item != null) {
            grid.dataSource.remove(item);
            ProductSalesDetailsManager.CalculateTotalPrice();
            //ProductSalesDetailsManager.CalculationSalesPrice();
            //ProductSalesManager.calculateNetAmt();
       
           // ProductSalesManager.calculateReturnAmount();
        }

    },

    CombBoxSex: function () {

        $("#cmbPateintSex").kendoComboBox({
            placeholder: "All",
            dataTextField: 'text',
            dataValueField: 'value',
            dataSource: [
                { text: 'Male', value: 1 },
                { text: 'Female', value: 2 },

            ]
        });
    },


};


//Prodsuct Sales model
//var ProductSalesDetails = kendo.data.Model.define({
//    id: "DetailsId", // the identifier of the model
//    fields: {
//        "TestCode": {
//            type: "string"
//        },
//        "Barcode": {
//            type: "string"
//        },
//        "TestName": {
//            type: "string"
//        },
//        "Quantity": {
//            type: "number"
//        },
//        "Amount": {
//            type: "number"
//        },
//        "DiscountRate": {
//            type: "number"
//        },
//        "DiscountedPrice": {
//            type: "number"
//        },
//        "Price": {
//            type: "number"
//        }
//    }
//});