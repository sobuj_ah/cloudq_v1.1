﻿var _payment = null;

var ProductSalesManager = {
    init: function () {
        $("#txtPhone").focus();
        _ah.generateAutoIncrementCode("txtInvoiceNo", 'INV', "InvoiceNo", "PQM.PatientTest");
        _km.initWindow('wndStatement', 'Daily Sales Statement');
        ProductSalesManager.ProductSalesManagerGrid();
        ProductSalesDetailsManager.init();
        _km.datePicker("dateTimeSalesDate");
        //_km.dateTimePicker('txtdateDelivery');
        var newdate = new Date();
        newdate.setDate(newdate.getDate() + 1);
        $("#txtdateDelivery").kendoDateTimePicker({
            value: newdate,
                format: "dd hh:mm tt",
            });
       
        _km.numericTextbox("txtTotal");
        _km.numericTextbox("txtAdvanceAmount");
        _km.numericTextbox("txtDiscount");
        _km.numericTextbox("txtNetPay");
        _km.numericTextbox("txtReceiveAmount");
        // _km.numericTextbox("txtReturnAmount");
        // $("#txtReceiveAmount").data('kendoNumericTextBox').value("0");
        _km.datePicker('txtFromDate');
        _km.datePicker('txtToDate');
        // $("#btnSaveChanges").html("Print Invoice");

        ProductSalesManager.navigationEvents();


        $("#btnPrintDailyStatement").click(function () {
            var fromDt = $("#txtFromDate").data('kendoDatePicker').value();
            var toDt = $("#txtToDate").data('kendoDatePicker').value();
            var url = "../Report/PrintDailyStatement";

            var param = "fromDate:" + JSON.stringify(fromDt) + ",toDate:" + JSON.stringify(toDt);

            pdfViewer(url, param);
        });
        $("#btnPrintDailyStatementDetails").click(function () {
            var fromDt = $("#txtFromDate").data('kendoDatePicker').value();
            var toDt = $("#txtToDate").data('kendoDatePicker').value();
            var url = "../Report/PrintDailyStatementDetails";

            var param = "fromDate:" + JSON.stringify(fromDt) + ",toDate:" + JSON.stringify(toDt);

            pdfViewer(url, param);
        });

        $("#btnSaveChanges").click(function () {
            ProductSalesManager.SaveProductSale();
            //ProductSalesManager.printInvoice();

        });
        $("#btnPrintInvoice").click(function () {

            // ProductSalesManager.SaveProductSale();
            var invoiceNo = $("#txtInvoiceNo").val();
            ProductSalesManager.printInvoice(invoiceNo);
        });

        $("#btnAddProductSales").click(function () {
            $("#wndProductSales").show();
            $("#divGridProduct").hide();
            ProductSalesManager.clear();
        });
        $("#btnAddProdutItem").click(function () {
            ProductSalesDetailsManager.AddProductItem();
            ProductSalesDetailsManager.CalculateTotalPrice();
            //  ProductSalesManager.calculateReturnAmount();


            $("#txtBarcode").data("kendoComboBox").focus();
            $("#txtBarcode").data("kendoComboBox").value('');
            $("#ntSalesPrice").data("kendoNumericTextBox").value(0);
        });



        $("#btnPrintProductSalesSummary").click(function () {

            var url = "../Reports/ProductSalesDetailsSummaryPrint";
            var param = "" + JSON.stringify();
            reportViewer(url, param);

        });
        $("#btnCloseWindow").click(function () {
            //var wnd = $("#wndProductSales").data('kendoWindow');
            //wnd.close();
            $("#gridProductSales").data('kendoGrid').dataSource.read();
            $("#wndProductSales").show();
            $("#divGridProduct").hide();
        });
        $("#btnHistory").click(function () {
            $("#gridProductSales").data('kendoGrid').dataSource.read();
            $("#wndProductSales").hide();
            $("#divGridProduct").show();
        });

        $("#txtPhone").keypress(function (e) {
            if (e.keyCode == 13) {
                $("#txtCustomerName").focus();

            }
        });
        $("#txtCustomerName").keypress(function (e) {
            if (e.keyCode == 13) {
                $("#cmbPateintSex").data('kendoComboBox').focus();

            }
        });

        $("#btnSearchInvoiceNo").click(function () {
            var invNo = $("#txtSearchInvNo").val();
            ProductSalesManager.SearchByInvoiceNo(invNo);
            //$("#btnSaveChanges").html("Paid");
        });
        $("#txtSearchInvNo").keypress(function (e) {

            if (e.keyCode == 13) {
                var invNo = $("#txtSearchInvNo").val();
                ProductSalesManager.SearchByInvoiceNo(invNo);
                // $("#btnSaveChanges").html("Paid");
            }

        });

        $("#txtDiscount").focus(function () {
            var input = $(this);
            setTimeout(function () {
                input.select();
            });
        });
        $("#txtReceiveAmount").focus(function () {
            var input = $(this);
            setTimeout(function () {
                input.select();
            });
        });


    },
    onClickStatementWind: function () {
        var wnd = $("#wndStatement").data('kendoWindow');

        wnd.open().center();
    },
    SaveProductSale: function () {

        var obj = new Object();
        obj.PatientTest = ProductSalesManager.GetMasterObject();
        obj.PatientTestDetails = ProductSalesManager.GetProductSalesDetails();
        obj.Patient = ProductSalesManager.GetPatientDetails();

        if (ProductSalesManager.validation() && obj.PatientTestDetails.length > 0) {


            var url = "../PatientTest/SavePatientTest";
            var param = "formData:" + JSON.stringify(obj);

            sendObject(url, param, function (response) {
                window.open(response, '_blank');
                ProductSalesManager.clear();
            });
        } else {
            _km.notification("Enter required information");
        }
    },
    GetMasterObject: function () {

        var doctorCombo = $("#cmbRefdBy").data().kendoComboBox;
        var obj = new Object();
        obj.PatientTestId = $("#hdnPatientTestId").val();
        obj.InvoiceNo = $("#txtInvoiceNo").val();
        obj.SalesDate = kendo.toString($("#dateTimeSalesDate").data('kendoDatePicker').value(), "MM/dd/yyyy");
        obj.TotalAmount = $("#txtTotal").data('kendoNumericTextBox').value();
        obj.DiscountAmount = $("#txtDiscount").data('kendoNumericTextBox').value();
        obj.NetAmount = $("#txtNetPay").data('kendoNumericTextBox').value();
        obj.PaidAmount = $("#txtAdvanceAmount").data('kendoNumericTextBox').value();
        obj.DueAmount = $("#txtDueAmount").data('kendoNumericTextBox').value();
        obj.ReferredBy = doctorCombo.value();
        obj.DeliveryDate = kendo.toString($("#txtdateDelivery").data('kendoDateTimePicker').value(), "MM/dd/yyyy HH:mm");
        obj.PaymentType = 1;
        obj.TotalQuantity = 1;
        obj.Remarks = $("#txtRemarks").val();

        // var reciveAmt = $("#txtReceiveAmount").data('kendoNumericTextBox').value();


        return obj;
    },
    GetPatientDetails: function () {
        var objCustomer = new Object();
        objCustomer.PatientId = $("#hdnPatientId").val();
        objCustomer.PatientName = $("#txtCustomerName").val();
        objCustomer.Sex = $("#cmbPateintSex").data().kendoComboBox.text();
        objCustomer.Age = $("#txtPatientAge").val();
        objCustomer.ContactNo = $("#txtPhone").val();

        return objCustomer;

    },

    GetProductSalesDetails: function () {
        var objDtlS = [];
        var grid = $("#gridProductSalesDetails").data('kendoGrid');
        var detailsData = grid.dataSource.data();
        for (var i = 0; i < detailsData.length; i++) {
            var obj = new Object();
            obj.Quantity = 1;
            obj.TestId = detailsData[i].TestId;
            obj.Price = detailsData[i].Amount;
            objDtlS.push(obj);

        }
        return objDtlS;
    },
    ProductSalesManagerGrid: function () {

        _km.initGrid_DS("gridProductSales", ProductSalesManager.GenerateProductSalesColumns(), '../PatientTest/GridDataSource');
    },
    GenerateProductSalesColumns: function () {
        return columns = [
            { field: "PatientTestId", hidden: true },
            { field: "InvoiceNo", title: "Invoice No", width: 10 },
            { field: "PatientNo", title: "Patient ID", width: 10, hidden: false },
            { field: "PatientName", title: "Patient Name", width: 20, hidden: false },
            { field: "ContactNo", title: "Contact No", width: 10, hidden: false },
            { field: "Sex", title: "Sex", width: 5, },
            { field: "Age", title: "Age", width: 5, filterable: false, },
           // { field: "DoctorName", title: "Refd. By", width: 20, },
            { field: "DeliveryDate", title: "Delivery Date", width: 10, filterable: false, template: "#=kendo.toString(kendo.parseDate(DeliveryDate),'yyyy-MM-dd hh:mm tt')#" },
            //{ field: "Edit", title: "Edit", filterable: false, width: 10, template: '<input type="button" class="k-button" value="Edit" onClick="ProductSalesManager.clickEventForEditButton()"/>', sortable: false },
            { field: "Delete", title: "Delete", filterable: false, width: 5, template: '<button type="button" onClick="ProductSalesManager.clickEventForDeleteButton()" class="btn btn-raised waves-effect"> <i class="zmdi zmdi-delete"></i> </button>', sortable: false },
            { field: "Print", title: "Print", filterable: false, width: 5, template: '<button type="button" onClick="ProductSalesManager.clickEventForPrintButton()" class="btn btn-raised  waves-effect"> <i class="zmdi zmdi-print"></i> </button>', sortable: false }

        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridProductSales").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            //var wnd = $("#wndProductSales").data('kendoWindow');
            //  ProductSalesDetailsManager.GetProductSalesdDetails(selectedItem);
            $("#wndProductSales").show();
            $("#divGridProduct").hide();
            var objPt = ProductSalesManager.GetPatientTestData(selectedItem);


            _ah.populate("#divProductSales", selectedItem);
        }
    },
    clickEventForDeleteButton: function () {
        var grid = $("#gridProductSales").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {

            if (confirm("Are you sure you delete it?")) {
                var url = "../PatientTest/DeletePatientTest";
                var param = "patientTestId:" + JSON.stringify(selectedItem.PatientTestId);
                sendObject(url, param, function (response) {

                    $("#gridProductSales").data('kendoGrid').dataSource.read();

                });
            }
        }
    },
    clickEventForPrintButton: function () {
        var grid = $("#gridProductSales").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            ProductSalesManager.printInvoice(selectedItem.InvoiceNo);

        }
    },


    printInvoice: function (invoiceNo) {
        var url = "../PatientTest/PrintInvoice";
        // var invoiceNo = $("#txtInvoiceNo").val();
        var param = "invoiceNo:" + JSON.stringify(invoiceNo);
        pdfViewer(url, param);
    },
    calculateNetAmt: function () {
        var dueAmt = 0;
        var discunt = $("#txtDiscount").data('kendoNumericTextBox').value();
        var totalAmt = $("#txtTotal").data('kendoNumericTextBox').value();
        var netAmt = totalAmt - discunt;

        if (_payment != null) {
            var diff = (discunt - _payment.DiscountAmount);
            if (_payment.DueAmount > diff) {
                dueAmt = _payment.DueAmount - diff;
                $("#txtDueAmount").data('kendoNumericTextBox').value(dueAmt);

            }

        } else {
            $("#txtDueAmount").data('kendoNumericTextBox').value(netAmt);
        }
        // dueAmt = parseInt($("#txtDueAmount").data('kendoNumericTextBox').value());


        $("#txtNetPay").data('kendoNumericTextBox').value(totalAmt - discunt);
    },
    validation: function () {
        if (_km.kendoValidator("divPrdSlsMaster")) {
            var due = parseInt($("#txtDueAmount").data('kendoNumericTextBox').value());
            var advAmt = $("#txtAdvanceAmount").data('kendoNumericTextBox').value();
            if ((advAmt != null && advAmt > 0) && due > -1) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    },
    GetPatientTestData: function (selectedItem) {

        var url = "../PatientTest/GetPatientTestById";
        var param = "patientTestId:" + JSON.stringify(selectedItem.PatientTestId);
        return getData(url, param);
    },
    GetPatientTestDataByInvoiceNo: function (invoiceNo) {

        var url = "../PatientTest/GetPatientTestByInvoiceNo";
        var param = "invoiceNo:" + JSON.stringify(invoiceNo);
        return getData(url, param);
    },
    clear: function () {
        //ProductSalesDetailsManager.ClearSalesDetails();
        // _ah.clearAllFields();
        $("#cmbRefdBy").data('kendoComboBox').value('');
        $("#hdnPatientTestId").val(0);
        $("#txtInvoiceNo").val('');
        $("#dateTimeSalesDate").data('kendoDatePicker').value(new Date());
        $("#txtTotal").data('kendoNumericTextBox').value(0);
        $("#txtDiscount").data('kendoNumericTextBox').value(0);
        $("#txtNetPay").data('kendoNumericTextBox').value(0);
        $("#txtAdvanceAmount").data('kendoNumericTextBox').value(0);
        $("#txtDueAmount").data('kendoNumericTextBox').value(0);
        $("#txtReceiveAmount").data('kendoNumericTextBox').value(0);
        var newdate = new Date();
        newdate.setDate(newdate.getDate() + 1);
        $("#txtdateDelivery").data('kendoDateTimePicker').value(newdate);

        $("#txtRemarks").val('');

        $("#hdnPatientId").val(0);
        $("#txtCustomerName").val('');
        $("#txtPatientAge").val('');
        $("#txtPhone").val('');

        $("#gridProductSalesDetails").data('kendoGrid').dataSource.data([]);
        _ah.generateAutoIncrementCode("txtInvoiceNo", 'INV', "InvoiceNo", "PQM.PatientTest");
        _payment = null;
    },
    navigationEvents: function () {




        $("#cmbPateintSex").data("kendoComboBox").wrapper.keypress(function (e) {
            if (e.keyCode == 13) {
                $("#txtPatientAge").focus();
            }
        });

        $("#txtPatientAge").keypress(function (e) {
            if (e.keyCode == 13) {
                $("#cmbRefdBy").data("kendoComboBox").focus();

            }
        });

        $("#cmbRefdBy").data("kendoComboBox").wrapper.keypress(function (e) {
            if (e.keyCode == 13) {
                $("#txtBarcode").data("kendoComboBox").focus();
            }
        });

        $("#txtBarcode").data("kendoComboBox").wrapper.keypress(function (e) {
            if (e.keyCode == 13) {
                ProductSalesDetailsManager.CalculationSalesPrice();

                $("#btnAddProdutItem").focus();
            }
        });

        $("#txtBarcode").change(function () {

            ProductSalesDetailsManager.CalculationSalesPrice();

        });



        //Summary

        $("#txtReceiveAmount").data("kendoNumericTextBox").wrapper.keypress(function (e) {
            if (e.keyCode == 13) {
                // ProductSalesManager.calculatePayment();
                $("#btnPrintInvoice").focus();
            }
        });


        //$("#txtReceiveAmount").data("kendoNumericTextBox").wrapper.keyup(function (e) {

        //    ProductSalesManager.calculateNetPayment();


        //});


        $("#txtReceiveAmount").change(function () {
            ProductSalesManager.calculatePayment();
            //  ProductSalesManager.calculateReturnAmount();
            // ProductSalesDetailsManager.CalculateTotalPrice();

        });
        //$("#txtReceiveAmount").keypress(function (e) {
        //    if (e.keyCode == 13) {
        //        ProductSalesManager.calculateReturnAmount();
        //        $("#btnPrintInvoice").focus();

        //    }
        //});

        $("#txtDiscount").data("kendoNumericTextBox").bind("keypress", function (e) {

            if (e.keyCode == 13) {
                //  ProductSalesManager.calculateNetAmt();

            }
        });

        $("#txtDiscount").change(function (e) {
            ProductSalesManager.calculateNetAmt();

        });

        $("#txtDiscount").blur(function (e) {

            ProductSalesManager.calculateNetAmt();

        });
        //$("#txtAdvanceAmount").focusin(function () {

        //    $("#txtAdvanceAmount").select();
        //});

        //$("#txtAdvanceAmount").change(function () {
        //    ProductSalesManager.calculateReturnAmount();

        //});
        //$("#txtAdvanceAmount").keypress(function (e) {
        //    if (e.keyCode == 13) {
        //        ProductSalesManager.calculateReturnAmount();
        //        $("#btnPrintInvoice").focus();

        //    }
        //});


    },


    loadPatient: function () {
        //var phone = $("#txtPhone").val();
        //getDataAsync('../PatientInfo/GetPatientByPhone', 'phoneNo:' + phone, function (data) {

        //});
    },
    SearchByInvoiceNo: function (invoiceNo) {
        var data = ProductSalesManager.GetPatientTestDataByInvoiceNo(invoiceNo);
        var objCustomer = data.Patient;
        var obj = data.PatientTest;
        _payment = obj;
        $("#hdnPatientTestId").val(obj.PatientTestId);
        $("#cmbRefdBy").data('kendoComboBox').value(obj.ReferredBy);
        $("#txtInvoiceNo").val(obj.InvoiceNo);
        $("#dateTimeSalesDate").data('kendoDatePicker').value(obj.SalesDate);
        $("#txtTotal").data('kendoNumericTextBox').value(obj.TotalAmount);
        $("#txtDiscount").data('kendoNumericTextBox').value(obj.DiscountAmount);
        $("#txtNetPay").data('kendoNumericTextBox').value(obj.NetAmount);
        $("#txtAdvanceAmount").data('kendoNumericTextBox').value(obj.PaidAmount);
        $("#txtDueAmount").data('kendoNumericTextBox').value(obj.DueAmount);
        $("#txtdateDelivery").data('kendoDateTimePicker').value(obj.DeliveryDate);
        $("#txtRemarks").val(obj.Remarks);


        $("#hdnPatientId").val(objCustomer.PatientId);
        $("#txtCustomerName").val(objCustomer.PatientName);
        $("#cmbPateintSex").data().kendoComboBox.text(objCustomer.Sex);
        $("#txtPatientAge").val(objCustomer.Age);
        $("#txtPhone").val(objCustomer.ContactNo);


        var grid = $("#gridProductSalesDetails").data('kendoGrid');
        var gridDS = ProductSalesDetailsManager.gridDataSource(data.PatientTestDetails);
        grid.setDataSource(gridDS);

    },
    calculatePayment: function () {
        var dueAmt = 0, advAmt = 0, paidAmt = 0, netPay = 0, rcvAmt = 0, discunt = 0, totalAmt = 0;
        debugger;
        discunt = parseInt($("#txtDiscount").data('kendoNumericTextBox').value());
        totalAmt = parseInt($("#txtTotal").data('kendoNumericTextBox').value());
        netPay = parseInt($("#txtNetPay").data('kendoNumericTextBox').value());
        rcvAmt = parseInt($("#txtReceiveAmount").data('kendoNumericTextBox').value());
        //advAmt = parseInt($("#txtAdvanceAmount").data('kendoNumericTextBox').value());
        //dueAmt = parseInt($("#txtDueAmount").data('kendoNumericTextBox').value());


        var id = $("#hdnPatientTestId").val();
        if (id > 0) {
            var diff = (discunt - _payment.DiscountAmount);
            if (_payment.DueAmount - diff >= rcvAmt) {


                dueAmt = (_payment.DueAmount - rcvAmt);
                advAmt = _payment.PaidAmount + rcvAmt;
                $("#txtDueAmount").data('kendoNumericTextBox').value(dueAmt - diff);
                $("#txtAdvanceAmount").data('kendoNumericTextBox').value(advAmt);


            } else {

                $("#txtDueAmount").data('kendoNumericTextBox').value(0);
                $("#txtReceiveAmount").data('kendoNumericTextBox').value(_payment.DueAmount - diff)
            }


        } else {
            if (netPay >= rcvAmt) {
                dueAmt = (netPay - rcvAmt);

                $("#txtDueAmount").data('kendoNumericTextBox').value(dueAmt);
                $("#txtAdvanceAmount").data('kendoNumericTextBox').value(rcvAmt);


            } else {
                $("#txtDueAmount").data('kendoNumericTextBox').value(0);
                $("#txtReceiveAmount").data('kendoNumericTextBox').value(netPay)
            }


        }

    }
}