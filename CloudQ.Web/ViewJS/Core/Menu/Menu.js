﻿
var MenuManager = {
    init: function () {
        MenuManager.MenuGrid();
        _km.initWindow('wndMenu', 'Customer Information Form');
        var wnd = $("#wndMenu").data('kendoWindow');

        $("#btnSaveChangesMenu").click(function () {
            if (MenuManager.isValidation()) {
                $('#divMenu').saveTo("../Menu/SaveMenu", function (response) {
                    showSuccessMessage(response.Message);
                    if (response.Status) {

                        $("#gridMenu").data('kendoGrid').dataSource.read();
                    }
                });
            }
        });

        $("#btnAddMenu").click(function () {

            wnd.open().center();

        });

        $("#btnCloseWindowMenu").click(function () {
            wnd.close();
        });
        $("#btnClearMenu").click(function () {
            _ah.clearAllFields();
        });

    },
    MenuGrid: function () {

        _km.initGrid_DS("gridMenu", MenuManager.GenerateMenuColumns(), '../Menu/MenuGridDataSource');
    },
    GenerateMenuColumns: function () {
        return columns = [
        { field: "MenuName", title: "Menu Name", width: 100 },
        { field: "MenuPath", title: "Menu Path", width: 100 },
        { field: "ParentMenu", title: "Parent Menu", width: 100 },
        { field: "IsActive", title: "IsActive", width: 100 },
            { field: "Edit", title: "Edit Menu", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="MenuManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridMenu").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);
            var wnd = $("#wndMenu").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divMenu", selectedItem);
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divMenu")) {
            return true;
        }
        return false;
    }
}