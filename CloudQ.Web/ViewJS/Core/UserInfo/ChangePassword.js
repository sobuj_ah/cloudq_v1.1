﻿var wnd;
var baseUrl = "../UserInfo/";

$(document).ready(function () {

    UserInfoManager.init();
})

var UserInfoManager = {
    init: function () {
        //UserInfoManager.UserInfoGrid();
      
        //_km.initWindow('wndUserInfo', 'User Information');
        //wnd = $("#wndUserInfo").data('kendoWindow');

        $("#btnReset").click(function () {
            //if (UserInfoManager.isValidation()) {
            $('#divResetPass').saveTo(baseUrl + "ChangePassword", function (response) {

                alert(response.Message);
                    //showSuccessMessage(response.Message);
                    //if (response.Status) {
                    //   // $("#gridUserInfo").data('kendoGrid').dataSource.read();
                    //}

                   _ah.clearAllFields();
                });
            //}
        });

        //$("#btnAddUserInfo").click(function () {
        //    wnd.open().center();
        //});

        //$("#btnCloseWindow").click(function () {
        //    wnd.close();
        //});
        //$("#btnClear").click(function () {
        //    _ah.clearAllFields();
        //});
    },

    
    clickEventForEditButton: function () {
        var grid = $("#gridUserInfo").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);
            _ah.populate("#divUserInfo", selectedItem);
            var wnd = $("#wndUserInfo").data('kendoWindow');
            wnd.open().center();
        }
    },

    isValidation: function () {
        if (_km.kendoValidator("divUserInfo")) {
            return true;
        }
        return false;
    }
}