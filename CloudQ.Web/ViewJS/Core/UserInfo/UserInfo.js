﻿ 

var wnd;
var baseUrl = "../UserInfo/";

var UserInfoManager = {
    init: function () {
        UserInfoManager.UserInfoGrid();
        //"html-Id","Entity Id","Entity Id Name","Database table name or Entity Name"
        _ah.GenerateCommonCombo("cmbUserType", "UserTypeId", "UserType", "CQM.CoreUserTypes", "");
      
        _km.initWindow('wndUserInfo', 'User Information');
        wnd = $("#wndUserInfo").data('kendoWindow');

        $("#btnSaveChanges").click(function () {
            if (UserInfoManager.isValidation()) {
                $('#divUserInfo').saveTo(baseUrl + "SaveUserInfo", function (response) {
                  //  showSuccessMessage(response.Message);

                    if (response.Status) {
                        swal({
                            title: "Saved",
                            text: "User is Saved successfully",
                            type: "success",
                            timer: 1000,
                            showConfirmButton: false
                        });

                        $("#gridUserInfo").data('kendoGrid').dataSource.read();
                        wnd.close();
                    }
                });
            }
        });

        $("#btnAddUserInfo").click(function () {
            wnd.open().center();
        });

        $("#btnCloseWindow").click(function () {
            wnd.close();
        });
        $("#btnClear").click(function () {
            _ah.clearAllFields();
        });
    },

    UserInfoGrid: function () {

        _km.initGrid_DS("gridUserInfo", UserInfoManager.GenerateUserInfoColumns(), baseUrl + 'UsersGridDataSource');
    },
    GenerateUserInfoColumns: function () {
        return columns = [
            { field: "LoginId", title: "LoginId" , width: 100  },
            { field: "UserName", title: "Display Name", width: 100 },
            { field: "UserType", title: "Type", width: 100 },

            { field: "IsActive", title: "Is Active?", width: 100, template: '#:IsActive?"Yes":"NO"#' },
            { field: "Edit", title: "Action", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="UserInfoManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridUserInfo").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);
            debugger;
            _ah.populate("#divUserInfo", selectedItem);
            var wnd = $("#wndUserInfo").data('kendoWindow');
            wnd.open().center();
        }
    },

    isValidation: function () {
        if (_km.kendoValidator("divUserInfo")) {
            return true;
        }
        return false;
    }
}