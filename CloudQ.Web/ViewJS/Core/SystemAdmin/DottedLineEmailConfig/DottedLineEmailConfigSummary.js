﻿

var DottedLineEmailConfigSummaryManager = {


    GenerateDottedLineEmailConfigEmployeeSummaryGrid: function () {
        var url = "../DottedLineEmailConfig/GetDottedLineEmailConfigEmployeeSummaryGrid/";
        var column = DottedLineEmailConfigSummaryHelper.DottedLineHrRecordsColumns();
        empressCommonManager.GenerateCommonGrid("gridDottedLineEmailConfigEmployeeSummary", url, column);
    },
};

var DottedLineEmailConfigSummaryHelper = {
    initDottedLineEmailConfigSummary: function () {
        DottedLineEmailConfigSummaryManager.GenerateDottedLineEmailConfigEmployeeSummaryGrid();
    },

    InvoiceSummaryGridColumns: function () {

        var columns = [
            { field: "InvoiceId", title: "InvoiceId", width: 50, hidden: true },
            { field: "InvoiceNo", title: "Invoice No", width: 100 },
            { field: "CompanyName", title: "CompanyName", hidden: true },
            { field: "ApproverName", title: "Approver Name", hidden: true },
            { field: "InvoiceDate", title: "Invoice Date", width: 120, template: '#= InvoiceDate == null ? "" : kendo.toString(InvoiceDate,"dd/MM/yyyy") #' },
            { field: "PurchaseOrderDate", title: "PurchaseOrderDate", width: 130, template: '#= PurchaseOrderDate == null ? "" : kendo.toString(PurchaseOrderDate,"dd/MM/yyyy") #' },
            { field: "Name", title: "Contact Name", width: 120, hidden: true },
            { field: "Address", title: "Address", width: 10, hidden: true },
            { field: "SubTotal", title: "SubTotal (BDT)", width: 100 },
            { field: "TaxRate", title: "TaxRate", width: 100 },
            { field: "TaxAmount", title: "TaxAmount (BDT)", width: 100 },
            { field: "Discount", title: "Discount (BDT)", width: 100 },
            { field: "Total", title: "Total (BDT)", width: 100 },
            { field: "MotherCompanyName", title: "ChkPayTo", width: 150 },
            { field: "Edit", title: "Edit", filterable: false, width: 80, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="InvoiceSummaryHelper.clickEventForInvoiceInfoEdit()"  />', sortable: false },

        ];

        return columns;
    },

    DottedLineHrRecordsColumns: function () {
        var columns = [
            { field: "HrRecordId", title: "HRRecordId", hidden: true },
            { field: "ProfilePicture", title: "Photo", width: 30, hidden: false, template: '#= DottedLineEmailConfigSummaryHelper.GenerateProfilePicture(data) #', sortable: false, filterable: false, },
            { field: "EmployeeId", title: "Emp ID", width: 70 },
            { field: "FullName", title: "Name", width: 100 },
            { field: "BranchName", title: "Location", width: 70, hidden: false },
            { field: "DivisionName", title: "Division", width: 70, hidden: false },
            { field: "DepartmentName", title: "Department", width: 70, hidden: false },
            { field: "CompanyName", title: "Company Name", width: 100, hidden: false },
            { field: "BranchName", title: "Branch Name", width: 100, hidden: true },
            { field: "CurrentPosition", title: "Designation", width: 100 },
            { field: "EmploymentDate", title: "Employment<br>Date", width: 70, hidden: true, template: '#= kendo.toString(EmploymentDate,"dd/MM/yyyy") #' },
            { field: "Edit", title: "#", filterable: false, width: 40, template: '<input type="button" class="k-button" value="View" id="btnEdit" onClick="DottedLineEmailConfigSummaryHelper.clickEventForEditButtonForDottedLine()"  />', sortable: false }

        ];

        return columns;
    },

    GenerateProfilePicture: function (data) {
        var pathImg = "";
        if (data.ProfilePicture != "") {
            pathImg = data.ProfilePicture;
        } else {
            if (data.Gender == 1) {
                pathImg = '../Images/male.png';
            } else {
                pathImg = '../Images/female.png';
            }
        }

        return "<img id=\"imgProfilePicturegrid\" alt='Photo' src=\"" + pathImg + "\" style=\"height:50px; width:50px; border-radius:150px;-webkit-border-radius:150px; -moz-border-radius: 150px;box-shadow:0 0 8px rgba(0, 0, 0, .8); -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8); -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8; \" /> ";
    },

    clickEventForEditButtonForDottedLine: function () {

        var entityGrid = $("#gridDottedLineEmailConfigEmployeeSummary").data("kendoGrid");
        var selectedItem = entityGrid.dataItem(entityGrid.select());
        if (selectedItem != null) {
            DottedLineEmailConfigHelper.FillDottedLineEmailConfigForm(selectedItem);
        }

        $("#divDottedLineEmailConfigEmployeeSummary").hide();
        $("#divDottedLineEmailConfigEmployeeDetails").show();

    },
};