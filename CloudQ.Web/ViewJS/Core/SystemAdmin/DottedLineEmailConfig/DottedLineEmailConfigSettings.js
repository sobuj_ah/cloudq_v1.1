﻿$(document).ready(function () {

    AjaxManager.initPopupWindow("windCompanyLocation", 'Company/Location', "60%");
    DottedLineEmailConfigHelper.InitDottedLineEmailConfig();
    DottedLineEmailConfigSummaryHelper.initDottedLineEmailConfigSummary();

    $("#btnRemoveAll").click(function () {

        DottedLineEmailConfigHelper.clickEventForRemoveAll();
    });


    $("#btnSave").click(function () {
        DottedLineEmailConfigManager.SaveDottedLineEmailConfig();
    });
    $("#btnClearAll").click(function () {
        DottedLineEmailConfigHelper.clearFields();
    });


    $("#txtEmployeeId").keypress(function (e) {
        if (e.keyCode == 13) {
            DottedLineEmailConfigHelper.PopulatePreviousSettingsForEmployee();
        }
    });
    $("#btnRemoveAll").hide();

    $("#btnAddNewDottedLineSettings").click(function () {
        DottedLineEmailConfigHelper.AddNewDottedLineForm();
    });

    $("#btnCloseDottedLineSettings").click(function () {
        DottedLineEmailConfigHelper.CloseDottedLineForm();
    });
});