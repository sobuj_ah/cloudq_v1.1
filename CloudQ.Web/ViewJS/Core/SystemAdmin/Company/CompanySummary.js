﻿var companySummaryManager = {
    
    GenerateCompanyGridSummary: function () {
        var url = "../Company/LoadAllCompanies/";
        var column = companySummaryHelper.GenerateCompanyColumns();
        empressCommonManager.GenerateCommonGridWithPaging("gridCompany", url, column,50);
    }, 
};

var companySummaryHelper = {
    
    GenerateCompanyColumns: function () {
        return columns = [
            { field: "CompanyCode", title: "Company Code", width: 100 },
            { field: "CompanyName", title: "Company Name", width: 100 },
            { field: "Phone", title: "Phone", width: 100 },
            { field: "Email", title: "Email", width: 100 },
            { field: "CompanyId", hidden: true },
            { field: "Address", hidden: true },
            { field: "Fax", hidden: true },
            { field: "ShortLogoPath", hidden: true },
            { field: "FullLogoPath", hidden: true },
            { field: "PrimaryContact", hidden: true },
            { field: "FiscalYearStart", hidden: true },
            { field: "MotherId", hidden: true },
            { field: "Edit", title: "Edit Company", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="companySummaryHelper.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    
    clickEventForEditButton: function () {
        companyDetailsHelper.clearCompanyForm();
        $("#btnSave").text("Update");
        var entityGrid = $("#gridCompany").data("kendoGrid");
        var selectedItem = entityGrid.dataItem(entityGrid.select());
        if (selectedItem != null) {
            companyHelper.FillCompanyDetailsInForm(selectedItem);
            var objLocationList = empressCommonManager.GenerateBranchCombo(selectedItem.CompanyId);
            var objDepartmentList = empressCommonManager.GetDepartmentByCompanyId(selectedItem.CompanyId);
            var objDesignationList = empressCommonManager.GenerateDesignationCombo(selectedItem.CompanyId);
            var objDivisionList = empressCommonManager.GetDivisionByCompanyId(selectedItem.CompanyId);
            var objFacilityList = empressCommonManager.GetAllActiveFacility();
         
            branchSolutionHelper.PopulateBranchArray(objLocationList);
            departmentSolutionHelper.PopulateDepartmentArray(objDepartmentList);
            designationSolutionHelper.PopulateDessignationArray(objDesignationList);
            DivisionSolutionHelper.PopulateDivisionArray(objDivisionList);
            FacilitySolutionHelper.PopulateFacilityArray(objFacilityList);
            companyDetailsHelper.showHideSbuComanySummaryEdit();
            DivisionSolutionManager.GenerateDivisionDeptMapSolutionGrid(selectedItem.CompanyId);
        }

    }
};