﻿
var RoleManager = {
    init: function () {
        RoleManager.RoleGrid();
        _km.initWindow('wndRole', 'Role Form');
        var wnd = $("#wndRole").data('kendoWindow');

        $("#btnSaveChangesRole").click(function () {
            if (RoleManager.isValidation()) {
                $('#divRole').saveTo("../Role/SaveRole", function (response) {
                    showSuccessMessage(response.Message);
                    if (response.Status) {

                        $("#gridRole").data('kendoGrid').dataSource.read();
                    }
                });
            }
        });

        $("#btnAddRole").click(function () {

            wnd.open().center();

        });

        $("#btnCloseWindowRole").click(function () {
            wnd.close();
        });
        $("#btnClearRole").click(function () {
            _ah.clearAllFields();
        });

    },
    RoleGrid: function () {

        _km.initGrid_DS("gridRole", RoleManager.GenerateRoleColumns(), '../Role/RoleGridDataSource');
    },
    GenerateRoleColumns: function () {
        return columns = [
        { field: "RoleId", title: "Role Id", width: 100 , hidden:true},
        { field: "RoleName", title: "Role Name", width: 100 },
        { field: "IsActive", title: "IsActive", width: 100 },
            { field: "Edit", title: "Edit Role", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="RoleManager.clickEventForEditButton()"  />', sortable: false }
        ];
    },
    clickEventForEditButton: function () {
        var grid = $("#gridRole").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            // companyManager.FillCompanyForm(selectedItem);
            var wnd = $("#wndRole").data('kendoWindow');
            wnd.open().center();
            _ah.populate("#divRole", selectedItem);
        }
    },
    isValidation: function () {
        if (_km.kendoValidator("divRole")) {
            return true;
        }
        return false;
    }
}