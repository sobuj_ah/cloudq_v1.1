﻿var image = new Image();
var mnManager = {

    Logoff: function () {
        var serviceURL = "../Home/Logoff";
        window.location.href = serviceURL;
    }
};

var mnHelper = {
    GetMenuInformation: function () {
        var objMenuList = getDataByQS("../Menu/GetMenuByUserPermission/", "");
        mnHelper.populateMenus(objMenuList);
        mnManager.getCurrentUser(true);
    },

    populateMenus: function (menus) {
        var dynamicmenuArray = [];
        var chiledMenuArray = [];
        var parentMenubyMenuId = [];
        var pathName = window.location.pathname;

        for (var j = 0; j < menus.length; j++) {
            if (menus[j].MenuPath == ".." + pathName) {
                parentMenubyMenuId = getDataByQS("../Menu/GetParentMenuByMenu/", "parentMenuId=" + menus[j].ParentId);

            }
        }

        var menulink = "";

        for (var i = 0; i < menus.length; i++) {
            var haveparentMenu = 0;
            if (menus[i].ParentMenu == null || menus[i].ParentMenu == 0) {
                for (var k = 0; k < parentMenubyMenuId.length; k++) {
                    if (parentMenubyMenuId[k].MenuId == menus[i].MenuId) {
                        haveparentMenu = 1;
                    }
                }
                if (haveparentMenu == 1) {
                    menulink += "<li class='ob-selected-ancestor'>";
                } else {
                    menulink += "<li>";
                }

                if (menus[i].MenuPath == null || menus[i].MenuPath == "") {
                    menulink += menus[i].MenuName;
                }
                else {
                    menulink += "<a href='" + menus[i].MenuPath + "'>" + menus[i].MenuName + "</a>";
                }
                menulink += mnHelper.addchiledMenu(menus[i], menus[i].MenuId, menus, parentMenubyMenuId);

                menulink += "</li>";


            }
        }
        $("#menu").kendoMenu({
            //closeOnClick: false
        });
        var menu = $("#menu").data("kendoMenu");
        menu.append(menulink);
        $("#menu").kendoMenu({

        });

    },
    addchiledMenu: function (objMenuOrginal, menuId, objMenuList, parentMenubyMenuId) {


        var menulink = "<ul>";
        var added = false;
        var haveChildsParentMenu = 0;
        for (var j = 0; j < objMenuList.length; j++) {
            if (objMenuList[j].ParentMenu == menuId) {
                menulink += "<li id=" + objMenuList[j].MenuName + " >";
                if (objMenuList[j].MenuPath == null || objMenuList[j].MenuPath == "") {
                    //menulink += objMenuList[j].MenuName;

                    menulink += "<a href='#'>" + objMenuList[j].MenuName + "</a>";
                }
                else {
                    menulink += "<a href='" + objMenuList[j].MenuPath + "'>" + objMenuList[j].MenuName + "</a>";
                }
                menulink += mnHelper.addchiledMenu(objMenuList[j], objMenuList[j].MenuId, objMenuList);
                menulink += "</li>";
                added = true;
            }
        }
        menulink += "</ul>";
        if (added == false) {
            menulink = "";
        }

        return menulink;
    }
};