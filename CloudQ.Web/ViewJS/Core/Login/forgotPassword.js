﻿$(document).ready(function () {

    $("#btnReset").click(function () {

        _forgotPasshelper.resetPwd();
    });

});

var _forgotPasshelper = {

    resetPwd: function () {
        var emailaddress = $('#txtEmailId').val();
        if ($('#txtEmailId').val() == "") {
            alert("Invalid Email Address!");
            $('#txtEmailId').focus();
            return false;
        }

        var jsonParam = "emailaddress=" + emailaddress;
        //var pathName = window.location.pathname;
        //var pageName = pathName.substring(pathName.lastIndexOf('/') + 1);
        //if (pageName.toLowerCase() == "home" || pageName.toLowerCase() == "login" || pageName.toLowerCase() == "logoff") {
        //    serviceURL = "./ForgetEmail";
        //}
        //else {
        //    serviceURL = "./Home/ForgetEmail";
        //}
        serviceURL = "../Authorization/ForgetPasswordEmail";
        sendData(serviceURL, jsonParam, onSuccess);
        function onSuccess(jsonData) {
            if (jsonData == "Success") {
                alert("Soon you receive an email about your Login Information. Please check your email account!");
              
            }
            else {
                alert(jsonData);
            }
        }
        
    },

}