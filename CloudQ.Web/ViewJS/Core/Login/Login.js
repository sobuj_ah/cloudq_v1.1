﻿

$(document).ready(function () {
    $("#btnSendEmail").click(function () { loginManager.SendEmail(); });

    $("#txtpassword").keypress(function (event) {
        if (event.keyCode == 13) {
            loginManager.LogInToSystem();
        }
    });

    $("#txtLoginId").keypress(function (event) {
        if (event.keyCode == 13) {
            loginManager.LogInToSystem();
        }
    });
    $("#txtLoginId").change(function (event) {
        //loginManager.GetRememberData();
    });

    $("#btnLogin").click(function () {

        loginManager.LogInToSystem();

    });

});


var loginManager = {

    GetRememberData: function () {
        var inputData = AjaxManager.GetSingleObject("../Home/GetRememberData", '');

        $("#txtLoginId").val(inputData.userid);
        $("#txtpassword").val(inputData.password);
        if (inputData.isRemember == "1") {
            $("#chkRememberMe").attr("checked", "checked");
        } else {
            $("#chkRememberMe").removeAttr("checked", "checked");

        }



    },
    btnCancelLogin: function () {
        $("#btnCancelLogin").click(function () {
            loginHelper.hideLoginPanel();
            $(".panel_button").show();
            $("#hide_button").hide();

        });
    },
    btnCancelLogin1: function () {
        $('#dbloginPanel').show();
        $('#dvChangePasswordPanel').hide();
    },
    btnCancelLogin2: function () {
        $('#dbloginPanel').show();
        $('#ForgetPassWorddiv').hide();
    },

    getCurrentUser: function (menuRefresh) {

        var jsonParam = '';
        var pathName = window.location.pathname;
        var pageName = pathName.substring(pathName.lastIndexOf('/') + 1);
        var serviceURL = "../Home/GetCurrentUser";

        AjaxManager.GetJsonResult(serviceURL, jsonParam, false, false, onSuccess, onFailed);
        //AjaxManager.SendJson(serviceURL, jsonParam, onSuccess, onFailed);
        function onSuccess(jsonData) {

            CurrentUser = jsonData;
            if (CurrentUser != undefined) {

                var userName = "Welcome " + CurrentUser.UserName;
                $("#lblWelcome").html(userName);
                if (CurrentUser.FullLogoPath != null) {
                    $("#headerLogo").attr('style', 'background-image: url("' + CurrentUser.FullLogoPath + '") !important');
                }


            }

        }
        function onFailed(error) {
            window.alert(error.statusText);
        }
    },

    LogInToSystem: function () {
        CurrentUser = null;
        var isRememberMe = $("#chkRememberMe").is(":checked");

        var lType = $("#loginType").val();

        var logonId = $("#txtLoginId").val();
        var pass = $("#txtpassword").val();

        if (logonId == "") {
            alert("Please enter Login ID!");
            $("#txtLoginId").focus();
            return;
        }
        if (pass == "") {
            alert("Please enter Password!");
            $("#txtpassword").focus();
            //jQuery.unblockUI({ message: '' });
            return;
        }

        //   $.blockUI({message: $('#divBlockMessage')});
        var pathName = window.location.pathname;
        var pageName = pathName.substring(pathName.lastIndexOf('/') + 1);

        var jsonParam = 'loginId=' + logonId + '&password=' + pass + '&IsKeepLogged=' + isRememberMe + "&loginType=" + lType;

        var serviceURL = "../Authorization/AuthorizeLogin";

        sendData(serviceURL, jsonParam, onSuccess);
        function onSuccess(jsonData) {
            if (jsonData.Status) {
                if (lType == "Patient") {
                    window.location.href = "../Home/Patient";

                } else {
                    window.location.href = "../Home/Index";

                }
            } else {
                alert(jsonData.Message);
            }
        }
        
    },
    getLeaveInfoForLogin: function () {
        var jsonParam = "";
        var url = "../Leave/SelectAllLeaveBalanceForDashBoardWhileLogin";
        AjaxManager.SendJson(url, jsonParam, onSuccess, onFailed);

        function onSuccess(data) {

            loginHelper.populateLeaveCombo(data);
        }

        function onFailed(error) {
            window.alert(error.statusText);
        }
    },
   

    resetPassword: function () {
        var loginId = $('#txtLoginIdForResetPassword').val();
        var oldpass = $('#txtoldPass').val();
        var password = $('#txtnewPass').val();
        var confirmpass = $('#txtResetConfirmPass').val();

        if (loginId == "") {
            alert("Login ID cannot be blank!");
            $('#txtLoginId').focus();
            return false;
        }

        if (oldpass == password) {
            alert("New password must have to be different from old password!");
            $('#txtnewPass').val('');
            $('#txtResetConfirmPass').val('');
            $('#txtnewPass').focus();
            return false;
        }

        if (password != confirmpass) {
            alert("Password does not match");
            $('#txtResetConfirmPass').val('');
            $('#txtconfirmPass').focus();
            return false;
        }

        var jsonParam = "loginId=" + loginId + "&oldpassword=" + oldpass + "&newpassword=" + confirmpass;
        var serviceURL = "../Home/ResetUserPassword";
        AjaxManager.SendJson(serviceURL, jsonParam, onSuccess, onFailed);
        function onSuccess(jsonData) {

            //var js = jsonData.split('"');
            if (jsonData == "Success") {
                AjaxManager.MsgBox('success', 'center', 'Reset Successfull', 'Password reset successfully, Thank you!',
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            window.location.href = "../Home/Login";
                        }
                    }]);
                // alert("Password reset successfully, Thank you!");
                //                window.location.href = "../Home/Login";

            }
            else {
                AjaxManager.MsgBox('error', 'center', 'Failed', jsonData,
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                        }
                    }]);
            }
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Failed', error,
                [{
                    addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }
    },

    ChangePassword: function () {
        var password = $('#txtnewPass').val();
        var confirmpass = $('#txtconfirmPass').val();

        if (password != confirmpass) {
            alert("Password doesnot match");
            $('#txtconfirmPass').val('');
            $('#txtconfirmPass').focus();
            return false;
        }

        var jsonParam = "password=" + password;
        var serviceURL = "../Home/ChangePassword";
        AjaxManager.SendJson(serviceURL, jsonParam, onSuccess, onFailed);
        function onSuccess(jsonData) {

            //var js = jsonData.split('"');
            if (jsonData == "Success") {


                AjaxManager.MsgBox('success', 'center', 'Success:', 'Password Change Successfully',
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            var url = "../Home/Login";
                            window.location.href = url;
                        }
                    }]);



            }
            else {
                AjaxManager.MsgBox('error', 'center', 'Failed', jsonData,
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                        }
                    }]);
            }
        }
        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Failed', error,
                [{
                    addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                        $noty.close();
                    }
                }]);
        }
    },
    HomeIndex: function () {
        window.location.href = "../Home/Login";
    },
    getClientList: function (oldData) {
        var jsonParam = "companyId=" + CurrentUser.CompanyID;
        var url = "../Client/GetClientList";
        AjaxManager.SendJson(url, jsonParam, onSuccess, onFailed);
        $.blockUI({ message: $('#divBlockMessage') });
        function onSuccess(jsonData) {
            $.unblockUI();
            if (jsonData != "") {

                loginHelper.populateClientCombo(jsonData, oldData);
            }
        }

        function onFailed(error) {
            AjaxManager.MsgBox('error', 'center', 'Login Failed', error.statusText,
                [{
                    addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                        $noty.close();
                        $.unblockUI();
                    }
                }]);
        }
    },
    logMovementForLate: function () {
        $.blockUI({ message: $('#divBlockMessage') });
        if ($("#cmbMovementType").val() == "3") {
            if ($("#cmbClient").val() == "0") {
                alert("Please select a client!");
                return false;
            }
        }
        if ($("#cmbMovementType").val() == "7") {
            if ($("#cmbLeaveType").val() == "0") {
                alert("Please select a Leave Type!");
                return false;
            }

            var leaveType = parseInt($("#cmbLeaveType").val());

            //if (leaveType != "5") {
            for (var i = 0; i < balanceArr.length; i++) {
                if (leaveType == balanceArr[i].LeaveType) {
                    if (balanceArr[i].LeaveWithoutPay == false) {
                        if (balanceArr[i].ClosingLeaveBalance <= 0) {
                            alert("You don't have enough leave balance");
                            return false;
                        }
                    }
                }
            }
            // }

        }


        var jsonParam = "";
        var serviceURL = "";

        if ($("#cmbMovementType").val() == "6") {
            //Page.closeMovementPopup();
            //window.location.href = "../ZResource.mvc/MyeOffice";

            var remarksOriginal = $("#txtAreaRemarks").val();
            var remarks = remarksOriginal.replace(/&/g, '^');

            jsonParam = "remarks='" + remarks + "'&userId=" + CurrentUser.UserId;
            serviceURL = "../Home/UpdateAttendanceRemarks";

        } else {
            var objMovementLog = new Object();
            objMovementLog.UserId = CurrentUser.UserId;
            //objMovementLog.MovementDate = AjaxManager.changeFormattedDate($("#txtAtdAdjDate").val(), "MMDDYYYY");
            objMovementLog.MovementDate = $("#txtAtdAdjDate").val();
            objMovementLog.Status = "0";
            objMovementLog.MovementType = $("#cmbMovementType").val();
            objMovementLog.Remarks = $("#txtAreaRemarks").val();
            objMovementLog.ExpectedReturnTime = "";
            objMovementLog.ProjectCode = "";
            objMovementLog.IsBackToOffice = "True";
            objMovementLog.IsApproved = "False";
            if ($("#cmbClient").val() == "0" || $("#cmbClient").val() == "") {
                objMovementLog.ClientCode = "";
                objMovementLog.ClientName = "";
            } else {
                objMovementLog.ClientCode = $("#cmbClient").val();
                objMovementLog.ClientName = $("#cmbClient :selected").text().replace('&', '^');
            }
            objMovementLog.ConvenceAmount = "0";
            objMovementLog.LeaveType = $("#cmbLeaveType").val();

            jsonParam = "movementLog=" + JSON.stringify(objMovementLog);
            serviceURL = "../Home/LogMovementForLate";
        }
        AjaxManager.SendJson(serviceURL, jsonParam, onSuccess, onFailed);
        function onSuccess(jsonData) {
            $.unblockUI();
            if (jsonData == "Success") {
                AjaxManager.MsgBox('success', 'center', 'Success:', 'Your movement has been logged!',
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            $.unblockUI();
                            loginHelper.closeMovementPopup();
                            window.location.href = "../Dashboard/Dashboard";
                            // noty({ dismissQueue: false, force: false, layout: layout, theme: 'defaultTheme', text: 'You clicked "Ok" button', type: 'success' });
                        }
                    }]);




            } else {
                AjaxManager.MsgBox('error', 'center', 'Login Failed', jsonData,
                    [{
                        addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                            $noty.close();
                            loginHelper.closeMovementPopup();
                        }
                    }]);

            }
        }

        function onFailed(error) {

            AjaxManager.MsgBox('error', 'center', 'Login Failed', error.statusText,
                [{
                    addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                        $noty.close();
                        $.unblockUI();
                    }
                }]);
        }

    }

};

var loginHelper = {
    errorMessage: function (error) {
        AjaxManager.MsgBox('error', 'center', 'Error:', error.statusText,
            [{
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                }
            }]);
    },

    LogInButtonOnOff: function () {
        $("div.panel_button").click(function () {
            //$("#txtLoginId").val('');
            $("#txtLoginId").focus();
            //$("#txtpassword").val('');

            $("div#panel").animate({
                height: "500px"
            })
                .animate({
                    height: "100%"
                }, "fast");
            $("div.panel_button").toggle();
            $("#dbloginPanel").show();

        });
        $("div.hide_button").click(function () {
            loginHelper.hideLoginPanel();
        });
        loginManager.btnCancelLogin();
    },
    hideLoginPanel: function () {
        $("div#panel").animate({
            height: "0px"
        }, "fast");
        $("#ForgetPassWorddiv").hide();
    },

    forgetPassword: function () {
        $('#dbloginPanel').hide();
        $('#ForgetPassWorddiv').show();
    },
    loadMovementPopup: function () {
        AjaxManager.centerPopup('#divMovementPopup');
        $("#backgroundPopup").css({ "opacity": "0.7" });
        $("#backgroundPopup").fadeIn("slow");
        $("#divMovementPopup").fadeIn("slow");
        $("#txtAtdAdjDate").val(AjaxManager.changeFormattedDate(new Date(), "MMDDYYYY"));
    },

    populateLeaveCombo: function (jsonData) {
        balanceArr = [];

        var link = '<option value="0">Select a Leave Type</option>';
        $.each(jsonData, function () {
            //if (this.LeaveType == 1 || this.LeaveType == 2 || this.LeaveType == 3) {
            //if (this.LeaveType == 1 || this.LeaveType == 2) {
            link += '<option value=\"' + this.LeaveType + '\" >' + this.TypeName + '</option>'; //+ ' (Balance Remaining: ' + this.ClosingLeaveBalance + ')</option>';
            var obj = new Object();
            obj.LeaveType = this.LeaveType;
            obj.ClosingLeaveBalance = this.ClosingLeaveBalance;
            obj.LeaveWithoutPay = this.LeaveWithoutPay;
            balanceArr.push(obj);
            //}
        });
        //link += '<option value=\"' + 5 + '\" >' + 'Without Pay' + '</option>';
        $("#cmbLeaveType").html(link);
    },

    showHideClientOption: function () {
        var movementValue = $("#cmbMovementType").val();

        if (movementValue == 3) {
            $("#divClientInfo").attr("style", "display:block");
            $("#divLeaveInfo").attr("style", "display:none");
            loginManager.getClientList("");
        }
        else if (movementValue == 7) {
            $("#divLeaveInfo").attr("style", "display:block");
            $("#divClientInfo").attr("style", "display:none");
        }
        else {
            $("#divClientInfo").attr("style", "display:none");
            $("#divClientInfo").attr("style", "display:none");
        }
    },
    populateClientCombo: function (data, oldData) {
        var link = '<option value="0">Select a client</option>';
        $.each(data, function () {
            link += '<option value=\"' + this.ClientCode + '\" >' + this.ClientName + '</option>';
        });
        $("#cmbClient").html(link);
        if (oldData != "") {
            $("#cmbClient").val(oldData);
        }
    },

    closeMovementPopup: function () {
        $("#txtAtdAdjDate").val('');
        $("#cmbClient").val('');
        $("#txtAreaRemarks").val('');
        AjaxManager.disablePopup("#divMovementPopup", "#backgroundPopup");
    },

};