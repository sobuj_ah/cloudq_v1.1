﻿

var UploadPictureManager = {
    
    init: function () {

        UploadPictureManager.PictureUploadGrid();
        UploadPictureManager.PictureUpload();
        _km.initWindow('wndUploadPicture', 'Add new picture', false);

        
        $("#btnSaveChangesPicture").click(function () {
            if (UploadPictureManager.isValidation()) {
                $('#divPictureUpload').saveTo("../PictureUpload/SaveUploadedPicture", function (response) {
                    showSuccessMessage(response.Message);
                    if (response.Status) {

                        $("#gridPicture").data('kendoGrid').dataSource.read();
                    }
                });
            }
        });

        $("#btnCloseWindowPicture").click(function() {

            var wnd = $("#wndUploadPicture").data('kendoWindow');
            wnd.close();
        });
        
        $("#btnClearPictureFields").click(function () {

            _ah.clearAllFields();
            $("#showProductPicture").attr('src', "");
        });
    },
    
    PictureUploadGrid : function() {
        
        _km.initGrid_DS("gridPicture", UploadPictureManager.GeneratePictureColumns(), '../PictureUpload/ProductPictureGridDataSource');
        
    },
    
    GeneratePictureColumns : function() {
      
        return columns = [

            { field: "PictureId", hidden: true },
            { field: "PicturePath", title: "Picture", width: 100, template: '#= UploadPictureManager.GeneratePicture(data) #' },
            { field: "PictureCode", title: "Picture Code", width: 100 },
            { field: "Edit", title: "Action", filterable: false, width: 60, template: '<input type="button" class="k-button" value="Edit" id="btnEdit" onClick="UploadPictureManager.clickEventForEditButton()"/>&nbsp; &nbsp;<input type="button" class="k-button" value="Select" id="btnSelect" onClick="UploadPictureManager.clickEventForSelectButton()"/>', sortable: false },
        ];
    },
    
    GeneratePicture: function(data) {
        return "<img  src='" + data.PicturePath + "' height='42' width='42' />";
    },

    clickEventForSelectButton: function() {

        var grid = $("#gridPicture").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem !=null) {
            
            $("#hdnPictureId").val(selectedItem.PictureId);
            $("#showProductPicture").attr('src', selectedItem.PicturePath);
        }
        var wnd = $("#wndUploadPicture").data('kendoWindow');
        wnd.close();

    },

    clickEventForEditButton:function() {
      
        var grid = $("#gridPicture").data('kendoGrid');
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {

            _ah.populate("#divPictureUpload", selectedItem);
            $("#showPicture").attr('src', selectedItem.PicturePath);
            
        }
    },
    
    getPictureData: function () {

        var jsonParam = "";
        var serviceUrl = "../PictureUpload/GetPicture/";
        sendObject(serviceUrl, jsonParam, function (jsonData) {
            if (jsonData != "") {
                $("#showPicture").attr('src', "");
                $("#showPicture").attr('src', jsonData);
            }
        });
        
    },
    
    PictureUpload: function () {
        
        $("#Picturefiles").kendoUpload({
            upload: onUpload,
            multiple: false,
            success: onSuccess,
            error: onError,
            select: onSelect,
            async: {
                saveUrl: "../PictureUpload/PictureSave",
                removeUrl: "../PictureUpload/PictureRemove",
                autoUpload: true
            },
            localization: {
                select: "Browse Picture",
            }
        });

        function onUpload(e) {

            // Array with information about the uploaded files
            var files = e.files;

            //// Check the extension of each file and abort the upload if it is not .jpg
            $.each(files, function () {
                if ((this.extension != ".jpg") && (this.extension != ".png") && (this.extension != ".gif") && (this.extension != ".JPG") && (this.extension != ".PNG") && (this.extension != ".GIF")) {
                    alert("Only .jpg/.png files can be uploaded as Profile picture.");
                    e.preventDefault();
                }
            });
        }

        function onSuccess(e) {

            // Array with information about the uploaded files

            var files = e.files;
            if (e.operation == "upload") {

                UploadPictureManager.getPictureData();
            }
        }

        function onError(e) {

            // Array with information about the uploaded files
            var files = e.files;

            if (e.operation == "upload") {
                alert("Failed to uploaded " + files.length + " files");
            }
        }

        function onSelect(e) {
            // Array with information about the uploaded files

        }
    },
    
    isValidation: function () {
        if (_km.kendoValidator("divPictureUpload")) {
            return true;
        }
        return false;
    }
}