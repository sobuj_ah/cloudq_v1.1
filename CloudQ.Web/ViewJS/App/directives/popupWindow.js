﻿(function () {
    var dir = app.directive("popupWindow", function () {

        var directive = {
            restrict: "AE",
            //scope: {
            //    show: '=',
            //    title: '@',
            //    onClose: '&?',
            //    ngModel: '='
            //},
            template: "",
            link: function (scope, elem, attrs) {
                init(attrs.title);
            }

        };
              
        return directive;
    });



}).call(this);

function init(title) {
    $("popup-Window").kendoWindow({
        width: "50%",
        height: '80%',
        resizable: true,
        title: title,
        actions: ["Pin", "Refresh", "Maximize", "Close"],
        modal: true,
        visible: false,

    });
}