﻿(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);


//var frm = $(document.myform);
//var data = JSON.stringify(frm.serializeObject());
(function ($) {

    $.fn.serializeObject = function () {
       
        var o = {};

        //    var a = this.serializeArray();
        $(this).find('input[type="hidden"], input[type="text"], input[type="email"], input[type="password"], input[type="textarea"],input[data-role="combobox"],input[class="k-combobox"],input[class="k-dropdownlist"], [type="checkbox"]:checked, input[type="radio"]:checked, select,textarea').each(function () {

            if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                var $parent = $(this).parent();
                var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                if ($chb != null) {
                    if ($chb.prop('checked')) return;
                }
            }
            if (this.name === null || this.name === undefined || this.name === '')
                return;
            var elemValue = null;
            var c = $(this);
            
            if ($(this).is('select'))
                elemValue = $(this).find('option:selected').val();
            else if (this.id.substr(0, 3) == 'cmb')
                elemValue = c.data('kendoComboBox').value();
            else if (this.id.substr(0, 2) == 'dt')
                elemValue = c.data('kendoDatePicker').value();
            else if (this.id.substr(0, 2) == 'ch')
                elemValue = c.is(":checked") == true ? 1 : 0;
            else elemValue = this.value;
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(elemValue || '');
            } else {
                o[this.name] = elemValue || '';
            }
        });
        return o;
    }
})(jQuery);

(function ($) {

    $.fn.getFormData = function () {

        var o = {};
       
        //    var a = this.serializeArray();
        $(this).find('input[type="hidden"], input[type="text"], input[type="email"], input[type="password"], input[type="textarea"],input[class="k-combobox"],input[data-role="combobox"],input[data-role="dropdownlist"],input[class="k-dropdownlist"], input[type="checkbox"]:checked, input[type="radio"]:checked, select,textarea').each(function () {

            if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
                var $parent = $(this).parent();
                var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
                if ($chb != null) {
                    if ($chb.prop('checked')) return;
                }
            }
            if (this.name === null || this.name === undefined || this.name === '')
                return;
            var elemValue = null;
            var c = $(this);
            
            if ($(this).is('select'))
                elemValue = $(this).find('option:selected').val();
            else if (this.id.substr(0, 3) == 'cmb')
                elemValue = c.data('kendoComboBox').value();
            else if (this.id.substr(0, 2) == 'dt')
                elemValue = c.data('kendoDatePicker').value();
            else if (this.id.substr(0, 2) == 'ch')
                elemValue = c.is(":checked") == true ? 1 : 0;
            else elemValue = this.value;
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(elemValue || '');
            } else {
                o[this.name] = elemValue || '';
            }
        });
        return o;

    };
})(jQuery);


(function () {

    Array.prototype.addObject = function (obj) {

        if (obj == null) {
            throw new TypeError('object is null or not defined');
        }
        return this.push(obj);
    };
    Array.prototype.remove = function (obj) {

        if (this.length < 1) {
            throw new TypeError('Array is empty or not defined');
        }
        if (obj == null) {
            throw new TypeError('object is null or not defined');
        }
        var index = this.indexOfArray(obj);

        if (index != -1) {
            this.splice(index, 1);
        }

    };

    Array.prototype.removeObject = function (obj) {

        if (this.length < 1) {
            throw new TypeError('Array is empty or not defined');
        }
        if (obj == null) {
            throw new TypeError('object is null or not defined');
        }
        var dArray = $.grep(this, function (dt) {
            return (dt != obj);
        });

        var index = this.indexOfArray(obj);

        if (index != -1) {
            this.splice(index, 1);
        }

        return dArray;

    };

    Array.prototype.indexOfArray = function (obj) {
        if (obj == null) {
            throw new TypeError('object is null or not defined');
        }
        var index = -1;
        for (var i = 0; i < this.length; i++) {
            if (JSON.stringify(this[i]) == JSON.stringify(obj)) {
                index = i;
                break;
            }
        }
        return index;
    };

})();