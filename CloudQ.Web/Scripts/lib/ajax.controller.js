﻿

(function ($) {
    $.fn.extend({
        saveTo: (function (url, callback) {
            //showProcessingMessage();
            var obj = $(this).serializeObject();

            if (callback != undefined) {
                sendObject(url, "formData:" + JSON.stringify(obj), callback);
            } else {
                sendObject(url, "formData:" + JSON.stringify(obj), function (response) {

                    if (response.Status) {
                        // MessageBox(response)
                        _ah.clearAllFields();
                    } else {
                        //  MessageBox(response)
                    }

                    return data;
                });
            }

        }),

    });


})(jQuery);
(function ($) {
    $.fn.extend({
        saveToApi: (function (url, callback) {
            //showProcessingMessage();

            var obj = $(this).serializeObject();

            if (callback != undefined) {
                PostAPI(url, "formData:" + obj, callback);
            } else {
                PostAPI(url, "formData:" + obj, function (data) {

                    if (data.Status) {
                        // showSuccessMessage(data.Message);
                        _ah.clearAllFields();
                    } else {
                        // showErrorMessage(data.Message);
                    }

                    return data;
                });
            }

        }),

    });


})(jQuery);

function PostAPI(serviceUrl, jsonParams, successCallback) {

    jQuery.ajax({
        url: serviceUrl,
        async: true,
        cache: false,
        type: "POST",
        data: "{" + jsonParams + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: successCallback,
        error: function (x, y, z) {
            showErrorMessage(y.Message);
        }
    });

};
function sendObject(serviceUrl, jsonParams, successCallback) {

    jQuery.ajax({
        url: serviceUrl,
        async: true,
        cache: false,
        type: "POST",
        data: "{" + jsonParams + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: successCallback,
        error: function (x, y, z) {
            //showErrorMessage(y.Message);
            debugger;
            swal({
                title: "Error",
                text: z,
                type: "worning",
                timer: 1000,
                showConfirmButton: false
            });
        }
    });

};
function sendData(serviceUrl, jsonParams, successCallback) {

    jQuery.ajax({
        url: serviceUrl,
        data: jsonParams,
        async: false,
        cache: false,
        type: "POST",
        success: successCallback,
        error: function (x, y, z) {
            alert('internal server error');
        }
    });

};

function reportViewer(serviceUrl, jsonParams) {

    jQuery.ajax({
        url: serviceUrl,
        async: false,
        type: "POST",
        data: "{" + jsonParams + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            window.open('../Report/ReportViewer.aspx', '_blank');

        },
        error: function (x, y, z) {
            showErrorMessage(y.Message);
        }
    });

};
function pdfViewer(serviceUrl, jsonParams) {

    jQuery.ajax({
        url: serviceUrl,
        async: false,
        type: "POST",
        data: "{" + jsonParams + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (url) {
            window.open(url, '_blank');

        },
        error: function (x, y, z) {
            swal("Sorry!", y.statusTex, "error");
        }
    });

};
function getDataByQS(serviceUrl, jsonParams) {
    var obj = new Object();
    $.ajax({
        type: "GET",
        async: false,
        cache: false,
        url: serviceUrl,
        data: jsonParams,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            obj = data;
        },
        error: function (error) {
            alert(error.statusText);
        }
    });
    return obj;
};

function getData(serviceUrl, jsonParams) {
    var obj = new Object();
    $.ajax({
        type: "POST",
        async: false,
        cache: true,
        url: serviceUrl,
        data: "{" + jsonParams + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            obj = data;
        },
        error: function (error) {
            swal("Sorry!", error.statusTex, "error");
            
        }
    });
    return obj;
};

function getDataAsync(serviceUrl, jsonParams,onData) {
    var obj = new Object();
    $.ajax({
        type: "POST",
        async: true,
        cache: true,
        url: serviceUrl,
        data: "{" + jsonParams + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: onData,
        error: function () {
            alert("Internal Server Error");
        }
    });
    return obj;
};

function MessageBox(response) {
    debugger;
    if (response.Status)
        swal("Done!", response.Message, "success");
    else
        swal("Sorry!", response.Message, "error");

}
 
//function confirm(msg,onCallback) {
//    swal({
//        title: "Are you sure?",
//        text: "Once deleted, you will not be able to recover this imaginary file!",
//        icon: "warning",
//        buttons: true,
//        dangerMode: true,
//    })
// .then((willDelete) -> {
//     if (willDelete) {
//     swal("Poof! Your imaginary file has been deleted!", {
//         icon: "success",
//     });
//} else {
//    swal("Your imaginary file is safe!");
//}
//});
//}

