﻿var _am = {

    GenerateCommonCombo: function (value, text, table, by) {

        var jsonParam = "peram=" + value + "," + text + "," + table + "," + by;
        var serviceUrl = "../CommonService/GetCommonComboData";
        return getDataByQS(serviceUrl, jsonParam);


    },
    CreateNextCode: function (pre, field, table) {
        var jsonParam = "pre=" + pre + "&field=" + field + "&table=" + table;
        var serviceUrl = "../CommonService/CreateNextCode";
        return getDataByQS(serviceUrl, jsonParam);


    },
};


var _ah = {
    clearAllFields: function () {
        $("input[type='text']").val("");
        $("input[type='hidden']").val("0");
        $("input[type='email']").val("");
        $("input[type='password']").val("");
        $("textarea").val("");
        $('input:checkbox').removeAttr('checked');
    },
    MsgBox: function (messageBoxType, displayPosition, messageBoxHeaderText, messageText, buttonsArray) {
        var n = noty({
            textHeader: messageBoxHeaderText,
            text: messageText,
            type: messageBoxType,
            modal: true,
            dismissQueue: true,
            layout: displayPosition,
            theme: 'defaultTheme',
            buttons: buttonsArray

        });
        $(".btn-primary").focus();
    },



    GenerateCommonCombo: function (identity, value, text, table, by) {
        var obj = _am.GenerateCommonCombo(value, text, table, by);
        $("#" + identity).kendoComboBox({
            placeholder: "All",
            dataTextField: "Text",
            dataValueField: "Id",
            dataSource: obj
        });
    },

    populate: function (frm, data) {

        $.each(data, function (key, value) {
            var $ctrl = $('[name=' + key + ']', frm);
            if ($ctrl.attr("type") != undefined) {
                switch ($ctrl.attr("type")) {
                    case "text":
                    case "hidden":
                        if ($ctrl.attr('id') != undefined) {


                            if ($ctrl.attr('id').substr(0, 3) === 'cmb') {
                                $ctrl.data('kendoComboBox').value(value);
                            }
                            else if ($ctrl.attr('id').substr(0, 3) === 'ddl') {
                                $ctrl.data('kendoDropDownList').value(value);
                            }
                            else if ($ctrl.attr('id').substr(0, 2) === 'dt') {
                                $ctrl.data('kendoDatePicker').value(value);
                            } else if ($ctrl.attr('id').substr(0, 2) === 'nt') {
                                $ctrl.data('kendoNumericTextBox').value(value);
                            } else {
                                $ctrl.val(value);
                            }
                        }


                        break;
                    case "radio":
                    case "checkbox":
                        $ctrl.each(function () {
                            //if ($(this).attr('value') == value) {
                            //      $(this).attr("checked",value);
                            //}
                            if ($ctrl.attr('id').substr(0, 2) == 'ch') {
                                var val = value == 1 ? true : false;
                                $ctrl.prop("checked", val);
                            }
                        });
                        break;
                    default:
                        $ctrl.val(value);
                }

            } else {
                if ($ctrl.attr('id') != undefined) {
                    if ($ctrl.attr('id').substr(0, 3) === 'cmb') {
                        $ctrl.data('kendoComboBox').value(value);
                    } else if ($ctrl.attr('id').substr(0, 2) === 'dt') {
                        $ctrl.data('kendoDatePicker').value(value);
                    } else if ($ctrl.attr('id').substr(0, 2) === 'nt') {
                        $ctrl.data('kendoNumericTextBox').value(value);
                    } else {
                        $ctrl.val(value);
                    }
                }
            }
        });
    },

    generateAutoIncrementCode: function (ctrl, pre, field, table) {
        var newIdObj = _am.CreateNextCode(pre, field, table);

        if (newIdObj.Status == true) {
            $("#" + ctrl).val(newIdObj.Data);
        }
    },


};