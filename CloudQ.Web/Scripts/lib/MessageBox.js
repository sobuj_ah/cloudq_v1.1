﻿
//$(document).ready(function () {
//    MessageBox.initMesageWindow();
//})
var MessageBox = {
    initMesageWindow: function () {

        // var el = document.createElement("div");
        $("#m-box").kendoWindow({
            width: "30%",
            resizable: false,
            title: "Processing...",
            actions: ["Close"],//"Close","Pin", "Refresh", "Maximize",
            modal: true,
            visible: false,

        });


    },
    start: function () {
        var msgBox = $("#m-box").data('kendoWindow');
        msgBox.open().center();
    },
    success: function (message, callback) {
        var img = $("#loadingImg");
        var msgBox = $("#m-box").data('kendoWindow');
        var div = $("#divMessage");
        img.attr('src', '../Images/checkmark.gif');
        div.html(message);
        setTimeout(function () {
            msgBox.close();
            callback();
        }, 1000);


    },
    confirmation: function (message, callback) {
        var img = $("#loadingImg");
        var msgBox = $("#m-box").data('kendoWindow');
        var div = $("#divMessage");
        img.attr('src', '../Images/checkmark.gif');
        div.html(message);
        setTimeout(function () {
            msgBox.close();
            callback();
        }, 1000);

    },
    warning: function (message, callback) {
        var img = $("#loadingImg");
        var msgBox = $("#m-box").data('kendoWindow');
        var div = $("#divMessage");
        img.attr('src', '../Images/checkmark.gif');
        div.html(message);
        setTimeout(function () {
            msgBox.close();
            callback();
        }, 1000);
    },
    show: function (response, callback) {
        if (response.Status) {
            MessageBox.success(response.Message, callback);
        } else {
            MessageBox.warning(response.Message, callback);
        }

    },


};


function showProcessingMessage(messageText) {
    msgBox.open().center();
    $(".successMessageIndicator").hide();
    $("#processing").show();
    $("#messageText").text("");
    if (messageText != "") {
        $("#messageText").show();
        $("#messageText").text(messageText);
    }
};

function showSuccessMessage(messageText) {
    $(".successMessageIndicator").hide();
    $("#nike").show();
    if (messageText != "") {
        $("#messageText").show();
        $("#messageText").text(messageText);
    }
};
function showErrorMessage(messageText) {
    $(".successMessageIndicator").hide();
    $("#success").hide();
    $("#error").show();
    if (messageText != "") {
        $("#errormessageText").show();
        $("#errormessageText").text(messageText);
    }
};