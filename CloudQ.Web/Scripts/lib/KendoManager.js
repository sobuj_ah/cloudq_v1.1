﻿var _gridId = "";

var _km = {
    setGridOption: function (options) {
        _gridId.data('kendoGrid').setOptions(options);
    },

    initGrid_blank: function (id, columns) {
        var grid = $("#" + id).kendoGrid({
            dataSource: [],
            pageable: {
                refresh: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            filterable: true,
            sortable: true,
            columns: columns,
            editable: false,
            navigatable: true,
            selectable: "row",
        }).data('kendoGrid');
        return grid;

    },
    initGrid_DS: function (id, columns, url, model) {
        _gridId = $("#" + id);
        if (model == undefined) {
            model = {};
        }
        _gridId.kendoGrid({
            dataSource: _km.gridDataSource(url, model),
            pageable: {
                refresh: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            filterable: true,
            sortable: true,
            columns: columns,
            editable: false,
            navigatable: true,
            selectable: "row",
            //scrollable: {
            //    virtual: true
            //},
            //height: 300
        });



    },

    gridDataSource: function (url, model) {
        var gridDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,

            serverSorting: true,

            serverFiltering: true,

            allowUnsort: true,

            pageSize: 5,

            transport: {
                read: {
                    url: url,

                    type: "POST",

                    dataType: "json",

                    async: true,

                    cache: true,

                    contentType: "application/json; charset=utf-8"
                },

                parameterMap: function (options) {

                    return JSON.stringify(options);

                }
            },
            schema: {
                model: model,

                data: "Items", total: "TotalCount"

            }
        });
        return gridDataSource;
    },

    gridDataSource_blank: function (model) {
        var gridDataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,

            serverSorting: true,

            serverFiltering: true,

            allowUnsort: true,

            pageSize: 5,

            schema: {
                model: model,

                //  data: "Items", total: "TotalCount"

            }
        });
        return gridDataSource;
    },

    initWindow: function (ctrId, title, isModal) {

        if (isModal == undefined) {
            isModal = true;
        }
        var wind = $("#" + ctrId).kendoWindow({
            width: "auto",
            maxWidth: '90%',
            minWidth: '30%',
            resizable: true,
            title: title,
            actions: ["Pin", "minimize", "Maximize", "Close"],//"Pin", "Refresh", "Maximize",
            modal: isModal,
            visible: false,
            top: 300


        }).data('kendoWindow');
        return wind;
    },

    initServerWindow: function (ctrId, title, url) {
        $("#" + ctrId).kendoWindow({
            width: "60%",
            resizable: true,
            title: title,
            actions: ["Pin", "minimize", "Maximize", "Close"],//"Pin", "Refresh", "Maximize",
            modal: true,
            content: url,
            visible: false,
            top: 300

        });
    },

    kendoValidator: function (divId) {

        var validator = $("#" + divId).kendoValidator().data("kendoValidator"),
            status = $(".status");

        if (validator.validate()) {
            status.text("").addClass("valid");
            return true;
        } else {
            status.text("Oops! There is invalid data in the form.").addClass("invalid");
            return false;
        }
    },
    datePicker: function (ctrId) {
        $("#" + ctrId).kendoDatePicker({
            value: new Date(),
            format: "dd-MMM-yyyy",
            parseFormats: ['dd.MM.yyyy', 'dd.MM.yy', 'dd/MM/yyyy', 'dd/MM/yy', 'ddMMyyyy', 'ddMMyy']
        });
    },
    dateTimePicker: function (ctrId) {
        $("#" + ctrId).kendoDateTimePicker({
            value: new Date(),
            format: "dd-MMM-yyyy hh:mm tt",
        });
    },
    monthPicker: function (ctrId) {
        $("#" + ctrId).kendoDatePicker({
            value: new Date(),
            depth: "year",
            start: "year",
            format: "MMMM yyyy",
            parseFormats: ['MMyyyy', 'MMyy', 'yyyyMM']
        });
    },
    autoComplete: function (ctrId, field, url, param) {
        $("#" + ctrId).kendoAutoComplete({
            dataTextField: field,
            minLength: 1,
            filter: "startwith",
            //  suggest: true,
            dataSource: {
                type: "json",

                pageSize: 20,

                serverFiltering: true,
                serverPaging: true,
                transport: {
                    read: url,
                    type: "POST",

                    dataType: "json",

                    contentType: "application/json; charset=utf-8"
                },
                //parameterMap: function (options) {

                //    return JSON.stringify(options);

                //}
                parameterMap: function (data, action) {
                    var newParams = {
                        filter: data.filter.filters[0].value
                    };
                    return JSON.stringify(newParams);
                }
            }

        });
    },
    numericTextbox: function (ctrId) {

        var obj = $("#" + ctrId).kendoNumericTextBox({
            min: 0,
            value: 0,
            max: 10000000
        }).data('kendoNumericTextBox'); //numeric text box
        return obj;
    },
    checkedAfterHideShow: function (chkId, divId) {
        $("#" + chkId).change(function () {
            if ($("#" + chkId).is(":checked")) {
                $("#" + divId).show();
            } else {
                $("#" + divId).hide();

            }
        });
    },
    ComboBox: function (identity, value, text, url, jsonParam) {

        $("#" + identity).kendoComboBox({
            placeholder: "Select",
            dataTextField: text,
            dataValueField: value,
            dataSource: getDataByQS(url, jsonParam)
        });
    },
    notification: function (msg) {
        var placementFrom = 'bottom';
        var placementAlign = 'right';
        var animateEnter ='';
        var animateExit ='';
        var colorName = 'alert-warning';

        showNotification(colorName, msg, placementFrom, placementAlign, animateEnter, animateExit);
    }
}

//Kendo Model Sample Code


//var Person = kendo.data.Model.define({
//    id: "personId", // the identifier of the model
//    fields: {
//        "name": {
//            type: "string"
//        },
//        "age": {
//            type: "number"
//        }
//    }
//});


