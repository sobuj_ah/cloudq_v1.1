﻿using System.Web.Optimization;

namespace CloudQ.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/AngularJS").Include(
                //"~/Assets/Scripts/jquery-{version}.js",
                //"~/Assets/Scripts/modernizr-*",
                //"~/Assets/Scripts/respond.js",
                //"~/Scripts/ripples.js",
                //"~/Scripts/material.js",
                //"~/Scripts/jquery.validate.js",
                //"~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/angular.min.js",
                "~/Scripts/angular-route.min.js",
                "~/Scripts/angular-resource.min.js",
                "~/Scripts/angular-animate.min.js",
                "~/Scripts/angular-loader.min.js",
                "~/Scripts/angular-aria.min.js",
                "~/Scripts/angular-cookies.min.js",
                "~/Scripts/angular-message-format.min.js",
                "~/Scripts/angular-messages.min.js",
                "~/Scripts/angular-mocks.min.js",
                "~/Scripts/angular-sanitize.min.js",
                "~/Scripts/angular-scenario.js",
                "~/Scripts/angular-touch.min.js",
                "~/Scripts/i18n/angular-locale_en-us.js",
                //"~/Scripts/ui-bootstrap.js",
                //"~/Scripts/ui-bootstrap-tpls.js",
                "~/App/app.js"
               ));

            bundles.Add(new StyleBundle("~/Assets/Styles/CSS").Include(
                //"~/Scripts/Styles/bootstrap.css",
                "~/Scripts/Styles/roboto.css",
                "~/Scripts/Styles/ripples.css",
                //"~/Scripts/Styles/material.css",
                "~/Scripts/Styles/styles.css"
                ));

            bundles.Add(new StyleBundle("~/fixed-width-light/CSS").Include(
              "~/fixed-width-light/vendors/bower_components/morris.js/morris.css",
              "~/fixed-width-light/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css",
              "~/fixed-width-light/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css",
              "~/fixed-width-light/dist/css/style.css"
              ));
            bundles.Add(new ScriptBundle("~/fixed-width-light/JS").Include(
              "~/fixed-width-light/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js",
              "~/fixed-width-light/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js",
              "~/fixed-width-light/vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js",
              "~/fixed-width-light/vendors/bower_components/jszip/dist/jszip.min.js",
              "~/fixed-width-light/vendors/bower_components/pdfmake/build/pdfmake.min.js",
              "~/fixed-width-light/vendors/bower_components/pdfmake/build/vfs_fonts.js",
              "~/fixed-width-light/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js",
              "~/fixed-width-light/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js",
              "~/fixed-width-light/dist/js/jquery.slimscroll.js",
              "~/fixed-width-light/vendors/bower_components/moment/min/moment.min.js",
              "~/fixed-width-light/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js",
              "~/fixed-width-light/dist/js/simpleweather-data.js",
              "~/fixed-width-light/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js",
              "~/fixed-width-light/vendors/bower_components/jquery.counterup/jquery.counterup.min.js",
              "~/fixed-width-light/dist/js/dropdown-bootstrap-extended.js",
              "~/fixed-width-light/vendors/jquery.sparkline/dist/jquery.sparkline.min.js",
              "~/fixed-width-light/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js",
              "~/fixed-width-light/vendors/chart.js/Chart.min.js",
              "~/fixed-width-light/vendors/bower_components/raphael/raphael.min.js",
              "~/fixed-width-light/vendors/bower_components/morris.js/morris.min.js",
              "~/fixed-width-light/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js",
              "~/fixed-width-light/vendors/bower_components/switchery/dist/switchery.min.js",
              "~/fixed-width-light/dist/js/init.js"
              ));



            //BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            //bundles.Add(new ScriptBundle("~/angular").Include("~/Scripts/angular.min.js"));
            bundles.Add(new ScriptBundle("~/kendo").Include("~/Scripts/kendoLib/kendo/js/kendo.web.min.js", "~/Scripts/lib/KendoManager.js"));


            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                "~/Scripts/lib/formToJson.js", "~/Scripts/lib/ajax.controller.js", "~/Scripts/lib/ApplicationCommon.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/app.css"));

            bundles.Add(new StyleBundle("~/kendo/css").Include(
                "~/Scripts/kendoLib/kendo/styles/kendo.common.min.css",
                "~/Scripts/kendoLib/kendo/styles/kendo.bootstrap.min.css"));


            bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
                "~/Scripts/signalr/jquery.signalR-2.0.0.min.js"));
        }
    }
}
