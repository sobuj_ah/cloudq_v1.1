﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="CloudQ.Web.Report.ReportViewer" %>



<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

 

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
  
<script type="text/javascript">
    function Print() {
        var dvReport = document.getElementById("dvReport");
        var frame1 = dvReport.getElementsByTagName("iframe")[0];
        if (navigator.appName.indexOf("Internet Explorer") != -1) {
            frame1.name = frame1.id;
            window.frames[frame1.id].focus();
            window.frames[frame1.id].print();
        }
        else {
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.print();
        }
    }
</script>


</head>

<body>
    <form id="form1" runat="server">
         <div id="dvReport">
         <%--<input type="button" id="btnPrint" value="Print to Printer" class="k-button" onclick="Print()" />--%>
             
        <hr/>
        <div>
            <CR:CrystalReportViewer ID="CrystalReportViewer" runat="server" HasPrintButton="True" EnableParameterPrompt="False" EnableDatabaseLogonPrompt="False" EnableTheming="False" HasCrystalLogo="False" HasRefreshButton="True" HasToggleParameterPanelButton="False" Height="50px" ShowAllPageIds="True" ToolPanelView="None" Width="350px" OnUnload="CrystalReportViewer_Unload" HasDrillUpButton="False" HyperlinkTarget="" EnableDrillDown="False" HasDrilldownTabs="False" PrintMode="ActiveX"  />
        </div>
             </div>
    </form>
</body>

</html>
 
