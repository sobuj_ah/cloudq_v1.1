﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace CloudQ.Web.Report
{
    public partial class ReportViewer : System.Web.UI.Page
    {

        ReportDocument rd = new ReportDocument();
        private string webPath = "";
        private string physicalPath = "";
        protected void Page_Init(object sender, EventArgs e)
        {

            physicalPath = Server.MapPath("~/");
            LoadReport();

        }


        private void LoadReport()
        {
            bool isValid = true;
            string res = "";
            string reportTitle = "";

            try
            {

                var reportPram = (dynamic)HttpContext.Current.Session["ReportParam"];
                var reportType = reportPram.ReportType;
                reportTitle = reportPram.ReportTitle;
                if (reportPram != null && string.IsNullOrEmpty(reportPram.RptFileName)) // Checking is Report name provided or not
                {
                    isValid = false;
                }


                if (isValid) // If Report Name provided then do other operation
                {
                    Page.Title = "Report | " + reportPram.ReportTitle;

                    reportPram.ReportType = reportType;

                    // Setting report data source
                    if (reportPram.DataSource != null || reportPram.IsDataTableExist == true)
                    {
                        if (reportPram.DataSource.Count > 0)
                        {
                            rd = GenerateReportDocument(reportPram);
                        }
                        else
                        {
                            //reportPram.RptFileName = "NotFoundReport.rpt";
                            //string strRptPath = physicalPath + reportPram.RptFileName;
                            ////Loading Report
                            //rd.Load(strRptPath);
                            //rd = GenerateReportDocument(reportPram);
                            Response.Write("Data Not Found");
                        }

                    }
                    else
                    {

                        reportPram.RptFileName = "NotFoundReport.rpt";
                        string strRptPath = physicalPath + reportPram.RptFileName;
                        //Loading Report

                        rd.Load(strRptPath);
                        //rd = GenerateReportDocument(reportPram);
                    }

                    //rd.SetCssClass(ObjectScope.AllReportObjectsInReportHeaderSections, "ReportHeaderImage");

                    CrystalReportViewer.ReportSource = rd;
                    res = "Report Generated";

                }
                else
                {
                    Response.Write("<H2>Nothing Found; No Report name found</H2>");
                    res = "Nothing Found; No Report name found";
                }



            }
            catch (Exception ex)
            {

                res = ex.Message;
                Response.Write(res);
            }
            finally
            {


            }
        }

        private ReportDocument GenerateReportDocument(dynamic reportPram)
        {

            try
            {
                string strReportName = reportPram.RptFileName;
                var rptSource = reportPram.DataSource;
                //SetReportLogo.SetLogo(rptSource);
                string strRptPath = physicalPath + strReportName;
                rd.Load(strRptPath);

                rd.SetDataSource(rptSource);

                if (reportPram.IsSubReport)
                {
                    SetSubReportDataSource(ref rd, reportPram);
                }

                if (reportPram.IsPassParamToCr)
                {

                    return SetReportParameters(rd, reportPram);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rd;
        }

        private ReportDocument SetReportParameters(ReportDocument rd, dynamic reportPram)
        {
            try
            {

                var reportType = (string)reportPram.ReportType;
                switch (reportType)
                {
                    case "LeaveReport":
                        {
                            //Please set your reports parameter one by one
                            rd.SetParameterValue("@fromDate", reportPram.FromDate);
                            rd.SetParameterValue("@toDate", reportPram.ToDate);
                            break;
                        }




                }
            }
            catch (Exception)
            {
                throw;
            }
            return rd;
        }

        private void SetSubReportDataSource(ref ReportDocument rd, dynamic reportPram)
        {
            var reportType = (string)reportPram.ReportType;
            switch (reportType)
            {

                case "IncomeTaxComputationSheet":
                    rd.Subreports[0].SetDataSource(reportPram.SubReportDataSources.accountHead);
                    rd.Subreports[1].SetDataSource(reportPram.SubReportDataSources.taxParam);
                    break;
            }
        }

        protected void CrystalReportViewer_Unload(object sender, EventArgs e)
        {

            rd.Close();
            rd.Dispose();
            GC.Collect();

        }

        protected void PrintToPDF()
        {
            rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, "report_" + DateTime.Now.ToString("ddMMyyyy"));

        }


    }


    public class SetReportLogo
    {
        public static void SetLogo<T>(List<T> dataList)
        {
            var systemPath = AppDomain.CurrentDomain.BaseDirectory;
            var fullPath = systemPath + "Images/Logo/" + "report-logo.bmp";
            if (Directory.Exists(fullPath))
            {
                byte[] buffer = System.IO.File.ReadAllBytes(fullPath);
                foreach (var data in dataList)
                {
                    var properties = data.GetType().GetProperties();
                    foreach (PropertyInfo prop in from property in properties let prop = property where property.Name == "ReportLogo" select prop)
                    {
                        prop.SetValue(data, buffer);
                    }
                }
            }

        }

        public static void SetLogoForSignatory<T>(List<T> dataList)
        {
            var systemPath = AppDomain.CurrentDomain.BaseDirectory;
            var fullPath = systemPath + "Images/Logo/" + "SignatoryImage.bmp";
            byte[] buffer = System.IO.File.ReadAllBytes(fullPath);
            foreach (var data in dataList)
            {
                var properties = data.GetType().GetProperties();
                foreach (PropertyInfo prop in from property in properties let prop = property where property.Name == "SignatureLogo" select prop)
                {
                    prop.SetValue(data, buffer);
                }
            }
        }
    }
}