﻿using CloudQ.Base.Controllers.Attributes;
using System.Web.Mvc;

namespace CloudQ.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult Patient()
        {
            return View();
        }
        [CustomActionFilter]
        public ActionResult Login()
        {
            ViewBag.UserType = "Admin";
            Session["CurrentUser"] = null;
            return View("_Login");
        }
        public ActionResult LoginPatient()
        {
            Session["CurrentUser"] = null;

            ViewBag.UserType = "Patient";
            return View("_Login");
        }
        public ActionResult LoginDoctor()
        {
            Session["CurrentUser"] = null;

            ViewBag.UserType = "Doctor";

            return View("_Login");
        }
        public ActionResult Expire()
        {
            return View("Expire");
        }
        public ActionResult Forgot()
        {
            return View("ForgotPassword");
        }
    }
}
