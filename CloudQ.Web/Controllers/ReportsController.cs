﻿using System.Web.Mvc;
using CloudQ.Core.Service.Organization.CompanyServices;

namespace CloudQ.Web.Controllers
{
    public class ReportsController : Controller
    {
        public ActionResult InvoicePrint(string invoiceNo)
        {
            IProductSalesService service=new ProductSalesService();

            var reportParam = new BaseReportModel<InvoiceReportView>();
            reportParam.ReportTitle = "Invoice";
            reportParam.RptFileName = "InvoicePrint.rpt";

            reportParam.DataSource = service.GetInvoiceReportData(invoiceNo);

            //If you want to pass any parameter to CR then Enable
            reportParam.IsPassParamToCr = false;
            reportParam.ReportType = "Invoice";

            Session["ReportParam"] = reportParam;
            

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        #region Chalan

        public ActionResult ChalanPrint()
        {
            return null;
        }

        #endregion

        public ActionResult CompanyInfoPrint()
        {
            ICompanyService service = new CompanyService();

            BaseReportModel<CompanyInfoView> report = new BaseReportModel<CompanyInfoView>();
            report.ReportTitle = "ComapnyInfo";
            report.RptFileName = "CompanyInfoPrint.rpt";

            report.DataSource = service.GetCompanyInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "ComapnyInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClientInfoPrint()
        {
            IClientInfoService service = new ClientInfoService();

            var report = new BaseReportModel<ClientInfoView>();

            report.ReportTitle = "ClientInfo";
            report.RptFileName = "ClientInfoPrint.rpt";

            report.DataSource = service.GetClientInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "ClientInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClientUserSummeryPrint()
        {
            IClientUserService service = new ClientUserService();

            var report = new BaseReportModel<ClientUserView>();

            report.ReportTitle = "ClientUser";
            report.RptFileName = "ClientUserPrint.rpt";

            report.DataSource = service.GetClientUserReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "ClientUser";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult WarehouseInfoPrint()
        {
            IWerehouseService service = new WerehouseService();

            var report = new BaseReportModel<WarehouseInfoView>();

            report.ReportTitle = "WarehouseInfo";
            report.RptFileName = "WarehousePrint.rpt";

            report.DataSource = service.GetWarehouseReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "WarehouseInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SupplierDetailsSummaryPrint()
        {
            ISupplierInfoService service = new SupplierInfoService();

            var report = new BaseReportModel<SupplierInfoView>();

            report.ReportTitle = "SupplierInfo";
            report.RptFileName = "SupplierInfoPrint.rpt";

            report.DataSource = service.GetSupplierInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "SupplierInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerDetailsSummaryPrint()
        {
            ICustomerService service = new CustomerService();

            var report = new BaseReportModel<CustomerInfoView>();

            report.ReportTitle = "CustomerInfo";
            report.RptFileName = "CustomerInfoPrint.rpt";

            report.DataSource = service.GetCustomerInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "CustomerInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);

        }

        public ActionResult OutletDetailsSummaryPrint()
        {
            IOutletService service = new OutletService();

            var report = new BaseReportModel<OutletInfoView>();

            report.ReportTitle = "OutletInfo";
            report.RptFileName = "OutletInfoPrint.rpt";

            report.DataSource = service.GetOutletInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "OutletInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductInfoDetailsSummaryPrint()
        {
            IProductInfoService service = new ProductInfoService();

            var report = new BaseReportModel<ProductInfoView>();

            report.ReportTitle = "ProductInfo";
            report.RptFileName = "ProductInfoPrint.rpt";

            report.DataSource = service.GetProductInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "ProductInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductPurchaseDetailsSummaryPrint()
        {
            IProductPurchaseService service = new ProductPurchaseService();
            var report = new BaseReportModel<ProductPurchaseView>();

            report.ReportTitle = "ProductPurchase";
            report.RptFileName = "ProductPurchasePrint.rpt";

            report.DataSource = service.GetProductPurchaseReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "ProductPurchase";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult CurrentStockSummaryPrint()
        {
            IStockService service = new StockService();

            var report = new BaseReportModel<StockView>();

            report.ReportTitle = "StockInfo";
            report.RptFileName = "StockInfoPrint.rpt";

            report.DataSource = service.GetStockInfoReportData();

            report.IsPassParamToCr = false;
            report.ReportType = "StockInfo";

            Session["ReportParam"] = report;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}