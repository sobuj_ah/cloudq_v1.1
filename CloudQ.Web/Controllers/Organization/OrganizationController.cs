﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;

namespace CloudQ.Web.Controllers.Organization
{
    public class OrganizationController : BaseController
    {
        // GET: Organization
        public ActionResult CompanyIndex()
        {
            return View("Company/CompanyIndex");
        }
    }
}