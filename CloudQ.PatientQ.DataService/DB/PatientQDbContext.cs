﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Context;
using CloudQ.Infrastructure.Entity;
using CloudQ.Logging.Logging;
using CloudQ.PatientQ.DataService.Migrations;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools.StaticData;

namespace CloudQ.PatientQ.DataService.DB
{
    public class PatientQDbContext : DbContext, IDbContext
    {


        private ObjectContext _objectContext;
        private DbTransaction _transaction;
        private static readonly object Lock = new object();
        private static bool _databaseInitialized;

        public PatientQDbContext()
            : base("SqlConnectionString")
        {

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<PatientQDbContext, Configuration>());
            Database.Log = s => Debug.Write(s);
        }

        public PatientQDbContext(string nameOrConnectionString, ILogger logger)
            : base(nameOrConnectionString)
        {
            if (logger != null)
            {
                Database.Log = logger.Log;
            }

            if (_databaseInitialized)
            {
                return;
            }
            lock (Lock)
            {
                if (!_databaseInitialized)
                {
                    Database.SetInitializer(new BigBangInitializer());
                    _databaseInitialized = true;
                }
            }
        }


        #region DbSets
        public virtual IDbSet<Gender> Genders { get; set; }
        public virtual IDbSet<DoctorSpecialty> DoctorSpecialties { get; set; }
        public virtual IDbSet<DoctorType> DoctorTypes { get; set; }
        public virtual IDbSet<Doctor> Doctors { get; set; }
        public virtual IDbSet<DoctorFee> DoctorFees { get; set; }
        public virtual IDbSet<DoctorShedule> DoctorShedules { get; set; }
        public virtual IDbSet<DoctorWithShedule> DoctorWithShedules { get; set; }
        public virtual IDbSet<DrugEligibleAge> DrugEligibleAges { get; set; }
        public virtual IDbSet<DrugSchedule> DrugSchedules { get; set; }

        public virtual IDbSet<Appointment> Appointments { get; set; }
        public virtual IDbSet<Disease> Diseases { get; set; }
        public virtual IDbSet<Symptom> Symptoms { get; set; }
        public virtual IDbSet<DiseaseWithSymptom> DiseaseWithSymptoms { get; set; }


        public virtual IDbSet<PharmaCompany> PharmaCompanies { get; set; }
        public virtual IDbSet<DrugCategory> DrugCategories { get; set; }
        public virtual IDbSet<DrugFormulation> DrugFormulations { get; set; }
        public virtual IDbSet<DrugStrengths> DrugStrengthses { get; set; }
        public virtual IDbSet<DrugPackSize> DrugPackSizes { get; set; }
        public virtual IDbSet<DrugGeneric> DrugGenerics { get; set; }
        public virtual IDbSet<DrugAvailableStrengths> DrugAvailableStrengthses { get; set; }
        public virtual IDbSet<Medicine> Medicines { get; set; }
        public virtual IDbSet<DrugWithSymptom> DrugWithSymptoms { get; set; }
        public virtual IDbSet<DrugDosage> DrugDosages { get; set; }

        public virtual IDbSet<PatientInfo> PatientInfos { get; set; }
        public virtual IDbSet<PatientTest> PatientTests { get; set; }
        public virtual IDbSet<PatientTestDetails> PatientTestDetails { get; set; }

        public virtual IDbSet<TestCategory> TestCategories { get; set; }
        public virtual IDbSet<TestInfo> TestInfos { get; set; }
        public virtual IDbSet<DoctorTestCommission> DoctorTestCommissions { get; set; }
        public virtual IDbSet<TestParameter> TestParameters { get; set; }
        public virtual IDbSet<TestReportDetails> TestReportDetails { get; set; }
        public virtual IDbSet<TestReportMaster> TestReportMasters { get; set; }


        public virtual IDbSet<Prescription> Prescriptions { get; set; }
        public virtual IDbSet<PrescribeMedicine> PrescribeMedicines { get; set; }
        public virtual IDbSet<PrescribeDosage> PrescribeDosages { get; set; }

        //public virtual IDbSet<PrescribeDiease> PrescribeDieases { get; set; }
        //public virtual IDbSet<PrescribeDrugSchedule> PrescribeDrugSchedules { get; set; }
        public virtual IDbSet<PrescribeSymptom> PrescribeSymptoms { get; set; }
        //public virtual IDbSet<PrescribeTest> PrescribeTests { get; set; }



        #endregion




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema(DbSchema.pqm.ToString());



            //modelBuilder.Configurations.Add(new ContactConfiguration());
            // modelBuilder.Configurations.Add(new CompanyConfiguration());

        }

        public static PatientQDbContext Create()
        {
            return new PatientQDbContext(nameOrConnectionString: "SqlConnectionString", logger: null);
        }




        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseModel
        {
            return base.Set<TEntity>();
        }

        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            UpdateEntityState(entity, EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            UpdateEntityState(entity, EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            UpdateEntityState(entity, EntityState.Deleted);
        }

        public void BeginTransaction()
        {
            this._objectContext = ((IObjectContextAdapter)this).ObjectContext;
            if (_objectContext.Connection.State == ConnectionState.Open)
            {
                return;
            }
            _objectContext.Connection.Open();
            _transaction = _objectContext.Connection.BeginTransaction();
        }

        public int Commit()
        {
            try
            {
                BeginTransaction();
                var saveChanges = SaveChanges();
                _transaction.Commit();

                return saveChanges;
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public DbEntityEntry Entry(BaseModel entity)
        {
            return base.Entry(entity);
        }

        public async Task<int> CommitAsync()
        {
            try
            {
                BeginTransaction();
                var saveChangesAsync = await SaveChangesAsync();
                _transaction.Commit();

                return saveChangesAsync;
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : BaseModel
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            var dbEntityEntry = Entry<TEntity>(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }
    }
}
