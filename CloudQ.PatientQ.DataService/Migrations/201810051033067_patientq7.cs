namespace CloudQ.PatientQ.DataService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientq7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("pqm.DrugSchedules", "PatientAgeId", "pqm.DrugEligibleAges");
            DropForeignKey("pqm.DrugAvailableStrengths", "MedicineId", "pqm.Medicines");
            DropIndex("pqm.DrugSchedules", new[] { "PatientAgeId" });
            DropIndex("pqm.DrugAvailableStrengths", new[] { "MedicineId" });
            RenameColumn(table: "pqm.DrugAvailableStrengths", name: "MedicineId", newName: "Medicine_MedicineId");
            AlterColumn("pqm.DrugAvailableStrengths", "Medicine_MedicineId", c => c.Int());
            CreateIndex("pqm.DrugAvailableStrengths", "Medicine_MedicineId");
            AddForeignKey("pqm.DrugAvailableStrengths", "Medicine_MedicineId", "pqm.Medicines", "MedicineId");
            DropColumn("pqm.DrugSchedules", "MedicineId");
            DropColumn("pqm.DrugSchedules", "PatientAgeId");
        }
        
        public override void Down()
        {
            AddColumn("pqm.DrugSchedules", "PatientAgeId", c => c.Int());
            AddColumn("pqm.DrugSchedules", "MedicineId", c => c.Int(nullable: false));
            DropForeignKey("pqm.DrugAvailableStrengths", "Medicine_MedicineId", "pqm.Medicines");
            DropIndex("pqm.DrugAvailableStrengths", new[] { "Medicine_MedicineId" });
            AlterColumn("pqm.DrugAvailableStrengths", "Medicine_MedicineId", c => c.Int(nullable: false));
            RenameColumn(table: "pqm.DrugAvailableStrengths", name: "Medicine_MedicineId", newName: "MedicineId");
            CreateIndex("pqm.DrugAvailableStrengths", "MedicineId");
            CreateIndex("pqm.DrugSchedules", "PatientAgeId");
            AddForeignKey("pqm.DrugAvailableStrengths", "MedicineId", "pqm.Medicines", "MedicineId", cascadeDelete: true);
            AddForeignKey("pqm.DrugSchedules", "PatientAgeId", "pqm.DrugEligibleAges", "PatientAgeId");
        }
    }
}
