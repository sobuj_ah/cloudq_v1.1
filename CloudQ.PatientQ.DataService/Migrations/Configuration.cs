using System;
using System.Data.Entity.Migrations;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.DataService.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PatientQDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            //AutomaticMigrationsEnabled = false;
            ContextKey = "PatientQDbContext";

        }

        protected override void Seed(PatientQDbContext context)
        {

         //   context.DoctorTypes.AddOrUpdate(d => d.DoctorTypeName,
         //       new DoctorType { Id = Guid.NewGuid(), DoctorTypeName = "Medicine" },
         //       new DoctorType { Id = Guid.NewGuid(), DoctorTypeName = "Cardiologists" },
         //       new DoctorType { Id = Guid.NewGuid(), DoctorTypeName = "Dermatologists" },
         //       new DoctorType { Id = Guid.NewGuid(), DoctorTypeName = "Endocrinologists" }
         //       );

         //   context.Symptoms.AddOrUpdate(d => d.SymptomTitle,
         //      new Symptom { Id = Guid.NewGuid(), SymptomTitle = "Fever" },
         //      new Symptom { Id = Guid.NewGuid(), SymptomTitle = "Headache" },
         //      new Symptom { Id = Guid.NewGuid(), SymptomTitle = "Back Pain" },
         //      new Symptom { Id = Guid.NewGuid(), SymptomTitle = "Vomiting" }
         //      );
         //   context.PharmaCompanies.AddOrUpdate(d => d.PharmaCompanyName,
         //new PharmaCompany { Id = Guid.NewGuid(), PharmaCompanyName = "Renata Ltd" },
         //new PharmaCompany { Id = Guid.NewGuid(), PharmaCompanyName = "Square" },
         //new PharmaCompany { Id = Guid.NewGuid(), PharmaCompanyName = "Beximco" },
         //new PharmaCompany { Id = Guid.NewGuid(), PharmaCompanyName = "SKF" }
         //);
         //   context.DrugCategories.AddOrUpdate(d => d.CategoryName,
         //  new DrugCategory { Id = Guid.NewGuid(), CategoryName = "Anti-Hypertensive" },
         //  new DrugCategory { Id = Guid.NewGuid(), CategoryName = "Anthelmintic" },
         //  new DrugCategory { Id = Guid.NewGuid(), CategoryName = "Anti-Diabetic" },
         //  new DrugCategory { Id = Guid.NewGuid(), CategoryName = "Anti-Depressants" }
         //  );

         //   context.DrugGenerics.AddOrUpdate(d => d.GenericName,
         //   new DrugGeneric { Id = Guid.NewGuid(), GenericName = "Flupenthixol+Melitracen" },
         //   new DrugGeneric { Id = Guid.NewGuid(), GenericName = "Pyrantel Pamoate" },
         //   new DrugGeneric { Id = Guid.NewGuid(), GenericName = "Bisoprolol" },
         //   new DrugGeneric { Id = Guid.NewGuid(), GenericName = "Arithromicine" }
         //   );
         //   context.DrugFormulations.AddOrUpdate(d => d.Formulation,
         //  new DrugFormulation { Id = Guid.NewGuid(), Formulation = "Tablet" },
         //  new DrugFormulation { Id = Guid.NewGuid(), Formulation = "Suspension" },
         //  new DrugFormulation { Id = Guid.NewGuid(), Formulation = "Injection" },
         //  new DrugFormulation { Id = Guid.NewGuid(), Formulation = "Capsule" }
         //  );

         //   context.DrugPackSizes.AddOrUpdate(d => d.PackSize,
         // new DrugPackSize { Id = Guid.NewGuid(), PackSize = "3x10s" },
         // new DrugPackSize { Id = Guid.NewGuid(), PackSize = "10x10s" },
         // new DrugPackSize { Id = Guid.NewGuid(), PackSize = "20x10s" },
         // new DrugPackSize { Id = Guid.NewGuid(), PackSize = "1 Pice" }
         // );

         //   context.Medicines.AddOrUpdate(d => d.MedicineName,
         //   new Medicine { Id = Guid.NewGuid(), MedicineName = "Renxit ", Company = new PharmaCompany { PharmaCompanyName = "Renata Ltd" } },
         //   new Medicine { Id = Guid.NewGuid(), MedicineName = "ACE Plus", Company = new PharmaCompany { PharmaCompanyName = "Square" } },
         //   new Medicine { Id = Guid.NewGuid(), MedicineName = "Napa", Company = new PharmaCompany { PharmaCompanyName = "Beximco" } },
         //   new Medicine { Id = Guid.NewGuid(), MedicineName = "Losectil", Company = new PharmaCompany { PharmaCompanyName = "SKF" } }
         //   );

         //   context.TestInfos.AddOrUpdate(d => d.TestName,
         //new TestInfo { Id = Guid.NewGuid(), TestName = "CBC ", },
         //new TestInfo { Id = Guid.NewGuid(), TestName = "X-Ray",},
         //new TestInfo { Id = Guid.NewGuid(), TestName = "Ultra-Sound",  },
         //new TestInfo { Id = Guid.NewGuid(), TestName = "Urine Test", }
         //);



            context.SaveChanges();
            base.Seed(context);

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //context.UserTypes.AddOrUpdate(u => u.UserTypeName,
            //    new UserType { UserTypeName = "Admin", UserTypeId = 1 },
            //    new UserType { UserTypeName = "Manager", UserTypeId = 2 },
            //    new UserType { UserTypeName = "User", UserTypeId = 3 });
            //context.BusinessTypes.AddOrUpdate(b => b.BusinessTypeName,
            //    new BusinessType { BusinessTypeName = "Pharmacy", BusinessTypeId = 1 },
            //    new BusinessType { BusinessTypeName = "General Store", BusinessTypeId = 2 },
            //    new BusinessType { BusinessTypeName = "Cloth Shop", BusinessTypeId = 3 });


            //context.UserCategories.AddOrUpdate(u => u.UserCategoryName,
            //    new UserCategory { UserCategoryName = "Back Office", UserCategoryId = 1 },
            //    new UserCategory { UserCategoryName = "Client", UserCategoryId = 2 });




        }
    }
}
