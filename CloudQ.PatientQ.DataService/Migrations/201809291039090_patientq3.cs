namespace CloudQ.PatientQ.DataService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientq3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("pqm.Prescriptions", "DoctorId", "pqm.Doctors");
            DropForeignKey("pqm.Prescriptions", "PatientId", "pqm.PatientInfo");
            DropForeignKey("pqm.PrescribeMedicines", "DrugScheduleId", "pqm.DrugSchedules");
            DropForeignKey("pqm.PrescribeMedicines", "MedicineId", "pqm.Medicines");
            DropForeignKey("pqm.PrescribeMedicines", "PrescriptionId", "pqm.Prescriptions");
            DropForeignKey("pqm.PrescribeSymptoms", "PrescriptionId", "pqm.Prescriptions");
            DropForeignKey("pqm.PrescribeSymptoms", "SymptomId", "pqm.Symptoms");
            DropForeignKey("pqm.PrescribeTests", "PrescriptionId", "pqm.Prescriptions");
            DropForeignKey("pqm.PrescribeTests", "TestId", "pqm.TestInfoes");
            DropIndex("pqm.Prescriptions", new[] { "PatientId" });
            DropIndex("pqm.Prescriptions", new[] { "DoctorId" });
            DropIndex("pqm.PrescribeDieases", new[] { "Disease_PrescriptionId" });
            DropIndex("pqm.PrescribeDieases", new[] { "Prescription_PrescriptionId" });
            DropIndex("pqm.PrescribeMedicines", new[] { "MedicineId" });
            DropIndex("pqm.PrescribeMedicines", new[] { "DrugScheduleId" });
            DropIndex("pqm.PrescribeMedicines", new[] { "PrescriptionId" });
            DropIndex("pqm.PrescribeSymptoms", new[] { "SymptomId" });
            DropIndex("pqm.PrescribeSymptoms", new[] { "PrescriptionId" });
            DropIndex("pqm.PrescribeTests", new[] { "PrescriptionId" });
            DropIndex("pqm.PrescribeTests", new[] { "TestId" });
            DropColumn("pqm.PrescribeDieases", "DiseaseId");
            DropColumn("pqm.PrescribeDieases", "PrescriptionId");
            RenameColumn(table: "pqm.PrescribeDieases", name: "Disease_PrescriptionId", newName: "DiseaseId");
            RenameColumn(table: "pqm.PrescribeDieases", name: "Prescription_PrescriptionId", newName: "PrescriptionId");
            AddColumn("pqm.Appointments", "Status", c => c.Int(nullable: false));
            AlterColumn("pqm.Prescriptions", "PatientId", c => c.Int());
            AlterColumn("pqm.Prescriptions", "DoctorId", c => c.Int());
            AlterColumn("pqm.PrescribeDieases", "PrescriptionId", c => c.Int());
            AlterColumn("pqm.PrescribeDieases", "DiseaseId", c => c.Int());
            AlterColumn("pqm.PrescribeMedicines", "MedicineId", c => c.Int());
            AlterColumn("pqm.PrescribeMedicines", "DrugScheduleId", c => c.Int());
            AlterColumn("pqm.PrescribeMedicines", "PrescriptionId", c => c.Int());
            AlterColumn("pqm.PrescribeSymptoms", "SymptomId", c => c.Int());
            AlterColumn("pqm.PrescribeSymptoms", "PrescriptionId", c => c.Int());
            AlterColumn("pqm.PrescribeTests", "PrescriptionId", c => c.Int());
            AlterColumn("pqm.PrescribeTests", "TestId", c => c.Int());
            CreateIndex("pqm.Prescriptions", "PatientId");
            CreateIndex("pqm.Prescriptions", "DoctorId");
            CreateIndex("pqm.PrescribeDieases", "PrescriptionId");
            CreateIndex("pqm.PrescribeDieases", "DiseaseId");
            CreateIndex("pqm.PrescribeMedicines", "MedicineId");
            CreateIndex("pqm.PrescribeMedicines", "DrugScheduleId");
            CreateIndex("pqm.PrescribeMedicines", "PrescriptionId");
            CreateIndex("pqm.PrescribeSymptoms", "SymptomId");
            CreateIndex("pqm.PrescribeSymptoms", "PrescriptionId");
            CreateIndex("pqm.PrescribeTests", "PrescriptionId");
            CreateIndex("pqm.PrescribeTests", "TestId");
            AddForeignKey("pqm.Prescriptions", "DoctorId", "pqm.Doctors", "DoctorID");
            AddForeignKey("pqm.Prescriptions", "PatientId", "pqm.PatientInfo", "PatientId");
            AddForeignKey("pqm.PrescribeMedicines", "DrugScheduleId", "pqm.DrugSchedules", "DrugScheduleId");
            AddForeignKey("pqm.PrescribeMedicines", "MedicineId", "pqm.Medicines", "MedicineId");
            AddForeignKey("pqm.PrescribeMedicines", "PrescriptionId", "pqm.Prescriptions", "PrescriptionId");
            AddForeignKey("pqm.PrescribeSymptoms", "PrescriptionId", "pqm.Prescriptions", "PrescriptionId");
            AddForeignKey("pqm.PrescribeSymptoms", "SymptomId", "pqm.Symptoms", "SymptomId");
            AddForeignKey("pqm.PrescribeTests", "PrescriptionId", "pqm.Prescriptions", "PrescriptionId");
            AddForeignKey("pqm.PrescribeTests", "TestId", "pqm.TestInfoes", "TestId");
        }
        
        public override void Down()
        {
            DropForeignKey("pqm.PrescribeTests", "TestId", "pqm.TestInfoes");
            DropForeignKey("pqm.PrescribeTests", "PrescriptionId", "pqm.Prescriptions");
            DropForeignKey("pqm.PrescribeSymptoms", "SymptomId", "pqm.Symptoms");
            DropForeignKey("pqm.PrescribeSymptoms", "PrescriptionId", "pqm.Prescriptions");
            DropForeignKey("pqm.PrescribeMedicines", "PrescriptionId", "pqm.Prescriptions");
            DropForeignKey("pqm.PrescribeMedicines", "MedicineId", "pqm.Medicines");
            DropForeignKey("pqm.PrescribeMedicines", "DrugScheduleId", "pqm.DrugSchedules");
            DropForeignKey("pqm.Prescriptions", "PatientId", "pqm.PatientInfo");
            DropForeignKey("pqm.Prescriptions", "DoctorId", "pqm.Doctors");
            DropIndex("pqm.PrescribeTests", new[] { "TestId" });
            DropIndex("pqm.PrescribeTests", new[] { "PrescriptionId" });
            DropIndex("pqm.PrescribeSymptoms", new[] { "PrescriptionId" });
            DropIndex("pqm.PrescribeSymptoms", new[] { "SymptomId" });
            DropIndex("pqm.PrescribeMedicines", new[] { "PrescriptionId" });
            DropIndex("pqm.PrescribeMedicines", new[] { "DrugScheduleId" });
            DropIndex("pqm.PrescribeMedicines", new[] { "MedicineId" });
            DropIndex("pqm.PrescribeDieases", new[] { "DiseaseId" });
            DropIndex("pqm.PrescribeDieases", new[] { "PrescriptionId" });
            DropIndex("pqm.Prescriptions", new[] { "DoctorId" });
            DropIndex("pqm.Prescriptions", new[] { "PatientId" });
            AlterColumn("pqm.PrescribeTests", "TestId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeTests", "PrescriptionId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeSymptoms", "PrescriptionId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeSymptoms", "SymptomId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeMedicines", "PrescriptionId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeMedicines", "DrugScheduleId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeMedicines", "MedicineId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeDieases", "DiseaseId", c => c.Int(nullable: false));
            AlterColumn("pqm.PrescribeDieases", "PrescriptionId", c => c.Int(nullable: false));
            AlterColumn("pqm.Prescriptions", "DoctorId", c => c.Int(nullable: false));
            AlterColumn("pqm.Prescriptions", "PatientId", c => c.Int(nullable: false));
            DropColumn("pqm.Appointments", "Status");
            RenameColumn(table: "pqm.PrescribeDieases", name: "PrescriptionId", newName: "Prescription_PrescriptionId");
            RenameColumn(table: "pqm.PrescribeDieases", name: "DiseaseId", newName: "Disease_PrescriptionId");
            AddColumn("pqm.PrescribeDieases", "PrescriptionId", c => c.Int(nullable: false));
            AddColumn("pqm.PrescribeDieases", "DiseaseId", c => c.Int(nullable: false));
            CreateIndex("pqm.PrescribeTests", "TestId");
            CreateIndex("pqm.PrescribeTests", "PrescriptionId");
            CreateIndex("pqm.PrescribeSymptoms", "PrescriptionId");
            CreateIndex("pqm.PrescribeSymptoms", "SymptomId");
            CreateIndex("pqm.PrescribeMedicines", "PrescriptionId");
            CreateIndex("pqm.PrescribeMedicines", "DrugScheduleId");
            CreateIndex("pqm.PrescribeMedicines", "MedicineId");
            CreateIndex("pqm.PrescribeDieases", "Prescription_PrescriptionId");
            CreateIndex("pqm.PrescribeDieases", "Disease_PrescriptionId");
            CreateIndex("pqm.Prescriptions", "DoctorId");
            CreateIndex("pqm.Prescriptions", "PatientId");
            AddForeignKey("pqm.PrescribeTests", "TestId", "pqm.TestInfoes", "TestId", cascadeDelete: true);
            AddForeignKey("pqm.PrescribeTests", "PrescriptionId", "pqm.Prescriptions", "PrescriptionId", cascadeDelete: true);
            AddForeignKey("pqm.PrescribeSymptoms", "SymptomId", "pqm.Symptoms", "SymptomId", cascadeDelete: true);
            AddForeignKey("pqm.PrescribeSymptoms", "PrescriptionId", "pqm.Prescriptions", "PrescriptionId", cascadeDelete: true);
            AddForeignKey("pqm.PrescribeMedicines", "PrescriptionId", "pqm.Prescriptions", "PrescriptionId", cascadeDelete: true);
            AddForeignKey("pqm.PrescribeMedicines", "MedicineId", "pqm.Medicines", "MedicineId", cascadeDelete: true);
            AddForeignKey("pqm.PrescribeMedicines", "DrugScheduleId", "pqm.DrugSchedules", "DrugScheduleId", cascadeDelete: true);
            AddForeignKey("pqm.Prescriptions", "PatientId", "pqm.PatientInfo", "PatientId", cascadeDelete: true);
            AddForeignKey("pqm.Prescriptions", "DoctorId", "pqm.Doctors", "DoctorID", cascadeDelete: true);
        }
    }
}
