using System.Collections.Generic;
using CloudQ.Core.Model.Models.Core;

namespace CloudQ.Core.Model.DTOs.Core
{
    
    public class CoreWorkflowStateDto 
    {
        public int WorkflowStateId { get; set; }
        public string StateName { get; set; }
        public int MenuId { get; set; }
        public bool? IsDefaultStart { get; set; }
        public int? Flag { get; set; }
        public virtual ICollection<CoreWorkflowAction> CoreWorkflowActions { get; set; }
    }
}
