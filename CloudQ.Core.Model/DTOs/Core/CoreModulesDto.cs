namespace CloudQ.Core.Model.DTOs.Core
{
    
    public class CoreModulesDto
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
    }
}
