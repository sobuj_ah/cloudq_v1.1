namespace CloudQ.Core.Model.DTOs.Core
{
    
    public class CoreMenusDto
    {
        
        public int MenuId { get; set; }
        public int ModuleId { get; set; }
         
        public string MenuName { get; set; }
          
        public string MenuPath { get; set; }

        public int? ParentId { get; set; }

        public int? Sequence { get; set; }

    }
}
