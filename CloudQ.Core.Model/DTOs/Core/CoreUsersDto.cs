using System;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.DTOs.Core
{

    public class CoreUsersDto : BaseModel
    {
        public CoreUsersDto()
        {
            LastUpdateTime = DateTime.Now;
            LastLoginTime = DateTime.Now;

        }

        public int UserId { get; set; }
        public string LoginId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public DateTime LastLoginTime { get; set; }
        public int FailedLoginCount { get; set; }
        public bool IsExpired { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int UserTypeId { get; set; }
        public string UserType { get; set; }


    }
}
