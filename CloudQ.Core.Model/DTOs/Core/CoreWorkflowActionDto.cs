namespace CloudQ.Core.Model.DTOs.Core
{
    
    public class CoreWorkflowActionDto
    {
       
        public int WorkflowActionId { get; set; }
        public int WorkflowStateId { get; set; }
        public string ActionName { get; set; }
        public string ActionTitle { get; set; }
        public int NextStateId { get; set; }
    }
}
