using System.Collections.Generic;
using CloudQ.Core.Model.Models.Core;

namespace CloudQ.Core.Model.DTOs.Core
{
    
    public class CoreRoleDto
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int? IsDefault { get; set; }
        public virtual ICollection<CoreRolePermission> ModuleList { get; set; }
        public virtual ICollection<CoreRolePermission> MenuList { get; set; }
        public virtual ICollection<CoreRolePermission> AccessList { get; set; }
        public virtual ICollection<CoreRolePermission> StatusList { get; set; }
        public virtual ICollection<CoreRolePermission> ActionList { get; set; }
        public virtual ICollection<CoreRolePermission> ReportList { get; set; }
        
    }
}
