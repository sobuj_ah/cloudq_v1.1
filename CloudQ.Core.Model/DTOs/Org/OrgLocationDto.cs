﻿namespace CloudQ.Core.Model.DTOs.Org
{
    
    public class OrgLocationDto
    {
        
        public int LocationId { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }

    }
}
