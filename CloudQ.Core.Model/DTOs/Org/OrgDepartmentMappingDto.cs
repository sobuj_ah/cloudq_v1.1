namespace CloudQ.Core.Model.DTOs.Org
{
    public partial class OrgDepartmentMappingDto
    {
        public int DepartmentMapppingId { get; set; }
        public int DepartmentId { get; set; }
        public int ReferenceId { get; set; }
        public int ReferenceType { get; set; }
    }
}
