﻿namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgDepartmentDto
    {
         
        public int DepartmentId { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        
    }
}
