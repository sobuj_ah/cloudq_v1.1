﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Core.Model.Models.Core;

namespace CloudQ.Core.Model.DTOs.Org
{
    public class CoreRolePermissionDto
    {

        public int PermissionId { get; set; }


        public string PermissionTableName { get; set; }
        public int ReferenceId { get; set; }
        public int ParentPermission { get; set; }
        //public virtual CoreRole CoreRoles { get; set; }
        public int RoleId { get; set; }


    }
}
