﻿namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgDesignationDto
    {

        public int DesignationId { get; set; }
        public string DesignationName { get; set; }


    }
}
