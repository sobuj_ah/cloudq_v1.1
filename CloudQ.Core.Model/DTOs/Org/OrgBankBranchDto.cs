﻿using CloudQ.Core.Model.Models.Org;

namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgBankBranchDto 
    {
        public int BankBranchId { get; set; }
        public int BankId { get; set; }
        public OrgBank Bank { get; set; }
        public string BranchName { get; set; }
        public string BranceAddress { get; set; }
        public string PhoneNo { get; set; }

    }
}
