﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgSectionDto
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }

    }
}
