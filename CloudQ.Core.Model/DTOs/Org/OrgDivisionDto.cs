﻿namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgDivisionDto
    {
      
        public int DivisionId { get; set; }
        public string DivisionCode { get; set; }
        public string DivisionName { get; set; }
        

    }
}
