﻿namespace CloudQ.Core.Model.DTOs.Org
{

    public class CompanyDto
    {

        public int CompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }
        public string Fax { get; set; }
        public string OwnerName { get; set; }
        public string OwnerAddress { get; set; }
        public string LogoPath { get; set; }
        public string BannerPath { get; set; }
    }
}
