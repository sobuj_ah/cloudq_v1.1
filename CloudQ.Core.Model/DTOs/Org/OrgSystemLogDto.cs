using System;

namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgSystemLogDto  
    {
      
        public int SystemLogId { get; set; }
        public int UserId { get; set; }
        public string ClientOsUser { get; set; }
        public string ClientIP { get; set; }
        public string LogType { get; set; }
        public string Description { get; set; }
        public DateTime SystemDate { get; set; }
        public string RequestedUrl { get; set; }
        public string ReferrerUrl { get; set; }
        public string DomainName { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string RequestedParams { get; set; }
        public string IdentityInTable { get; set; }
        public int MenuId { get; set; }
        public int ModuleId { get; set; }
        public string Operation { get; set; }
        public string ExceptionLog { get; set; }








    }
}
