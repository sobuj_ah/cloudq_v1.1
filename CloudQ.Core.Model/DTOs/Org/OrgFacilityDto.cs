﻿namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgFacilityDto
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }

    }
}
