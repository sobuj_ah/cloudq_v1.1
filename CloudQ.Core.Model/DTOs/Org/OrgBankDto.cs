﻿namespace CloudQ.Core.Model.DTOs.Org
{
    public class OrgBankDto 
    {
      
        public int BankId { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string PhoneNo { get; set; }

    }
}
