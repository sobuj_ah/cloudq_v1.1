﻿namespace CloudQ.Core.Model.DTOs.Org
{
    public class PictureUploadDto
    {
        public int PictureId { get; set; }
        public string PictureCode { get; set; }
        public string PicturePath { get; set; }

    }
}
