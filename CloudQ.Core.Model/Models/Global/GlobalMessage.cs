using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public partial class Global_Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }

        public int? FromHrRecordId { get; set; }

        public int? ToHrRecordId { get; set; }

        public DateTime? MessagingDate { get; set; }

        public string MessageDetails { get; set; }

        public int? IsArchive { get; set; }

        public int? IsRead { get; set; }

        [StringLength(50)]
        public string MessageSubject { get; set; }
    }
}
