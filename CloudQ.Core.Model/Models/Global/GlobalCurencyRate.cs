using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Global
{
    public partial class GlobalCurencyRate : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int CurencyRateId { get; set; }

        public int CurrencyId { get; set; }

        public decimal? CurrencyRateRation { get; set; }

        public DateTime? CurrencyMonth { get; set; }

        public int? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}
