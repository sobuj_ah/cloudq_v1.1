using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public partial class Global_District
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DistrictId { get; set; }

        [StringLength(100)]
        public string DistrictName { get; set; }

        [StringLength(50)]
        public string DistrictCode { get; set; }

        public bool? IsActive { get; set; }
    }
}
