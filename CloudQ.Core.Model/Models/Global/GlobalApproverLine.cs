using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Global
{
    public class GlobalApproverLine : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ApproverLineId { get; set; }

        public int ApproverId { get; set; }

        public int HrRecordId { get; set; }

        public int ModuleId { get; set; }

        public int ApproverType { get; set; }

        public int? ApproverNo { get; set; }
    }
}
