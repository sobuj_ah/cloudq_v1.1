using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public class GlobalAssemblyInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AssemblyInfoId { get; set; }

        [Required]
        [StringLength(50)]
        public string AssemblyTitle { get; set; }

        [Required]
        [StringLength(50)]
        public string AssemblyDescription { get; set; }

        [Required]
        [StringLength(100)]
        public string AssemblyCompany { get; set; }

        [Required]
        [StringLength(100)]
        public string AssemblyProduct { get; set; }

        [Required]
        [StringLength(150)]
        public string AssemblyCopyright { get; set; }

        [Required]
        [StringLength(50)]
        public string AssemblyVersion { get; set; }

        [Required]
        [StringLength(250)]
        public string ProductBanner { get; set; }

        public bool IsAttendanceByLogin { get; set; }

        [Required]
        [StringLength(150)]
        public string PoweredBy { get; set; }

        [Required]
        [StringLength(250)]
        public string PoweredByUrl { get; set; }

        public bool IsDefault { get; set; }

        [StringLength(250)]
        public string ProductStyleSheet { get; set; }
    }
}
