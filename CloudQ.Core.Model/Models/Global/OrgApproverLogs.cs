using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public class OrgApproverLogs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ApproverLogId { get; set; }
        public int ApproverLineId { get; set; }
        public int MenuId { get; set; }
        public int ApplicationId { get; set; }
        public bool IsOpen { get; set; }
        public string Comments { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int ApplicantUserId { get; set; }
        public int ApprovedBy { get; set; }
        public int ApproverType { get; set; }
        public int Sequence { get; set; }
        public int ModuleId { get; set; }
        public int CompanyId { get; set; }
        public int BranchId { get; set; }
        public int DivisionId { get; set; }
        public int DepartmentId { get; set; }
        public int FacilityId { get; set; }
        public int SectionId { get; set; }
        public int DesignationId { get; set; }
        public int GradeId { get; set; }
        public int EmployeeTypeId { get; set; }
        public int SubSectionId { get; set; }


    }
}
