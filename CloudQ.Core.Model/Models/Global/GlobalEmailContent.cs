using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public partial class Global_EmailContent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int EmailContentId { get; set; }

        [StringLength(250)]
        public string EmailSubject { get; set; }

        [StringLength(250)]
        public string EmailTitle { get; set; }

        public string EmailAttachment { get; set; }

        public string EmailBody { get; set; }

        [StringLength(500)]
        public string ImagesPath { get; set; }

        public int? EmailContentStatus { get; set; }

        public int? EmailTitleId { get; set; }

        public int? EmailNotificationId { get; set; }
    }
}
