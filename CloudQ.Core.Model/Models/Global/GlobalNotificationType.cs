using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public partial class GlobalNotificationEmailType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmailNotificationTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string EmailNotificationTypeName { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(1000)]
        public string ParamDefination { get; set; }

        public int? ModuleId { get; set; }
    }
}
