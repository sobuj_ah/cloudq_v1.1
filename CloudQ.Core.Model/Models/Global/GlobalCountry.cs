using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Global
{
    public class GlobalCountry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CountryId { get; set; }

        [StringLength(100)]
        public string CountryName { get; set; }

        [StringLength(50)]
        public string CountryCode { get; set; }

        public int? Status { get; set; }
    }
}
