﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{
    public class OrgBank : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BankId { get; set; }
        [StringLength(100)]
        public string BankName { get; set; }
        [StringLength(200)]
        public string BankAddress { get; set; }
        [StringLength(100)]
        public string PhoneNo { get; set; }

    }
}
