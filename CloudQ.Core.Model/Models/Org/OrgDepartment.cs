﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{
    public class OrgDepartment : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DepartmentId { get; set; }
        [StringLength(20)]
        public string DepartmentCode { get; set; }
        [StringLength(200)]
        public string DepartmentName { get; set; }
        
    }
}
