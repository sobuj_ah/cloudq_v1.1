using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{
    public class OrgSystemLog : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SystemLogId { get; set; }

        public int UserId { get; set; }

        [StringLength(500)]
        public string ClientOsUser { get; set; }
        [StringLength(150)]
        public string ClientIP { get; set; }
        [StringLength(50)]
        public string LogType { get; set; }
        public string Description { get; set; }
        public DateTime SystemDate { get; set; }
        public string RequestedUrl { get; set; }
        public string ReferrerUrl { get; set; }
        [StringLength(100)]
        public string DomainName { get; set; }
        [StringLength(100)]
        public string ActionName { get; set; }
        [StringLength(100)]
        public string ControllerName { get; set; }
        public string RequestedParams { get; set; }
        public string IdentityInTable { get; set; }
        public int MenuId { get; set; }
        public int ModuleId { get; set; }
        [StringLength(200)]
        public string Operation { get; set; }
        public string ExceptionLog { get; set; }








    }
}
