﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{

    [Table("OrgCompany")]
    public class OrgCompany : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string WebAddress { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }
        public string Fax { get; set; }
        public byte[] LogoImage { get; set; }
        public byte[] BannerImage { get; set; }


    }
}
