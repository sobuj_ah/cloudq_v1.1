using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Org
{
    public partial class OrgDepartmentMapping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int DepartmentMapppingId { get; set; }

        public int DepartmentId { get; set; }

        public int ReferenceId { get; set; }

        public int ReferenceType { get; set; }
    }
}
