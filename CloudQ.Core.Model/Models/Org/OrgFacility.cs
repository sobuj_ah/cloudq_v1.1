﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Model.Models.Org
{
   public class OrgFacility
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }

    }
}
