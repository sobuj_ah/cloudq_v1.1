﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{
    [Table("OrgLocation")]
    public class OrgLocation : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocationId { get; set; }
        [StringLength(20)]
        public string LocationCode { get; set; }
        [StringLength(200)]
        public string LocationName { get; set; }
        [StringLength(250)]
        public string LocationAddress { get; set; }

    }
}
