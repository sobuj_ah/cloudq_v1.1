﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{
    public class OrgBankBranch : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BankBranchId { get; set; }

        public int BankId { get; set; }
        public OrgBank Bank { get; set; }
        [StringLength(100)]
        public string BranchName { get; set; }
        [StringLength(200)]
        public string BranceAddress { get; set; }
        [StringLength(100)]
        public string PhoneNo { get; set; }

    }
}
