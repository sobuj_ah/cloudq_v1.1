﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Org
{
    public class OrgDivision:BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DivisionId { get; set; }
        [StringLength(20)]
        public string DivisionCode { get; set; }
        [StringLength(200)]
        public string DivisionName { get; set; }
        

    }
}
