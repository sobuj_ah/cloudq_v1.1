﻿namespace CloudQ.Core.Model.Models.Org
{
    public class OrgSection
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }

    }
}
