using System;
using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_WithoutPay
    {
        [Key]
        public int LeaveWithoutId { get; set; }

        public int? HrRecordId { get; set; }

        public decimal? LeaveWithoutPay { get; set; }

        public DateTime? SalaryMonth { get; set; }

        public int? ChannelId { get; set; }
    }
}
