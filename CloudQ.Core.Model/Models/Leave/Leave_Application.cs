using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Application
    {
        [Key]
        public int LeaveApplicationId { get; set; }

        public int? LeaveTypeId { get; set; }

        public int? HRRecordId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeaveFromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeaveToDate { get; set; }

        public decimal? LeaveDays { get; set; }

        [Required]
        [StringLength(2000)]
        public string ReasonDetails { get; set; }

        public string Address { get; set; }

        public DateTime? AppliedDate { get; set; }

        [Column(TypeName = "ntext")]
        public string Remarks { get; set; }

        public int? ApproverId { get; set; }

        public DateTime? ApproveDate { get; set; }

        public int? StateId { get; set; }

        public int? ReferenceId { get; set; }

        public int? HalfDaySlot { get; set; }

        [StringLength(1000)]
        public string PerformedBy { get; set; }

        public int? InformedOffice { get; set; }

        [StringLength(1000)]
        public string Comments { get; set; }

        public int? IsLFAApplicable { get; set; }

        public int? LeaveReasonId { get; set; }
    }
}
