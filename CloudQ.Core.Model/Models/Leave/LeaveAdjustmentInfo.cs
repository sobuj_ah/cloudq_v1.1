using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Leave
{
    [Table("LeaveAdjustmentInfo")]
    public partial class LeaveAdjustmentInfo
    {
        [Key]
        public int LeaveAdjustmentId { get; set; }

        public int BalanceId { get; set; }

        public int HRRecordId { get; set; }

        public decimal? OpeningLeaveBalanceOld { get; set; }

        public DateTime? LeaveYearFrom { get; set; }

        public DateTime? LeaveYearTo { get; set; }

        public int? LeaveType { get; set; }

        public decimal? LeaveBroughtForwardOld { get; set; }

        public decimal? LeaveEnjoiedOld { get; set; }

        public decimal? ClosingLeaveBalanceOld { get; set; }

        public decimal? LeaveDeductedOld { get; set; }

        public int? FiscalYearId { get; set; }

        public decimal? OpeningLeaveBalanceNew { get; set; }

        public decimal? LeaveBroughtForwardNew { get; set; }

        public decimal? LeaveEnjoiedNew { get; set; }

        public decimal? ClosingLeaveBalanceNew { get; set; }

        public decimal? LeaveDeductedNew { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? UpdateType { get; set; }
    }
}
