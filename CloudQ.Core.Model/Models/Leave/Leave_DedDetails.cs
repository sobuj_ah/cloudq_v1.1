using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_DedDetails
    {
        [Key]
        public int LeaveDedDetailsId { get; set; }

        public int LeaveDedPolicyId { get; set; }

        public int LeaveTypeId { get; set; }

        public int SortOrder { get; set; }
    }
}
