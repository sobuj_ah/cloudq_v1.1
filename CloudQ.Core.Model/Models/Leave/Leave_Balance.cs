using System;
using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Balance
    {
        [Key]
        public int BalanceId { get; set; }

        public int? HRRecordId { get; set; }

        public decimal? OpeningLeaveBalance { get; set; }

        public DateTime? LeaveYearFrom { get; set; }

        public DateTime? LeaveYearTo { get; set; }

        public int? LeaveType { get; set; }

        public decimal? LeaveBroughtForward { get; set; }

        public decimal? LeaveEnjoied { get; set; }

        public decimal? ClosingLeaveBalance { get; set; }

        public decimal? LeaveDeducted { get; set; }

        public int? FiscalYearId { get; set; }
    }
}
