using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_LFAApplicationDetails
    {
        [Key]
        public int LFAAppDetailsId { get; set; }

        public int? LFAApplicationId { get; set; }

        public int? CTCId { get; set; }

        public decimal? CTCValue { get; set; }
    }
}
