using System;
using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_DeductionInformation
    {
        [Key]
        public int LeaveDeductionInfoId { get; set; }

        public int HrRecordId { get; set; }

        public int LeaveTypeId { get; set; }

        public decimal DeductedLeave { get; set; }

        public DateTime AttendanceMonth { get; set; }

        public int? ProcessBy { get; set; }

        public DateTime? ProcessDate { get; set; }
    }
}
