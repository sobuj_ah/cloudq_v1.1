using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Policy
    {
        [Key]
        public int LeavePolicyId { get; set; }

        public int? LeaveTypeId { get; set; }

        public int? EncashmentType { get; set; }

        public int? IsCertificateRequired { get; set; }

        public int? NoOfDays { get; set; }

        public int? IsLeavePlanApplicable { get; set; }

        public int? MaxAvilTime { get; set; }

        public int? EligiblityTime { get; set; }

        public int? IsActive { get; set; }

        public int? IsHolidayReplacement { get; set; }

        public int? IsShortLeave { get; set; }

        public decimal? ShortLeaveDuration { get; set; }

        public int? MaxChange { get; set; }

        public int? IsLeaveMarge { get; set; }

        public int? IsHolidayMarge { get; set; }

        public int? IsLFAApplicable { get; set; }

        public int? LFAMinimumDays { get; set; }

        public int? IsDeligationApplicable { get; set; }
    }
}
