using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Reason
    {
        [Key]
        public int LeaveReasonId { get; set; }

        [StringLength(250)]
        public string Reason { get; set; }

        public bool? IsActive { get; set; }
    }
}
