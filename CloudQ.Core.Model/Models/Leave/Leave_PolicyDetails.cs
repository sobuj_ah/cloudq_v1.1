using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_PolicyDetails
    {
        [Key]
        public int LeavePolicyDetailsId { get; set; }

        public int LeavePolicyId { get; set; }

        public int? EmployeeTypeId { get; set; }

        public int? GenderId { get; set; }

        public int? AllocatedDays { get; set; }

        public int? MaxEncash { get; set; }

        public int? MaxForward { get; set; }

        public int? MaxBalance { get; set; }

        public int? MaxConsicativeDays { get; set; }

        public int? MaxMonthlyDays { get; set; }

        public int? ApplicationBeforeDays { get; set; }
    }
}
