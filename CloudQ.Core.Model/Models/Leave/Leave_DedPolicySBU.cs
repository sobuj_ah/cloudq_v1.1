using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_DedPolicySBU
    {
        [Key]
        public int LeaveDedPolicySBUId { get; set; }

        public int? LeaveDedPolicyId { get; set; }

        public int? CompanyId { get; set; }
    }
}
