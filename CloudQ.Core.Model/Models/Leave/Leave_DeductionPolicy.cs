using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_DeductionPolicy
    {
        [Key]
        public int LeaveDedPolicyId { get; set; }

        [StringLength(250)]
        public string PolicyName { get; set; }

        public int? WithoutPayLeaveMap { get; set; }

        public int? IsActive { get; set; }
    }
}
