using System;
using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Planed
    {
        [Key]
        public int LeavePlaneId { get; set; }

        public int HrRecordId { get; set; }

        public int LeaveTypeId { get; set; }

        public DateTime PlaneDate { get; set; }

        public int Status { get; set; }

        public int? FiscalYearId { get; set; }

        public int? IsApproved { get; set; }

        public int? IsDrafted { get; set; }
    }
}
