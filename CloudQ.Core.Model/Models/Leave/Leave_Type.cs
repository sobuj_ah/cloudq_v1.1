using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Type
    {
        [Key]
        public int LeaveTypeId { get; set; }

        [StringLength(100)]
        public string LeaveTypeName { get; set; }

        [StringLength(5)]
        public string LeaveTypeCode { get; set; }

        public bool? IsActive { get; set; }

        public int? IsRemarksApplicable { get; set; }
    }
}
