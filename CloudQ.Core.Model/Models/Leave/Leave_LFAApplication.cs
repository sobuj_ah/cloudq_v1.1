using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_LFAApplication
    {
        [Key]
        public int LFAApplicationId { get; set; }

        public int? HrRecordId { get; set; }

        public int? LeaveId { get; set; }

        public int? LeaveDays { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeaveDateFrom { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeaveDateTo { get; set; }

        public string LeaveReason { get; set; }

        public int? LFAStateId { get; set; }

        public int? LFARecommendedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LFARecommendedDate { get; set; }

        public int? LFAApproveBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LFAApproveDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LFASubmitDate { get; set; }

        public string Comments { get; set; }

        public string Address { get; set; }

        public int? LFATotalAmount { get; set; }
    }
}
