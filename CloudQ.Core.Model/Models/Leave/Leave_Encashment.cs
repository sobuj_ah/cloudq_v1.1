using System;
using System.ComponentModel.DataAnnotations;

namespace CloudQ.Core.Model.Models.Leave
{
    public partial class Leave_Encashment
    {
        [Key]
        public int EncashmentId { get; set; }

        public int? LeaveType { get; set; }

        public int? EncashmentType { get; set; }

        public DateTime? AppliedDate { get; set; }

        public int? ApproverId { get; set; }

        public DateTime? ApproveDate { get; set; }

        public int? StateId { get; set; }

        public decimal? AccumulationDays { get; set; }

        public int? HrRecordId { get; set; }

        public DateTime? LeaveYearForm { get; set; }

        public DateTime? LeaveYearTo { get; set; }

        public int? PaidBy { get; set; }

        public DateTime? PaidDate { get; set; }

        public int? PaySlipNo { get; set; }

        public int? EncashmentAmount { get; set; }
    }
}
