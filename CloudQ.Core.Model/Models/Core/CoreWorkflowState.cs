using CloudQ.Infrastructure.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreWorkflowState")]
    public class CoreWorkflowState : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkflowStateId { get; set; }

        [Required]
        [StringLength(50)]
        public string StateName { get; set; }

        public int MenuId { get; set; }

        public bool? IsDefaultStart { get; set; }

        public int? Flag { get; set; }

        public virtual ICollection<CoreWorkflowAction> CoreWorkflowActions { get; set; }
    }
}
