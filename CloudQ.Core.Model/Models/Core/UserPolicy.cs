﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Model.Models.Core
{
    public class UserPolicy : BaseModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserPolicyId { get; set; }
        public int CompanyId { get; set; }
        public int MinLoginLength { get; set; }
        public int MinPassLength { get; set; }
        public int PassType { get; set; }
        public bool SpecialCharAllowed { get; set; }
        public int WrongAttemptNo { get; set; }
        public int ChangePassDays { get; set; }
        public bool ChangePassFirstLogin { get; set; }
        public int PassExpiryDays { get; set; }
        public string ResetPass { get; set; }
        public int PassResetBy { get; set; }
        public int OldPassUseRestriction { get; set; }
        public bool OdbcClientList { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int IsPasswordChange { get; set; }
        public int IsPasswordExpire { get; set; }
        public int IsWebLoginEnable { get; set; }

    }
}
