using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreWorkflowAction")]
    public class CoreWorkflowAction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkflowActionId { get; set; }
        public int WorkflowStateId { get; set; }
        [Required]
        [StringLength(50)]
        public string ActionName { get; set; }
        public string ActionTitle { get; set; }
        public int NextStateId { get; set; }
    }
}
