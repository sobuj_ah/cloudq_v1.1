﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Model.Models.Core
{
    public class UserMapping
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserMappingId { get; set; }
        public Guid UserUId { get; set; }
        public Guid RefUId { get; set; }
        public int RefTypeId { get; set; }


    }
}
