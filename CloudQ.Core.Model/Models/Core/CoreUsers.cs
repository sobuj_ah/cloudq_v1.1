using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreUsers")]
    public class CoreUsers : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [StringLength(20)]
        public string LoginId { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }

        [StringLength(250)]
        public string Password { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public int? FailedLoginCount { get; set; }
        public bool? IsExpired { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

       public int? UserTypeId { get; set; }
        public virtual CoreUserType CoreUserType { get; set; }


    }
}
