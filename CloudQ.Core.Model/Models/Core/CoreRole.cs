using CloudQ.Infrastructure.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreRole")]
    public class CoreRole : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleId { get; set; }

        [StringLength(100)]
        public string RoleName { get; set; }

        public int? IsDefault { get; set; }

        public virtual ICollection<CoreRolePermission> CoreGroupPermissions { get; set; }
    }
}
