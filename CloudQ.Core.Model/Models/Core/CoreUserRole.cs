using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreUserRole")]
    public class CoreUserRole
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleMembersId { get; set; }
        
        public int RoleId { get; set; }
        public int UserId { get; set; }
    }
}
