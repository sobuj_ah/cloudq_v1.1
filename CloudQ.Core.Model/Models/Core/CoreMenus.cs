using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreMenus")]
    public class CoreMenus : BaseModel
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MenuId { get; set; }
        public int ModuleId { get; set; }

        [Required]
        [StringLength(50)]
        public string MenuName { get; set; }

        [StringLength(200)]
        public string MenuPath { get; set; }

        public int? ParentId { get; set; }

        public int? Sequence { get; set; }

    }
}
