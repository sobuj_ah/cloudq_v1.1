using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreModules")]
    public class CoreModules:BaseModel
    {
        [Key]
        public int ModuleId { get; set; }

        [Required]
        [StringLength(50)]
        public string ModuleName { get; set; }
    }
}
