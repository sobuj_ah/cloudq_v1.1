using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreAppLicense")]
    public class CoreAppLicense
    {


        [Key]

        public int CoreAppLicenseId { get; set; }

        [StringLength(50)]
        public string LicenseFor { get; set; }

        [StringLength(50)]
        public string ProductCode { get; set; }

        [StringLength(50)]
        public string CodeBaseVersion { get; set; }

        [StringLength(50)]
        public string LicenseNumber { get; set; }

        [StringLength(50)]
        public string LicenseType { get; set; }

        [StringLength(50)]
        public string CompanyLicense { get; set; }

        [StringLength(50)]
        public string LocationLicense { get; set; }

        [StringLength(50)]
        public string UserLicense { get; set; }

        [StringLength(50)]
        public string ServerId { get; set; }

        public int IsActive { get; set; }
    }
}
