using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Core
{
    [Table("CoreRolePermission")]
    public class CoreRolePermission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionId { get; set; }

        [StringLength(50)]
        public string PermissionTableName { get; set; }
        public int? ReferenceId { get; set; }
        public int? ParentPermission { get; set; }
        public virtual CoreRole CoreRoles { get; set; }
    }
}
