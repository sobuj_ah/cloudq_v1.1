using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Core.Model.Models.Notification
{
    [Table("SMSSent")]
    public class SMSSent
    {
        public int ID { get; set; }

        [StringLength(1000)]
        public string SMSText { get; set; }

        [StringLength(25)]
        public string MobileNumber { get; set; }

        public DateTime? RequestDateTime { get; set; }

        public DateTime? DeliveryDateTime { get; set; }

        public int? Status { get; set; }

        public long? ReplyFor { get; set; }

        [StringLength(25)]
        public string SimNumber { get; set; }

        public int? NoOfTry { get; set; }

        public int? MessageReference { get; set; }
    }
}
