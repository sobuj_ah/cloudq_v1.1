﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Model.Models.Notification
{
    public class SMSRecieved
    {
        public int ID { get; set; }
        public int SMSIndex { get; set; }
        [StringLength(1000)]
        public string SMSText { get; set; }

        [StringLength(25)]
        public string FromMobileNumber { get; set; }

        public DateTime? RecievedDate { get; set; }
        public DateTime? SystemDate { get; set; }
        public int? Status { get; set; }

        public int? PartNo { get; set; }
        public int? PartMsgReference { get; set; }
        public int? TotalPart { get; set; }
        [StringLength(25)]
        public string SimNumber { get; set; }

   
    }
}
