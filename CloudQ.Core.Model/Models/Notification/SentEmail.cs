using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Core.Model.Models.Notification
{
    [Table("SentEmail")]
    public partial class SentEmail
    {
        public int SentEmailId { get; set; }

        [StringLength(250)]
        public string EmailFrom { get; set; }

        [StringLength(250)]
        public string EmailTo { get; set; }

        [StringLength(1000)]
        public string Subject { get; set; }

        [Column(TypeName = "ntext")]
        public string Body { get; set; }

        [StringLength(500)]
        public string CcAddress { get; set; }

        [Column(TypeName = "ntext")]
        public string Attachment { get; set; }

        public int? Status { get; set; }

        public DateTime? MailCreateDate { get; set; }

        public DateTime? MailSendDate { get; set; }
    }
}
