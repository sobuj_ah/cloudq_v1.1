﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Core.Model.Models.Org;

namespace CloudQ.Core.DataService.Configurations
{
    public class CompanyConfiguration : EntityTypeConfiguration<OrgCompany>
    {

        public CompanyConfiguration()
        {
            Property(c => c.CompanyId).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnOrder(1);
            Property(c => c.Id).IsRequired().HasColumnOrder(2);
            Property(c => c.CompanyCode).IsRequired().HasMaxLength(20);
            Property(c => c.CompanyName).IsRequired().HasMaxLength(100);
            Property(c => c.WebAddress).HasMaxLength(50);
            Property(c => c.Email).HasMaxLength(100);
            Property(c => c.Phone).HasMaxLength(50);
            Property(c => c.Location).HasMaxLength(250);
            Property(c => c.Fax).HasMaxLength(100);
            Property(c => c.LogoImage);
            Property(c => c.BannerImage);

        }
    }
}
