using System;
using System.Data.Entity.Migrations;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.Models.Core;
using CloudQ.UitilityTools;
using CloudQ.UitilityTools.StaticData;

namespace CloudQ.Core.DataService.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CoreDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
            //AutomaticMigrationsEnabled = false;
            ContextKey = "CoreDbContext";

        }

        protected override void Seed(CoreDbContext context)
        {



            base.Seed(context);
            context.CoreUsers.AddOrUpdate(
                u => u.LoginId, new CoreUsers
                {
                    UserName = "Administrators",
                    LoginId = "admin",
                    Password = EncryptDecryptHelper.Encrypt("admin"),
                    IsActive = true,
                    CreateBy = 1,
                    CreateDate = DateTime.Now,
                    LastLoginTime = null,
                    LastUpdateTime = null,
                    IsExpired = false,
                    FailedLoginCount = 0

                });



            context.Menus.AddOrUpdate(
                u => u.MenuId,
                new CoreMenus()
                {
                    MenuId = 1,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.CoreModule,
                    MenuName = "Core Settings",
                    MenuPath = "",
                    IsActive = true
                },

                new CoreMenus()
                {
                    MenuId = 2,
                    Id = Guid.NewGuid(),

                    ModuleId = (int)ModuleEnum.CoreModule,
                    MenuName = "User Settings",
                    MenuPath = "../Core/Users",
                    ParentId = 1,
                    IsActive = true
                },
                new CoreMenus()
                {
                    MenuId = 3,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.CoreModule,
                    MenuName = "Menu Settings",
                    MenuPath = "../Core/Menu",
                    ParentId = 1
                },
                new CoreMenus()
                {
                    MenuId = 4,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.CoreModule,
                    MenuName = "Role Settings",
                    MenuPath = "../Core/Role",
                    ParentId = 1,
                    IsActive = true
                },
                new CoreMenus()
                {
                    MenuId = 5,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.OrgModule,
                    MenuName = "Company Settings",
                    MenuPath = "../Org/Company",
                    ParentId = 1,
                    IsActive = true
                },
                new CoreMenus()
                {
                    MenuId = 6,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.PatientQModule,
                    MenuName = "Master Settings",
                    MenuPath = "",
                    IsActive = true

                },
                new CoreMenus()
                {
                    MenuId = 7,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.PatientQModule,
                    MenuName = "Doctor Information",
                    MenuPath = "../PatientQ/Doctor",
                    ParentId = 6,
                    IsActive = true

                },
                new CoreMenus()
                {
                    MenuId = 8,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.PatientQModule,
                    MenuName = "Patient Information",
                    MenuPath = "../PatientQ/Patient",
                    ParentId = 6,
                    IsActive = true

                },
                new CoreMenus()
                {
                    MenuId = 9,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.PatientQModule,
                    MenuName = "Test Information",
                    MenuPath = "../PatientQ/TestInfo",
                    ParentId = 6,
                    IsActive = true

                },
                new CoreMenus()
                {
                    MenuId = 10,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.PatientQModule,
                    MenuName = "Transaction",
                    MenuPath = "",
                    IsActive = true

                },
                new CoreMenus()
                {
                    MenuId = 11,
                    Id = Guid.NewGuid(),
                    ModuleId = (int)ModuleEnum.PatientQModule,
                    MenuName = "Patient Test Invoice",
                    MenuPath = "../PatientQ/PatientTest",
                    ParentId = 10,
                    IsActive = true

                });

            context.CoreRoles.AddOrUpdate(s => s.RoleName,
                new CoreRole { Id = Guid.NewGuid(), RoleId = 1, RoleName = "Administrators" },
                new CoreRole { Id = Guid.NewGuid(), RoleId = 2, RoleName = "Managers" },
                new CoreRole { Id = Guid.NewGuid(), RoleId = 3, RoleName = "Users" },
                new CoreRole { Id = Guid.NewGuid(), RoleId = 4, RoleName = "Doctors" },
                new CoreRole { Id = Guid.NewGuid(), RoleId = 5, RoleName = "Patients" }



                );

            context.CoreUserRoles.AddOrUpdate(s => s.RoleId, new CoreUserRole
            {
                UserId = 1,
                RoleId = 1
            });
            //context.Modules.AddOrUpdate(s => s.ModuleName,
            //    new CoreModules { Id = Guid.NewGuid(), ModuleId = 1, ModuleName = "Control Panel" },
            //    new CoreModules { Id = Guid.NewGuid(), ModuleId = 2, ModuleName = "Core System" },
            //    new CoreModules { Id = Guid.NewGuid(), ModuleId = 3, ModuleName = "Patient-Q Management " }
            //    );


            context.SaveChanges();

        }
    }
}
