﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.Models.Core;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CloudQ.Core.DataService.Migrations
{
    public class BigBangInitializer : DropCreateDatabaseIfModelChanges<CoreDbContext>
    {
        protected override void Seed(CoreDbContext context)
        {
            Initialize(context);
            base.Seed(context);
        }

        public void Initialize(CoreDbContext context)
        {
            try
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
                {
                    AllowOnlyAlphanumericUserNames = false
                };
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

                if (!roleManager.RoleExists("Admin"))
                {
                    roleManager.Create(new IdentityRole("Admin"));
                }

                if (!roleManager.RoleExists("Member"))
                {
                    roleManager.Create(new IdentityRole("Member"));
                }

                var user = new ApplicationUser()
                {
                    Email = "sobuj@gmail.com",
                    UserName = "test@test.com",
                    FirstName = "sobuj",
                    LastName = "Islam",


                };



                var userResult = userManager.Create(user, "sobuj@123");

                if (userResult.Succeeded)
                {
                    userManager.AddToRole<ApplicationUser, string>(user.Id, "Admin");
                }

               
                context.CoreUserTypes.Add(
                            new CoreUserType
                            {
                                UserType = "Admin",
                                Id = Guid.NewGuid()
                            }
                                 
                    );
                context.CoreUserTypes.Add(
                           new CoreUserType
                           {
                               UserType = "Doctor",
                               Id = Guid.NewGuid()
                           }
                   );
                context.CoreUserTypes.Add(
                         new CoreUserType
                         {
                             UserType = "Patient",
                             Id = Guid.NewGuid()
                         }
                  );

                context.Commit();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
