namespace CloudQ.Core.DataService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sd : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "cqm.CoreUsers", name: "CoreUserType_UserTypeId", newName: "UserTypeId");
            RenameIndex(table: "cqm.CoreUsers", name: "IX_CoreUserType_UserTypeId", newName: "IX_UserTypeId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "cqm.CoreUsers", name: "IX_UserTypeId", newName: "IX_CoreUserType_UserTypeId");
            RenameColumn(table: "cqm.CoreUsers", name: "UserTypeId", newName: "CoreUserType_UserTypeId");
        }
    }
}
