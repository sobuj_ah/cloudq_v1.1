﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Threading.Tasks;
using CloudQ.Core.DataService.Configurations;
using CloudQ.Core.DataService.Migrations;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Model.Models.Global;
using CloudQ.Core.Model.Models.Notification;
using CloudQ.Core.Model.Models.Org;
using CloudQ.Infrastructure.Context;
using CloudQ.Infrastructure.Entity;
using CloudQ.Logging.Logging;
using CloudQ.UitilityTools.StaticData;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CloudQ.Core.DataService.DB
{
    public class CoreDbContext : IdentityDbContext<ApplicationUser>, IDbContext
    {


        private ObjectContext _objectContext;
        private DbTransaction _transaction;
        private static readonly object Lock = new object();
        private static bool _databaseInitialized;

        public CoreDbContext()
            : base("SqlConnectionString")
        {

        }

        public CoreDbContext(string nameOrConnectionString, ILogger logger)
            : base(nameOrConnectionString)
        {
            if (logger != null)
            {
                Database.Log = logger.Log;
            }

            if (_databaseInitialized)
            {
                return;
            }
            lock (Lock)
            {
                if (!_databaseInitialized)
                {
                    Database.SetInitializer(new BigBangInitializer());
                    _databaseInitialized = true;
                }
            }
        }


        #region DbSets
        public virtual IDbSet<OrgCompany> Companies { get; set; }
        public virtual IDbSet<CoreRole> CoreRoles { get; set; }
        public virtual IDbSet<CoreUserRole> CoreUserRoles { get; set; }
        public virtual IDbSet<CoreRolePermission> CoreRolePermissions { get; set; }
        public virtual IDbSet<CoreMenus> Menus { get; set; }
        public virtual IDbSet<CoreModules> Modules { get; set; }
        public virtual IDbSet<CoreUsers> CoreUsers { get; set; }
        public virtual IDbSet<CoreWorkflowState> CoreWorkflowStates { get; set; }
        public virtual IDbSet<CoreWorkflowAction> CoreWorkflowActions { get; set; }


        public virtual IDbSet<SMSSent> SMSSents { get; set; }
        public virtual IDbSet<SMSRecieved> SMSRecieved { get; set; }
        public virtual IDbSet<SentEmail> SentEmails { get; set; }

        public virtual IDbSet<CoreUserType> CoreUserTypes { get; set; }
        public virtual IDbSet<VerificationCode> VerificationCodes { get; set; }

        public virtual IDbSet<UserMapping> UserMappings { get; set; }

        #endregion




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema(DbSchema.cqm.ToString());
            //modelBuilder.Configurations.Add(new ContactConfiguration());
            modelBuilder.Configurations.Add(new CompanyConfiguration());

        }

        public static CoreDbContext Create()
        {
            return new CoreDbContext(nameOrConnectionString: "SqlConnectionString", logger: null);
        }




        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseModel
        {
            return base.Set<TEntity>();
        }

        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            UpdateEntityState(entity, EntityState.Added);
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            UpdateEntityState(entity, EntityState.Modified);
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            UpdateEntityState(entity, EntityState.Deleted);
        }

        public void BeginTransaction()
        {
            this._objectContext = ((IObjectContextAdapter)this).ObjectContext;
            if (_objectContext.Connection.State == ConnectionState.Open)
            {
                return;
            }
            _objectContext.Connection.Open();
            _transaction = _objectContext.Connection.BeginTransaction();
        }

        public int Commit()
        {
            try
            {
                BeginTransaction();
                var saveChanges = SaveChanges();
                _transaction.Commit();

                return saveChanges;
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public DbEntityEntry Entry(BaseModel entity)
        {
            return base.Entry(entity);
        }

        public async Task<int> CommitAsync()
        {
            try
            {
                BeginTransaction();
                var saveChangesAsync = await SaveChangesAsync();
                _transaction.Commit();

                return saveChangesAsync;
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        private void UpdateEntityState<TEntity>(TEntity entity, EntityState entityState) where TEntity : BaseModel
        {
            var dbEntityEntry = GetDbEntityEntrySafely(entity);
            dbEntityEntry.State = entityState;
        }

        private DbEntityEntry GetDbEntityEntrySafely<TEntity>(TEntity entity) where TEntity : BaseModel
        {
            var dbEntityEntry = Entry<TEntity>(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                Set<TEntity>().Attach(entity);
            }
            return dbEntityEntry;
        }
    }
}
