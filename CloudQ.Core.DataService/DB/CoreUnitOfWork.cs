﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Base;
using CloudQ.Infrastructure.Context;
using CloudQ.Infrastructure.Entity;
using CloudQ.Infrastructure.Repositories;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;

namespace CloudQ.Core.DataService.DB
{
    public class CoreUnitOfWork : ICoreUnitOfWork
    {
        private readonly IDbContext _context;
        private bool _disposed;
        private Hashtable _repositories;

        public CoreUnitOfWork()
        {
            _context = new CoreDbContext();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : BaseModel
        {
            if (_repositories == null)
            {
                _repositories = new Hashtable();
            }

            var type = typeof(TEntity).Name;

            if (_repositories.ContainsKey(type))
            {
                return (IRepository<TEntity>)_repositories[type];
            }

            var repositoryType = typeof(BaseRepository<>);

            _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), _context));

            return (IRepository<TEntity>)_repositories[type];
        }

        public void BeginTransaction()
        {
            _context.BeginTransaction();
        }

        public int Commit()
        {
            return _context.Commit();
        }

        public Task<int> CommitAsync()
        {
            return _context.CommitAsync();
        }

        public void Rollback()
        {
            _context.Rollback();
        }

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //public virtual void Dispose(bool disposing)
        //{
        //    if (!_disposed && disposing)
        //    {
        //        _context.Dispose();
        //        foreach (IDisposable repository in _repositories.Values)
        //        {
        //            repository.Dispose();
        //        }
        //    }
        //    _disposed = true;
        //}
    }
}