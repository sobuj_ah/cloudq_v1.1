﻿using System;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;
using CloudQ.Infrastructure.Repositories;

namespace CloudQ.Infrastructure.UOW
{
    public interface IUnitOfWork  
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : BaseModel;
        
        void BeginTransaction();
        
        int Commit();
        
        Task<int> CommitAsync();

        void Rollback();

        //void Dispose(bool disposing);

    }
}
