﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.Infrastructure.Context
{
    public interface IDbContext : IDisposable
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseModel;

        void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseModel;

        void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseModel;

        void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseModel;
        
        void BeginTransaction();
        
        int Commit();
        Task<int> CommitAsync();
        
        void Rollback();

        DbEntityEntry Entry(BaseModel entity);
    }
}
