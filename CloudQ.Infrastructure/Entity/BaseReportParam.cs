﻿using System;
using System.Collections.Generic;

namespace CloudQ.Infrastructure.Entity
{
    public class BaseReportModel<T>
    {
        public string RptFileName { get; set; }
        public string ReportTitle { get; set; }
        public string ReportType { get; set; }
        //public DateTime FromDate { get; set; }
        //public DateTime ToDate { get; set; }
        public Boolean IsPassParamToCr { get; set; }
        public List<T> DataSource { get; set; }
        //public string CompanyName { get; set; }
        //public string BranchName { get; set; }
        //public string Location { get; set; }
         
        public byte[] ReportLogo { get; set; }

        public bool IsSubReport { get; set; }
    }


}
