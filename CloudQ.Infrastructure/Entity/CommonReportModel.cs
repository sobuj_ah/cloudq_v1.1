﻿using System;

namespace CloudQ.Infrastructure.Entity
{
    public class CommonReportModel 
    {
        //public string RptFileName { get; set; }
        public string ReportTitle { get; set; }
        //public string ReportType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
       
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyMobileNo { get; set; }
        public string Location { get; set; }

        public byte[] ReportLogo { get; set; }
    }
}