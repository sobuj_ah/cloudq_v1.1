﻿using System;

namespace CloudQ.Infrastructure.Entity
{
    public interface IEntity
    {
        Guid Id { get; set; }
        bool IsActive { get; set; }
        DateTime? CreateDate { get; set; }
        DateTime? UpdateDate { get; set; }
        int? CreateBy { get; set; }
        int? UpdateBy { get; set; }
    }
}
