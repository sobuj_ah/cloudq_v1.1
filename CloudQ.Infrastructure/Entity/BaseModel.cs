﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.Infrastructure.Entity
{
    public abstract class BaseModel : IEntity
    {


        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? CreateBy { get; set; }
        public int? UpdateBy { get; set; }

        protected BaseModel()
        {
            CreateDate = DateTime.Now;
            IsActive = true;
           // Id = Guid.NewGuid();

        }


    }
}
