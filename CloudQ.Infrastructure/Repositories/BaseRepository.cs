﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Base;
using CloudQ.Infrastructure.Context;
using CloudQ.Infrastructure.Entity;
using System.Linq;
using System.Data.Entity.Core;
using CloudQ.Infrastructure.GenericRepository;

namespace CloudQ.Infrastructure.Repositories
{
    public class BaseRepository<TEntity> : Disposable, IRepository<TEntity> where TEntity : BaseModel
    {
        private readonly IDbContext _dataContext;

        private IDbSet<TEntity> Dbset
        {
            get { return _dataContext.Set<TEntity>(); }
        }

        public BaseRepository(IDbContext dbContext)
        {
            _dataContext = dbContext;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Dbset.AsEnumerable();
        }

        public TEntity GetById(int id)
        {
            return Dbset.Find(id);
        }

        public void Add(TEntity entity)
        {
            Dbset.Add(entity);
        }

        public void Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
              Dbset.Add(entity);
           _dataContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null)
                throw new ObjectNotFoundException("entity");
            Dbset.Remove(entity);
        }

        public void Delete(TEntity entity)
        {
            Dbset.Remove(entity);
        }

        public void Delete(Expression<Func<TEntity, bool>> @where)
        {
            IEnumerable<TEntity> objects = Dbset.Where(where).AsEnumerable();
            foreach (TEntity obj in objects)
                Dbset.Remove(obj);
        }

        public TEntity Get(Expression<Func<TEntity, bool>> @where)
        {
            return Dbset.Where(where).FirstOrDefault();
        }

        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return Dbset.Where(where).ToList();
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await Dbset.ToListAsync();
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> @where)
        {
            return await Dbset.Where(where).FirstOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> @where)
        {
            return await Dbset.Where(where).ToListAsync();
        }

        //protected override void DisposeCore()
        //{
        //    if (_dataContext != null)
        //        _dataContext.Dispose();
        //}

       

        public TEntity GetById(Guid id)
        {
            return Dbset.FirstOrDefault(s=>s.Id== id);
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await Dbset.FirstOrDefaultAsync(s => s.Id == id);
        }

        
    }
}
