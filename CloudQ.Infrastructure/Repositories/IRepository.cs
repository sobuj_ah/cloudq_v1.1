﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;
using System.Linq.Expressions;

namespace CloudQ.Infrastructure.Repositories
{
    public interface IRepository<TEntity> : IDisposable where TEntity : BaseModel
    {
            
      

        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int id);
        void Delete(Expression<Func<TEntity, bool>> where);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        TEntity GetById(Guid id);
        TEntity Get(Expression<Func<TEntity, bool>> where);
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where);
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> where);
        Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> where);
        Task<TEntity> GetByIdAsync(Guid id);

    }
}
