﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;
using CloudQ.Infrastructure.Repositories;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;
using System.Linq.Expressions;

namespace CloudQ.Infrastructure.Services
{
    public class BaseService<TEntity> : IService<TEntity> where TEntity : BaseModel
    {
        public IUnitOfWork _unitOfWork { get; private set; }
        public IRepository<TEntity> _repository;
        private bool _disposed;

        public BaseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.Repository<TEntity>();
        }

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }
        public void Add(TEntity entity)
        {
            _repository.Add(entity);
        }
        public Result Save(TEntity entity)
        {
            var oResult = new Result();

            try
            {
                //if (IsExist(entity.Id))
                //{
                //    var nEnt = GetById(entity.Id);


                //    _repository.Update(nEnt);
                //    oResult.Action = ActionStatus.Update;
                //    oResult.Message = "The Record is Updated Successfully ";
                //}
                //else
                //{
                entity.Id = Guid.NewGuid();
                _repository.Add(entity);
                oResult.Action = ActionStatus.Save;
                oResult.Message = "The Record is Saved Successfully ";



                //}
                _unitOfWork.Commit();
                // oResult.Data = entity;
                oResult.Status = true;
                oResult.Operation = Operation.Success;
            }
            catch (Exception ex)
            {

                oResult.Status = false;
                oResult.Operation = Operation.Error;
                oResult.Exception = ex;
                oResult.Message = ex.Message;
            }



            return oResult;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            _repository.Add(entity);
            await _unitOfWork.CommitAsync();

            return entity;
        }

        public Result Update(TEntity entity)
        {
            var oResult = new Result();
            _repository.Update(entity);
            _unitOfWork.Commit();
            //oResult.Data = entity;
            oResult.Status = true;
            oResult.Action = ActionStatus.Update;
            oResult.Operation = Operation.Success;
            oResult.Message = "The Record is Updated Successfully ";
            return oResult;
        }

        public Result Delete(TEntity entity)
        {
            var oResult = new Result();
            _repository.Delete(entity);
            oResult.Status = true;
            oResult.Action = ActionStatus.Update;
            oResult.Operation = Operation.Success;
            oResult.Message = "The Record is Deleted Successfully ";
            _unitOfWork.Commit();
            return oResult;
        }


        public Task UpdateAsync(TEntity entity)
        {
            _repository.Update(entity);
            return _unitOfWork.CommitAsync();
        }

        public Task DeleteAsync(TEntity entity)
        {
            _repository.Delete(entity);
            return _unitOfWork.CommitAsync();
        }

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //public virtual void Dispose(bool disposing)
        //{
        //    if (!_disposed && disposing)
        //    {
        //        _unitOfWork.Dispose();
        //    }
        //    _disposed = true;
        //}

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }
        public Task<List<TEntity>> GetAllAsync()
        {
            return _repository.GetAllAsync();
        }

        public Task<TEntity> GetByIdAsync(Guid id)
        {
            return _repository.GetByIdAsync(id);
        }

        public TEntity GetById(Guid id)
        {
            return _repository.GetById(id);
        }

        public GridEntity<TEntity> DataSource(GridOptions options)
        {
            var data = _repository.GetAll().Take(options.take);
            var reuslt = new GridEntity<TEntity>
            {
                Items = data.ToList(),
                TotalCount = _repository.GetAll().Count()
            };
            return reuslt;
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> pred)
        {
            return _repository.GetMany(pred);
        }

        public bool IsExist(Guid id)
        {
            var item = GetById(id);
            if (item != null && item.Id != Guid.Empty)
                return true;
            else
                return false;
        }
    }
}
