﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;
using CloudQ.UitilityTools;

namespace CloudQ.Infrastructure.Services
{
    public interface IService<TEntity> 
    {

        void Add(TEntity entity);
        void SaveChanges();
        Result Save(TEntity entity);

        Task<TEntity> AddAsync(TEntity entity);

        Result Update(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Result Delete(TEntity entity);

        Task DeleteAsync(TEntity entity);

        Task<List<TEntity>> GetAllAsync();
        IEnumerable<TEntity> GetAll();

        Task<TEntity> GetByIdAsync(Guid id);

        TEntity GetById(Guid id);
        bool IsExist(Guid id);


        GridEntity<TEntity> DataSource(GridOptions options);
    }
}
