﻿using System;
using System.Text;

namespace CloudQ.Infrastructure.Base
{
    [AttributeUsage(AttributeTargets.Class |
                       AttributeTargets.Struct |AttributeTargets.Method,
                       AllowMultiple = true)]
    // multiuse attribute
    public class SystemAuthor : Attribute
    {
        public string Author { get; set; }
        

        public SystemAuthor()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Mohidul Islam Sobuj");
            sb.AppendLine("Sr. Software Engineer");
            sb.AppendLine("01764010666");
            sb.AppendLine("This software architectural design by Mr. Sobuj");

        }
        public SystemAuthor(string author, string position)
        {

        }
    }
}
