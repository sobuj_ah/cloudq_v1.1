﻿using System;
using CloudQ.Infrastructure.Entity;
using CloudQ.UitilityTools;

namespace CloudQ.Infrastructure.Base
{
    public class CommonLibDataService
    {
        public object GetCommonComboData(string peram)
        {
            try
            {
                
                var objPeram = peram.Split(',');
                var condition = "";
                if (objPeram[3] != "")
                {
                    condition = "Where " + objPeram[3];
                }
                string query = string.Format(@"Select {0} as Id, {1} as Text from {2} {3}", objPeram[0], objPeram[1], objPeram[2], condition);
                return CommonDataService.GetListByQuery<CommonCombo>(query);
            }
            catch (Exception ex)
            {
                
            }
          return null;
        }
    }

    
}
