﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PrescribeSymptomDto 
    {
        
        public int PrescribeSymptomId { get; set; }
        public int SymptomId { get; set; }
        public int PrescriptionId { get; set; }
      
    }
}
