﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PrescribeDrugScheduleDto : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrescribeDrugScheduleId { get; set; }
        public int DrugScheduleId { get; set; }
        public virtual DrugSchedule DrugSchedule { get; set; }

        public int PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
    }
}
