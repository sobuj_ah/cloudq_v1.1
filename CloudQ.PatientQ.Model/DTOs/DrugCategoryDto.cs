﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class DrugCategoryDto
    {

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

    }
}
