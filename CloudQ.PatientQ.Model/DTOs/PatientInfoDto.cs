﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PatientInfoDto
    {
        public int PatientId { get; set; }

        public string PatientNo { get; set; }

        public string PatientName { get; set; }

        public string PatientBarCode { get; set; }

        public string Age { get; set; }

        public string Sex { get; set; }

        public string ContactNo { get; set; }

        public string EmailAddress { get; set; }

        public decimal Weight { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Description { get; set; }
        public string PhoneNo { get; set; }
        public string ProfileImage { get; set; }
        public string Password { get; set; }


    }
}
