﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class TestCategoryDto  
    {
        public int TestCategoryId { get; set; }
         
        public string CategoryName { get; set; }

        public int ParentCategoryId { get; set; }
        public string Description { get; set; }

    }
}
