﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PatientTestDto
    {
        public PatientTest PatientTest { get; set; }
        public virtual ICollection<PatientTestDetailsDto> PatientTestDetails { get; set; }
        public PatientInfo Patient { get; set; } 
       

    }
}
