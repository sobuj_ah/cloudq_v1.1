﻿using System.ComponentModel.DataAnnotations;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;
using System;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class TestInfoDto  
    {
        public Guid Id { get; set; }
        public int TestId { get; set; }
        public string Barcode { get; set; }
        [StringLength(200)]
        public string TestName { get; set; }
        public int TestCategoryId { get; set; }
        //public virtual TestCategory TestCategory { get; set; }
        public string CategoryName { get; set; }
        public string TestCode { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }


    }
}
