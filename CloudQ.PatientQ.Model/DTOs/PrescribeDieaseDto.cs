﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PrescribeDieaseDto : BaseModel
    {
        public int PrescribeDieaseId { get; set; }

        public int PrescriptionId { get; set; }
        public int DiseaseId { get; set; }

    }
}
