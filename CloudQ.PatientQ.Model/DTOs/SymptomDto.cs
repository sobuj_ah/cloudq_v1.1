﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class SymptomDto 
    {
       
        public int SymptomId { get; set; }
        public string SymptomTitle { get; set; }


    }
}
