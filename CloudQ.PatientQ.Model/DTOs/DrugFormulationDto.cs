﻿using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class DrugFormulationDto : BaseModel
    {
       
        public int FormulationId { get; set; }
        public string Formulation { get; set; }
    }
}
