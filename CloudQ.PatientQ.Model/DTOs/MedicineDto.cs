﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class MedicineDto 
    {
        
        public int MedicineId { get; set; }
        [StringLength(200)]
        public string MedicineName { get; set; }
        public string CommercialName { get; set; }

        public int? PharmaCompanyId { get; set; }
        public virtual PharmaCompany Company { get; set; }
        public int? GenericId { get; set; }
        public virtual DrugGeneric DrugGeneric { get; set; }
        public int? FormulationID { get; set; }
        public virtual DrugFormulation DrugFormulation { get; set; }
        public int? PackSizeId { get; set; }
        public virtual DrugPackSize DrugPackSize { get; set; }

        public virtual ICollection<DrugAvailableStrengths> DrugAvailableStrengthses { get; set; }


        public int? CategoryId { get; set; }
        public virtual DrugCategory DrugCategory { get; set; }
    }
}
