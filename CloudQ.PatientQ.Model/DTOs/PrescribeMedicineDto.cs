﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PrescribeMedicineDto
    {
        public int PrescribeMedicineId { get; set; }

        public int MedicineId { get; set; }

        //public int DrugScheduleId { get; set; }

        public int PrescriptionId { get; set; }


        public int DosageId { get; set; }

        public int NumberOfDosage { get; set; }

        public int MaxDays { get; set; }



    }
}
