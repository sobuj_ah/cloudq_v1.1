﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PatientTestDetailsDto
    {
        public string TestCode { get; set; }
        public string Barcode { get; set; }
        public string TestName { get; set; }
        public int DetailsId { get; set; }
        public int PatientTestId { get; set; }

        public int Quantity { get; set; }
        public int Price { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal DiscountRate { get; set; }
        public int TestId { get; set; }
        public decimal Amount { get; set; }

    }
}
