﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PrescriptionDto
    {
        public int PrescriptionId { get; set; }

        public int PatientId { get; set; }

        public int DoctorId { get; set; }

        public DateTime PrescribDate { get; set; }
        public string Comments { get; set; }

        public int AppointmentId { get; set; }

        public virtual ICollection<PrescribeMedicineDto> PrescribeMedicines { get; set; }
        public virtual ICollection<PrescribeDieaseDto> PrescribeDieases { get; set; }
        public virtual ICollection<PrescribeTestDto> PrescribeTests { get; set; }
        public virtual ICollection<PrescribeSymptomDto> PrescribeSymptoms { get; set; }




    }
}
