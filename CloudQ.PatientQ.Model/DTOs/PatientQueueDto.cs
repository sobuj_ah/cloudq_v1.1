﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class PatientQueueDto
    {
        public string DoctorName { get; set; }
        public string ContactNo { get; set; }
        public string PatientName { get; set; }
        public int AppointmentId { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public DateTime SerialDate { get; set; }
        public string Age { get; set; }
        public string SerialNo { get; set; }
        public string Sex { get; set; }
        public decimal Weight { get; set; }

        public int Status { get; set; }



    }
}
