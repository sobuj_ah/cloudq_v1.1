﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.DTOs
{
    public class DoctorDto
    {
        public int DoctorID { get; set; }
        [StringLength(20)]
        public string DoctorCode { get; set; }
        [StringLength(200)]
        public string DoctorName { get; set; }
        [StringLength(200)]
        public string Degree { get; set; }
        [StringLength(500)]
        public string Address { get; set; }
        [StringLength(50)]
        public string ContactNo { get; set; }
        [StringLength(50)]
        public string LicenseNo { get; set; }

    }
}
