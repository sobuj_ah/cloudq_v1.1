﻿using System;

namespace CloudQ.PatientQ.Model.RTOs
{
    public class InvoiceReportRto 
    {
        public string ProductCode { get; set; }
        public string Barcode { get; set; }
        public DateTime BillDate { get; set; }
        public DateTime DeliveryDate { get; set; }

      
        public string InvoiceNo { get; set; }

        
        public string SalesByName { get; set; }
        public string CustomerName { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string ContactNo { get; set; }

        public string RefdBy { get; set; }



        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double DiscountRate { get; set; }
        public double DiscountedPrice { get; set; }
        public double SalesPrice { get; set; }
        public string PayType { get; set; }
        public double PaidAmount { get; set; }
        public double DueAmount { get; set; }
        public string Remarks { get; set; }
       // public string PaymentStatus { get; set; }



    }
}