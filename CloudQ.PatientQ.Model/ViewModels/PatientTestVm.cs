﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.ViewModels
{
    public class PatientTestVm
    {

        public string PatientName { get; set; }
    
        public string PatientNo { get; set; }
        public int PatientTestId { get; set; }

        public string Barcode { get; set; }
        public string ContactNo { get; set; }

        
       
        public string InvoiceNo { get; set; }

        public DateTime SalesDate { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal DiscountRate { get; set; }
        public decimal CommissionAmount { get; set; }
        public decimal CommissionRate { get; set; }
        public decimal TaxAmount { get; set; }
        public int ReferredBy { get; set; }

        public double PaidAmount { get; set; }
        public double DueAmount { get; set; }
        public int PatientId { get; set; }
        public string Remarks { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int PaymentType { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }

    }
}
