﻿namespace CloudQ.PatientQ.Model.ViewModels
{
    public class MedicineVm
    {
        
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
        public string Formulation { get; set; }
        public string CategoryName { get; set; }
        public string GenericName { get; set; }
        public string PharmaCompanyName { get; set; }
        public string FormulationID { get; set; }
        public string GenericId { get; set; }
        public string PharmaCompanyId { get; set; }

         
    }
}
