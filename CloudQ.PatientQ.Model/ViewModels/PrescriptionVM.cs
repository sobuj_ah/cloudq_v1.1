﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.PatientQ.Model.Models;

namespace CloudQ.PatientQ.Model.ViewModels
{
    public class PrescriptionVm
    {
        public int PrescriptionId { get; set; }

        public int PatientId { get; set; }

        public int DoctorId { get; set; }
        public DateTime PrescribDate { get; set; }
        public string Comments { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
        public string Formulation { get; set; }
        public string CategoryName { get; set; }
        public string GenericName { get; set; }
        public string PharmaCompanyName { get; set; }
        public string FormulationID { get; set; }
        public string GenericId { get; set; }
        public string PharmaCompanyId { get; set; }
        //public DrugStrengths DrugStrengths { get; set; }
        public DrugDosage DrugDosage { get; set; }
        public decimal NumberOfDosage { get; set; }
        public int StrengthsId { get; set; }
        public string Strengths { get; set; }
        public int DosageId { get; set; }

    }
}
