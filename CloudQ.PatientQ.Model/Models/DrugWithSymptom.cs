﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class DrugWithSymptom : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DrugWithSymptomId { get; set; }
        public int SymptomId { get; set; }
        public virtual Symptom Symptom { get; set; }

        public int MedicineId { get; set; }
        public virtual Medicine Medicine { get; set; }

    }
}
