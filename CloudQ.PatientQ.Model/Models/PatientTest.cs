﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    [Table("PatientTest")]
    public class PatientTest : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PatientTestId { get; set; }

        public int? PrescriptionId { get; set; }

        public string Barcode { get; set; }

        public virtual ICollection<PatientTestDetails> PatientTestDetails { get; set; }
        //[StringLength(50)]
       
        public string InvoiceNo { get; set; }

        public DateTime SalesDate { get; set; }
        public int TotalQuantity { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal DiscountRate { get; set; }
        public decimal CommissionAmount { get; set; }
        public decimal CommissionRate { get; set; }
        public decimal TaxAmount { get; set; }
        public int ReferredBy { get; set; }
        public double NetAmount { get; set; }
        public double PaidAmount { get; set; }
        public double DueAmount { get; set; }
        public int PatientId { get; set; }
        public string Remarks { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int PaymentType { get; set; }     //1=Paid,2=Unpaid
    
        

    }
}
