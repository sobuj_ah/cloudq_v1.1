﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class Doctor : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DoctorID { get; set; }
        [StringLength(20)]
        public string DoctorCode { get; set; }
        [StringLength(200)]
        public string DoctorName { get; set; }
        [StringLength(200)]
        public string Degree { get; set; }
        [StringLength(500)]
        public string Address { get; set; }
        [StringLength(50)]
        public string ContactNo { get; set; }
        [StringLength(50)]
        public string LicenseNo { get; set; }

        public int DoctorTypeID { get; set; }
        //public virtual DoctorType DoctorType { get; set; }

        public string Title { get; set; }
        public int Gender { get; set; }
        public int EmailAddress { get; set; }
        public string PhoneNo { get; set; }
        public string RegNo { get; set; }
        public string Description { get; set; }
        public string VerificationCode { get; set; }
        public int SpecialtyId { get; set; }
             


    }
}
