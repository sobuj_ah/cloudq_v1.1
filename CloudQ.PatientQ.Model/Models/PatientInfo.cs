﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    [Table("PatientInfo")]
    public class PatientInfo : BaseModel
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PatientId { get; set; }
        [StringLength(20)]
        public string PatientNo { get; set; }
        [StringLength(50)]
        public string PatientName { get; set; }
        [StringLength(50)]
        public string PatientBarCode { get; set; }
        [StringLength(20)]
        public string Age { get; set; }
        [StringLength(10)]
        public string Sex { get; set; }
        [StringLength(50)]
        public string ContactNo { get; set; }
        public string EmailAddress { get; set; }

        public decimal Weight { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Description { get; set; }
        public string PhoneNo { get; set; }
        public string ProfileImage { get; set; }



    }
}
