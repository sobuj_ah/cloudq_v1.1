﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class Medicine : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MedicineId { get; set; }
        [StringLength(200)]
        public string MedicineName { get; set; }
        public string CommercialName { get; set; }
        public string Description { get; set; }

        public int? PharmaCompanyId { get; set; }
        public virtual PharmaCompany Company { get; set; }
        public int? GenericId { get; set; }
        public virtual DrugGeneric DrugGeneric { get; set; }
        public int? FormulationID { get; set; }
        public virtual DrugFormulation DrugFormulation { get; set; }
        public int? PackSizeId { get; set; }
        public virtual DrugPackSize DrugPackSize { get; set; }

        public virtual ICollection<DrugAvailableStrengths> DrugAvailableStrengthses { get; set; }


        public int? CategoryId { get; set; }
        public virtual DrugCategory DrugCategory { get; set; }
    }
}
