﻿using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class TestSample : BaseModel
    {
        public int TestSampleId { get; set; }
        public string SampleName { get; set; }
    }
}
