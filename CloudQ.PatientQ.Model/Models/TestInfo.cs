﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class TestInfo : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestId { get; set; }
        [StringLength(10)]
        public string TestCode { get; set; }
        public string Barcode { get; set; }
        [StringLength(200)]
        public string TestName { get; set; }
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public int TestCategoryId { get; set; }
        public virtual TestCategory TestCategory { get; set; }


    }
}
