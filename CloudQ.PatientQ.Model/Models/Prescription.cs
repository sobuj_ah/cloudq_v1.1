﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class Prescription : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrescriptionId { get; set; }

        public int? PatientId { get; set; }
        public virtual PatientInfo Patient { get; set; }

        public int? DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }

        public DateTime PrescribDate { get; set; }

        public int AppointmentId { get; set; }
        //public virtual Appointment Appointment { get; set; }

        public string Comments { get; set; }

        public int Status { get; set; }



        //public virtual ICollection<PrescribeMedicine> PrescribeMedicines { get; set; }
        //public virtual ICollection<PrescribeSymptom> PrescribeSymptoms { get; set; }
        //public virtual ICollection<PrescribeDiease> PrescribeDiease { get; set; }
        //public virtual ICollection<PrescribeTest> PrescribeTest { get; set; }





    }
}
