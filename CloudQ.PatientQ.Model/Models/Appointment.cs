﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class Appointment : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AppointmentId { get; set; }

        public int DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }
        public int PatientId { get; set; }
        public virtual PatientInfo Patient { get; set; }
        public DateTime SerialDate { get; set; }

        public string SerialNo { get; set; }
        //public int? DoctorSheduleId { get; set; }
        //public virtual DoctorShedule DoctorShedule { get; set; }
        public int Status { get; set; }
    }
}
