﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class DoctorType : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DoctorTypeID { get; set; }
        
        [StringLength(200)]
        public string DoctorTypeName { get; set; }
         
    }
}
