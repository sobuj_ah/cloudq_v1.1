﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class DiseaseWithSymptom : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DiseaseWithSymptomId { get; set; }
        public int SymptomId { get; set; }
        public virtual Symptom Symptom { get; set; }

        public int DiseaseId { get; set; }
        public virtual Disease Disease { get; set; }

    }
}
