﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class PrescribeTest : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrescribeTestId { get; set; }

        public int? PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }

        public int? TestId { get; set; }
        public virtual TestInfo TestInfo { get; set; }

    }
}
