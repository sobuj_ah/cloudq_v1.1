﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class DrugAvailableStrengths : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AvailableStrengthsId { get; set; }

        public int StrengthsId { get; set; }
        public virtual DrugStrengths DrugStrengths { get; set; }

        //public int MedicineId { get; set; }
        //public virtual Medicine Medicine { get; set; }

    }
}
