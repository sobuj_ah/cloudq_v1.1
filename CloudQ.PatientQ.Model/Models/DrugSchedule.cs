﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class DrugSchedule : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DrugScheduleId { get; set; }
        public string DrugScheduleTitle { get; set; }



        //public int MedicineId { get; set; }

        //public int? PatientAgeId { get; set; }
        //public DrugEligibleAge PatientAge { get; set; }
    }
}
