﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class PrescribeDrugSchedule : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrescribeDrugScheduleId { get; set; }
        public int DrugScheduleId { get; set; }
        public virtual DrugSchedule DrugSchedule { get; set; }

        public int PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
    }
}
