﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class DoctorTestCommission : BaseModel
    {


        public int TestId { get; set; }
        public virtual TestInfo TestInfo { get; set; }

        public int DoctorID { get; set; }
        public virtual Doctor Doctor { get; set; }

        public decimal Commission { get; set; }


    }
}
