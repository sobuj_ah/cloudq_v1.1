﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class TestCategory : BaseModel
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestCategoryId { get; set; }
        [StringLength(100)]
        public string CategoryName { get; set; }
        public string Description { get; set; }


        public int ParentCategoryId { get; set; }


    }
}
