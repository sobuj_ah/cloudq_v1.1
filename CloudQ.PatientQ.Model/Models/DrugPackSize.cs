﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class DrugPackSize : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PackSizeId { get; set; }
        public string PackSize { get; set; }

    }
}
