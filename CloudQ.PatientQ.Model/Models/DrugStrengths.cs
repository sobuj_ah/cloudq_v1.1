﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class DrugStrengths : BaseModel
    {
        //Table Weight
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StrengthsId { get; set; }
        public string Strengths { get; set; }


    }
}
