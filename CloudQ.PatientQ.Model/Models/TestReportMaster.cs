﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class TestReportMaster : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestReportMasterId { get; set; }
        public int CategoryId { get; set; }
        public int TestId { get; set; }
        public int PetientId { get; set; }
        public int DoctorId { get; set; }
        public virtual List<TestReportDetails> ProductSalesDetails { get; set; }
    }
}
