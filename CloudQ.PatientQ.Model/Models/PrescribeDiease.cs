﻿using CloudQ.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.PatientQ.Model.Models
{
    public class PrescribeDiease : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PrescribeDieaseId { get; set; }

        public int? PrescriptionId { get; set; }
        public virtual Prescription Prescription { get; set; }
        public int? DiseaseId { get; set; }
        public virtual Disease Disease { get; set; }

    }
}
