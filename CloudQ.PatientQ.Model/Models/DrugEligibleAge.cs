﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class DrugEligibleAge : BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PatientAgeId { get; set; }
        public string PatientAgeTitle { get; set; }
        public int Age { get; set; }


    }
}
