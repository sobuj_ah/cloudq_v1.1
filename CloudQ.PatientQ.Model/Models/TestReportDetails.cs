﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{
    public class TestReportDetails  : BaseModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestReportDetailsId { get; set; }
     
        public int TestParameterId { get; set; }
        public int TestReportMasterId { get; set; }
        //[ForeignKey("TestReportMasterId")]
        //public TestReportMaster TestReportMaster { get; set; }


    }
}
