﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudQ.PatientQ.Model.Models
{
    public class PatientTestDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DetailsId { get; set; }
        public int PatientTestId { get; set; }
     
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal DiscountRate { get; set; }
        public int TestId { get; set; }
        public TestInfo TestInfo { get; set; }
    }
}
