﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CloudQ.Infrastructure.Entity;

namespace CloudQ.PatientQ.Model.Models
{

    public class TestParameter : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ParameterId { get; set; }
      
        [StringLength(100)]     
        public string ParameterName { get; set; }
                         
        [StringLength(100)]
        public string ParameterUnit { get; set; }
        [StringLength(200)]
        public string ReferenceRange { get; set; }

        public int CategoryId { get; set; }
       // public ProductCategory ProductCategory { get; set; }



    }
}
