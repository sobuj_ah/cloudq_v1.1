﻿using System;
using System.Web.Mvc;
using AutoMapper;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Model.Models.Notification;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Core.Service.Interfaces.Notifications;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Controllers.Patients
{
    public class PatientInfoController : BaseController
    {
        private IPatientService _patientInfoService;
        private readonly IUserInfoService _userService;
        private ICommonService _commonSvc;
        private ISmsNotificationService _smsNotification;

        public PatientInfoController(IPatientService patientInfoService, IUserInfoService userService, ICommonService commonSvc, ISmsNotificationService smsNotification)
        {
            _patientInfoService = patientInfoService;
            _userService = userService;
            _commonSvc = commonSvc;
            _smsNotification = smsNotification;
        }



        public JsonResult SavePatientInfo(PatientInfoDto formData)
        {
            var objPatient = new PatientInfo();
            Mapper.Map(formData, objPatient);
            //var objUser = new CoreUsers();
            //objUser.LoginId = formData.ContactNo;
            //objUser.UserName = formData.PatientName;
            //objUser.Password = EncryptDecryptHelper.Encrypt(formData.Password);
            //objUser.Email = formData.EmailAddress;
            //objUser.Phone = formData.ContactNo;
            //objUser.UserTypeId = 7;
            //objUser.CreateBy = 1;
            //objUser.LastLoginTime = DateTime.Now;
            //objUser.LastUpdateTime = DateTime.Now;

          
            var res = _patientInfoService.SavePatient(objPatient);
            //if(res.Status)
            //{
            //    _userService.Save(objUser);
            //    var sms = new SMSSent();
            //    sms.MobileNumber = "+88" + formData.ContactNo;
            //    sms.SMSText = string.Format("Dear Sir, Your UserId is {0} , Password: ", objUser.LoginId, formData.Password);
            //    _smsNotification.Send(sms);
            //}


            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GridDataSource(GridOptions options)
        {
            var res = _patientInfoService.GridDataSource(options);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPatientByPhone(string phoneNo)
        {
            var res = _patientInfoService.GetPatientByPhone(phoneNo);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}
