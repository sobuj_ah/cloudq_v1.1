﻿using System;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Controllers.Patients
{
    public class AppointmentController : BaseController
    {
        private readonly ISerialBookingService _service;
        public AppointmentController(ISerialBookingService service)
        {
            _service = service;
        }
        

        public ActionResult BookSerial(AppointmentDto formData)
        {

            var result = _service.BookSerial(formData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GridDataSource(GridOptions options)
        {

            var result = _service.GridDataSource(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult GetDoctorCombo()
        //{

        //    var result = _service.GridDataSource(null);

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult GetSerial(DateTime date)
        {

            var result = _service.GetSerial(date);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
