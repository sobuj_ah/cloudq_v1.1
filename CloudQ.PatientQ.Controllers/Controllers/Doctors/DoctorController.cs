﻿using System;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Controllers.Doctors
{
    public class DoctorController : BaseController
    {
        private readonly IDoctorService _service;
        public DoctorController(IDoctorService service)
        {
            _service = service;
        }

        public ActionResult SaveDoctorInfo(Doctor formData)
        {

            formData.Id = formData.Id == Guid.Empty ? Guid.NewGuid() : formData.Id;
            var result = _service.SaveDoctorInfo(formData);

            return Json(result, JsonRequestBehavior.AllowGet);
          //  return Json("Failed", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Delete(int doctorId)
        {

            _service.Delete(doctorId);

            return Json("Delete", JsonRequestBehavior.AllowGet);

            

        }
        public ActionResult DoctorGridDataSource(GridOptions options)
        {

            var result = _service.GridDataSource(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDoctors()
        {

            var result = _service.GetDoctors();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
