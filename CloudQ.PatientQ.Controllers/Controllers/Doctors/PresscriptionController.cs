﻿using System;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.RTOs;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;
using CloudQ.UitilityTools.Model;
using System.Configuration;
using System.IO;

namespace CloudQ.PatientQ.Controllers.Doctors
{
    public class PresscriptionController : BaseController
    {
        private IPrescriptionService _service;
        private IPatientService _patientInfoService;
        private readonly ISerialBookingService _appointmentService;

        public PresscriptionController(IPrescriptionService service, IPatientService patientInfoService, ISerialBookingService appointmentService)
        {
            _service = service;
            _patientInfoService = patientInfoService;
            _appointmentService = appointmentService;


        }

        public JsonResult GetPatientWaiting(GridOptions options)
        {

            var res = _appointmentService.GetPatientWaiting(options);
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SavePrescription(PrescriptionDto prescription)
        {
            var res = _service.SavePrescription(prescription);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrescription(GridOptions options, int appointmentId)
        {
            var res = _service.GetPrescription(options, appointmentId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrescriptionByUser(GridOptions options)
        {

            var user = (CoreUsersDto)Session["CurrentUser"];

            var res = _service.GetPrescriptionByUser(options, user.UserId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintPrescription(int prescriptionId)
        {
            ReportsParam<PrescriptionRto> objReportParams = new ReportsParam<PrescriptionRto>();
            objReportParams.ReportTitle = "Prescription";
            string reportPath = ConfigurationManager.AppSettings["ReportPath"];
            objReportParams.RptFileName = Path.Combine(reportPath, "PrescriptionCr.rpt"); //@"E:\Mohidul\sobuj\Projects\CloudQ_v1.1_Project\CloudQ.PatientQ.Controllers\Reports\RPT\PrescriptionCr.rpt";
            objReportParams.DataSource = _service.GetPrescription(prescriptionId);
            objReportParams.IsPassParamToCr = true;
            Session["ReportType"] = "Prescription";
            Session["ReportParam"] = objReportParams;
            return Json("Success", JsonRequestBehavior.AllowGet);
        }


    }
}
