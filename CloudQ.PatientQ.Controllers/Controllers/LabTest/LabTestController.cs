﻿using System;
using System.Web.Mvc;
using AutoMapper;
using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Controllers.DiagonsisTest
{
    public class LabTestController : BaseController
    {
        //
        // GET: /ProductInfo/
        private readonly ITestService _Service;
        public LabTestController(ITestService service)
        {
            _Service = service;
        }
        public ActionResult SaveTestInfo(TestInfoDto formData)
        {
            var obj = new TestInfo();
            Result objResult = new Result();
            Mapper.Map(formData, obj);
            //obj.Id = Guid.NewGuid();
            obj.CreateDate = DateTime.Now;
            if (_Service.IsExist(obj.Id))
            {
                var ety = _Service.GetById(obj.Id);
                ety.Amount = formData.Amount;
                ety.TestName = formData.TestName;
                ety.Description = formData.Description;


                objResult = _Service.Update(ety);
            }
            else
            {
                objResult = _Service.Save(obj);
            }



            return Json(objResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(Guid Id)
        {

            var ety = _Service.GetById(Id);
            var objResult = _Service.Delete(ety);

            return Json(objResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GridDataSource(GridOptions options)
        {

            var result = _Service.GridDataSource(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTestComboData()
        {

            var result = _Service.GetTestComboData();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}