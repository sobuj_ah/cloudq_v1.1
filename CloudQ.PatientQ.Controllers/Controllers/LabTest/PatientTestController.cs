﻿using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools.Model;
using CloudQ.PatientQ.Model.RTOs;
using System.Configuration;
using System.IO;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.UitilityTools;
using CloudQ.PatientQ.Model.ViewModels;
using CloudQ.Core.Model.DTOs.Core;
using System;

namespace CloudQ.PatientQ.Controllers.DiagonsisTest
{
    public class PatientTestController : BaseController
    {
        private IPatientTestService _patientTestService;
        private IPatientService _patientInfoService;
        public PatientTestController(IPatientTestService patientTestService)
        {
            _patientTestService = patientTestService;
            // _patientInfoService = patientInfoService;
        }


        public JsonResult SavePatientTest(PatientTestDto formData)
        {
            CoreUsersDto loggedUser = null;
            string msg = "";
            try
            {
                if (Session["CurrentUser"] != null)
                {
                    loggedUser = ((CoreUsersDto)Session["CurrentUser"]);

                }
                if (loggedUser != null)
                {
                    formData.PatientTest.CreateBy = loggedUser.UserId;
                    formData.PatientTest.UpdateBy = loggedUser.UserId;
                    var res = _patientTestService.SavePatientTest(formData);
                    if (res.Status)
                    {
                        string fullPath = GetExportInvoicePath(formData.PatientTest.InvoiceNo);
                        return Json(fullPath, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (System.Exception ex)
            {

                msg = ex.Message;

            }


            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeletePatientTest(int patientTestId)
        {
            // _patientInfoService.Save(formData.Patient);


            var res = _patientTestService.DeletePatientTest(patientTestId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintInvoice(string invoiceNo)
        {
            
            string fullPath = GetExportInvoicePath(invoiceNo);

            return Json(fullPath, JsonRequestBehavior.AllowGet);
        }
       

        private string GetExportInvoicePath(string invoiceNo)
        {
            var dataSource = _patientTestService.GetInvoiceData(invoiceNo);
            string path = "../ExportFile/";
            string physicalPath = System.Web.HttpContext.Current.Server.MapPath(path);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }
            string fileName = AzExportRptToPdf.ExportToPdf(dataSource, "InvoicePrintHalf.rpt", physicalPath);
            string fullPath = path + fileName;
            return fullPath;
        }
        public JsonResult GetPatientTestByInvoiceNo(string invoiceNo)
        {
            var dataSource = _patientTestService.GetPatientTestByInvoiceNo(invoiceNo);
            return Json(dataSource, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GridDataSource(GridOptions options)
        {

            var dataSource = _patientTestService.GridDataSource(options);
            return Json(dataSource, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPatientTestById(int patientTestId)
        {

            var dataSource = _patientTestService.GetPatientTestById(patientTestId);
            return Json(dataSource, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDailyStatementByDate(DateTime fromDate, DateTime toDate)
        {

            var dataSource = _patientTestService.GetDailyStatement(fromDate, toDate);
            return Json(dataSource, JsonRequestBehavior.AllowGet);
        }



    }
}