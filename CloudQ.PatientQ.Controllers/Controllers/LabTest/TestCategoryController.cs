﻿using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudQ.PatientQ.Controllers.LabTest
{
    public class TestCategoryController : BaseController
    {
        ITestCategoryService _service;
        public TestCategoryController(ITestCategoryService service)
        {
            _service = service;
        }

        public JsonResult SaveTestCategory(TestCategory formData)
        {
            formData.Id = Guid.NewGuid();
            formData.CreateDate = DateTime.Now;
            formData.IsActive = true;

            var res = _service.SaveTestCategory(formData);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GridDataSource(GridOptions options)
        {

            var result = _service.GridDataSource(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
