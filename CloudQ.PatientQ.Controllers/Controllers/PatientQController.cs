﻿using System.Web.Mvc;
using CloudQ.Base.Controllers;
using CloudQ.Base.Controllers.Base;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Controllers
{
    public class PatientQController : BaseController
    {
        // GET: Customer


        public ActionResult Doctor()
        {
            return View("Doctor/DoctorIndex");
        }
        public ActionResult DoctorRegistration()
        {
            return View("Doctor/DoctorRegistration");
        }
        public ActionResult PatientRegistration()
        {
            return View("Patient/PatientRegistration");
        }
        public ActionResult HospitalRegistration()
        {
            return View("Hospital/HospitalRegistration");
        }
        public ActionResult Patient()
        {
            return View("Patient/PatientIndex");
        }

        public ActionResult PatientProfile()
        {
            return View("Patient/_patient_profile");
        }

        public ActionResult PatintPrescriptions()
        {
            return View("Patient/_MyPrescriptions");
        }

        public ActionResult PatientDoctors()
        {
            return View("Patient/_MyDoctorList");
        }
        public ActionResult TestInfo()
        {
            if (Session["CurrentUser"] != null)
            {
                return View("Test/TestIndex");
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        public ActionResult PatientTest()
        {
            if (Session["CurrentUser"] != null)
            {
                return View("PatientTest/PatientTestIndex");
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public ActionResult SerialBooking()
        {
            return View("SerialBooking/SerialBookingIndex");
        }
        public ActionResult MedicineInfo()
        {
            return View("Medicine/MedicineInfo/MedicineIndex");
        }
        public ActionResult PatientWaiting()
        {
            return View("Prescription/_PetientQueue");
        }
        public ActionResult Appointment()
        {
            return View("Appointment/_book_appointment");
        }
        public ActionResult DoctorSchedule()
        {
            return View("Appointment/_doctor_schedule");
        }
    }


}