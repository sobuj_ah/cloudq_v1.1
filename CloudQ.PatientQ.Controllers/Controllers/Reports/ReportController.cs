﻿using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Model.RTOs;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;
using CloudQ.UitilityTools.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudQ.PatientQ.Controllers.Controllers.Reports
{
    public class ReportController : BaseController
    {
        private IPatientTestService _patientTestService;
        //ReportsParam<Object> rptParam;
        public ReportController(IPatientTestService patientTestService)
        {
            _patientTestService = patientTestService;
        }

        public ActionResult PrintDailyStatement(DateTime fromDate, DateTime toDate)
        {
            var dataSource = _patientTestService.GetDailyStatement(fromDate, toDate);
            var rptParam = new ReportsParam<DailySalesStatementSummaryRto>();
            rptParam.RptFileName = "DailySalesStatementSummary.rpt";
            rptParam.DataSource = dataSource;
            rptParam.IsPassParamToCr = true;
            rptParam.FromDate = fromDate;
            rptParam.ToDate = toDate;


            string fullPath = GetExportReportPath(rptParam);

            return Json(fullPath, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PrintDailyStatementDetails(DateTime fromDate, DateTime toDate)
        {
            var dataSource = _patientTestService.GetDailyStatementDetails(fromDate, toDate);
            var rptParam = new ReportsParam<DailySalesStatementSummaryRto>();
			rptParam.RptFileName = "DailySalesStatementDetails.rpt";
            rptParam.DataSource = dataSource;
            rptParam.IsPassParamToCr = true;
            rptParam.FromDate = fromDate;
            rptParam.ToDate = toDate;


            string fullPath = GetExportReportPath(rptParam);

            return Json(fullPath, JsonRequestBehavior.AllowGet);
        }

        private string GetExportReportPath<T>(ReportsParam<T> reportParam)
        {

            string path = "../ExportFile/";
            string physicalPath = System.Web.HttpContext.Current.Server.MapPath(path);
            if (!Directory.Exists(physicalPath))
            {
                Directory.CreateDirectory(physicalPath);
            }
            else
            {
                try
                {
                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                }
                catch (Exception)
                {


                }



            }
            reportParam.ExportPath = physicalPath;
            string fileName = AzExportRptToPdf.ExportToPdf(reportParam);
            string fullPath = path + fileName;
            return fullPath;
        }
    }
}
