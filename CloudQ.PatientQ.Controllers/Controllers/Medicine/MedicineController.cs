﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;
using CloudQ.UitilityTools.Common.Json;

namespace CloudQ.PatientQ.Controllers.Medicine
{
    public class MedicineController : BaseController
    {
        private IMedicineService _service;

        public MedicineController(IMedicineService service)
        {
            _service = service;
        }
        public JsonResult GetFormulation()
        {
            var res = _service.GetFormulation();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGeneric()
        {
            var res = _service.GetGeneric();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCategory()
        {
            var res = _service.GetCategory();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMedicine(GridOptions options, int formulationId = 0, int genericeId = 0, int categoryId = 0, string symptomIds = "", string searchKey = "")
        {
            List<int> symtoms = new List<int>();
            if (symptomIds != "")
            {
                symtoms = JsonHelper.JsonDeserialize<List<int>>(symptomIds);
            }
            var res = _service.GetMedicine(options, formulationId, genericeId, categoryId, symtoms, searchKey);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDrugStrengths()
        {
            var res = _service.GetDrugStrengths();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSymptoms()
        {
            var res = _service.GetSymptoms();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDrugDosage()
        {
            var res = _service.GetDosages();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
         
    }
}
