﻿using System.Collections.Generic;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface IBranchRepository
    {
        string SaveBranch(OrgBankBranchDto branch);

        GridEntity<OrgBankBranchDto> GetAllBranchSummary(GridOptions options);

        List<OrgBankBranchDto> GetBranchByCompanyId(int companyId, CurrentUserDto objUser);
        List<OrgBankBranchDto> GetAllBranchNameComboData();

        GridEntity<OrgBankBranchDto> GetActiveBranchGridData(GridOptions options,
            List<CompanyDto> gbCompanyList, int isSelectAllCompany, CurrentUserDto objUsers);
        GridEntity<OrgBankBranchDto> GetBranchForSubgridData(GridOptions options);
        GridEntity<OrgBankBranchDto> GetActiveBranchGridDataByCompanyId(GridOptions options, int companyId, CurrentUserDto objUser);
        GridEntity<OrgBankBranchDto> GetActiveBranchSummaryForSbuConfigaration(GridOptions options);
        OrgBankBranchDto GetBranchByBranchId(int branchId);
    }
}