﻿using CloudQ.Core.Model.Models.Org;
using CloudQ.Infrastructure.Services;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface IBankBranchService : IService<OrgBankBranch>
    {
         
    }
}
