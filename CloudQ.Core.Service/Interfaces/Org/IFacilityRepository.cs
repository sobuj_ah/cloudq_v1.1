﻿using System.Collections.Generic;
using System.Collections.Specialized;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface IFacilityRepository
    {
        string SaveFacilityDetails(OrgFacilityDto objFacility);

        GridEntity<OrgFacilityDto> GetFacilitySummary(GridOptions options);
        GridEntity<OrgFacilityDto> GetFacilitySummaryForSbuConfigaration(GridOptions options);
        GridEntity<OrgSectionDto> GetFacilitySectionSummaryForSbuConfiguration(GridOptions options, CurrentUserDto user);
        List<OrgSectionDto> GetFacilitySectionByFacilityId(int facilityId);
        string SaveFacilitySectionMap(List<OrgSectionDto> facilitySectionInfoList, int facilityId);
        List<OrgFacilityDto> GetAllActiveFacility();
        List<OrgFacilityDto> GetDepartmentFacilityByDepartmentId(int departmentId);

        GridEntity<OrgFacilityDto> GetActiveFacilityGridData(GridOptions options, List<OrgDepartmentDto> objDepartmentList,
            int isSelectAllDepartment, CurrentUserDto user);
    }
}