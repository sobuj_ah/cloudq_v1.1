﻿using System.Collections.Generic;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Org;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface IDesignationRepository
    {
        string SaveDesignation(OrgDesignation designation);

        object GetDesignationSummaryAll(GridOptions options);

        List<OrgDesignation> GetDesignationByCompanyId(int companyId);

        List<OrgDesignation> GetAllDesignationByCompanyIdAndStatus(int companyId, int status);

        List<OrgDesignation> GenerateDesignationByDepartmentIdCombo(int departmentId, int status);
        List<OrgDesignation> GenerateDesignationByDepartmentIdAndCompanyIdCombo(int departmentId, int status,int companyId);

        GridEntity<OrgDesignation> GetActiveDesignationGridData(GridOptions options,
            List<CompanyDto> objCompanyList, int isSelectAllCompany);

        List<OrgDesignation> GenerateDesignationAllCombo();
        List<OrgDesignation> GenerateDesignationByCompanyIdCombo(int companyId, int status);

        GridEntity<OrgDesignation> GetActiveDesignationSummaryForSbuConfigaration(GridOptions options);

        string SaveDesignationLinked(OrgDesignation degination);
        GridEntity<OrgDesignation> GetDesignationForSubgridData(GridOptions options);
    }
}