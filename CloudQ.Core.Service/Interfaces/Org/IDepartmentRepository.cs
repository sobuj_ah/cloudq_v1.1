﻿using System.Collections.Generic;
using System.Linq;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.UitilityTools;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Org;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface IDepartmentRepository
    {
        string SaveDepartment(OrgDepartmentDto department);

        object GetDepartmentSummaryAll(GridOptions options);

        IQueryable<OrgDepartmentDto> GetDepartmentByCompanyId(int companyId, CurrentUserDto objUser);

        GridEntity<OrgDepartmentDto> GetActiveDepartmentGridData(GridOptions options,
            List<CompanyDto> objCompanyList, int isSelectAllCompany, CurrentUserDto objUser);

        IQueryable<OrgDepartmentDto> GetDepartmentAll();

        List<OrgDepartmentDto> GetDepartmentByCompanies(List<int> companyList);

        object GetDepartmentByCompanies(GridOptions options, List<CompanyDto> companyList,
            int isSelectAllCompany, CurrentUserDto user);

        GridEntity<OrgDepartmentDto> GetActiveDepartmentGridDataByCompanyId(GridOptions options, int companyId,
            CurrentUserDto user);

        //List<AccessRestrictionEntity> GetAccessRestructionOfDepartment(int hrRecordId);

        GridEntity<OrgDepartmentDto> GetDepartmentSummaryForSbuConfigaration(GridOptions options);
      //  string SaveDepartmentFacilityMap(List<Facility> deptFacilityInfoList, int departmentId);

        List<OrgDepartmentDto> GetDeptComboData();

       
    }
}