﻿using System.Linq;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Core.Service.Services.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public class AuthorizationService : BaseService<CoreUsers>, IAuthorizationService
    {
        //CoreDbContext _CoreDbContext;
        IUserInfoService _userInfoService;
        public AuthorizationService(ICoreUnitOfWork unitOfWork, IUserInfoService userInfoService)
            : base(unitOfWork)
        {
            _userInfoService = userInfoService;

        }

        public Result AuthorizeLogin(CoreUsersDto coreUser)
        {

            //var repo = base._unitOfWork.Repository<CoreUsers>();
            //var users = repo.GetAll();
            var pwd = EncryptDecryptHelper.Encrypt(coreUser.Password);
            var oUser = _userInfoService.GetUserByLoginId(coreUser.LoginId);

            if (oUser != null)
            {
                var user = base.Get(s => s.LoginId == coreUser.LoginId && s.Password == pwd).SingleOrDefault();

                if (user != null && oUser.UserType == coreUser.UserType)
                {
                    return new Result()
                    {
                        Operation = Operation.Success,
                        Status = true,
                        Data = oUser,
                        // Message="Login",

                    };
                }
                else
                {
                    return new Result()
                    {
                        Operation = Operation.Failed,
                        Status = false,
                        Message = "The user not authorized in this panel",

                    };
                }



            }

            return new Result()
            {
                Operation = Operation.Failed,
                Status = false,
                Message = "User ID or Password is invalid"

            };
        }
    }
}
