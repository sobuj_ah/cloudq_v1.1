﻿using System.Collections.Generic;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Org;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface IDivisionRepository
    {
        GridEntity<OrgDivisionDto> GetDivisionSummaryForSbuConfigaration(GridOptions options);
        string SaveDivision(OrgDivisionDto objDivisionInfo);
        GridEntity<OrgDivisionDto> GetDivisionSummary(GridOptions options);
        List<OrgDivisionDto> GetDivisionByCompanyId(int companyId, CurrentUserDto user);

        //GridEntity<Department> GetDivisionDepartmentSummaryForSbuConfigarationByCompanyId(GridOptions options,
        //    int companyId, Users user);

        string SaveDivisionDepartmentMap(CompanyDto objCompanyInfo, List<OrgDepartmentDto> divisionDeptInfoList, int divisionId);
        List<OrgDepartmentDto> GetDivisionDeptByCompanyIdAndDivisionId(int companyId, int divisionId, CurrentUserDto user);

        GridEntity<OrgDivisionDto> GetActiveDivisionGridData(GridOptions options, List<CompanyDto> objCompanyList,
            int isSelectAllCompany, CurrentUserDto user);

        List<OrgDivisionDto> GetDivisionComboData();
    }
}