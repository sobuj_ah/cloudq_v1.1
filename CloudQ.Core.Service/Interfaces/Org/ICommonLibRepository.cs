﻿using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface ICommonService
    {
        object GetCommonComboData(string peram);

        Result CreateNextCode(string pre, string field, string table);
    }
}
