﻿using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Org;
using CloudQ.Infrastructure.Services;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface ICompanyService : IService<OrgCompany>
    {
         
        GridEntity<CompanyDto> GridDataSource(GridOptions options);
    }
}
