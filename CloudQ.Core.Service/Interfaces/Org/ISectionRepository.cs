﻿using System.Collections.Generic;
using System.Collections.Specialized;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Org
{
    public interface ISectionRepository
    {
        string SaveSectionDetails(OrgSectionDto objSection);
        GridEntity<OrgSectionDto> GetSectionSummary(GridOptions options);
        GridEntity<OrgSectionDto> GetActiveSectionGridData(GridOptions options, List<OrgFacilityDto> objFacilityList,
            int isSelectAllFacility, CurrentUserDto user);
        List<OrgSectionDto> GetDepartmentSectionByDepartmentId(int departmentId);
        GridEntity<OrgSectionDto> GetDeptSectionSummaryForSbuConfiguration(GridOptions options);
        string SaveDepartmentSectionMap(List<OrgSectionDto> deptSectionInfoList, int departmentId);

        GridEntity<OrgSectionDto> GetSectionByDepartmentAndFacility(GridOptions options, List<OrgDepartmentDto> objDepartmentList, int isSelectAllDepartment, List<OrgFacilityDto> objFacilityList, int isSelectAllFacility, CurrentUserDto user);
    }
}