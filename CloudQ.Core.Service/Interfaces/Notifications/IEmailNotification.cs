﻿using CloudQ.Core.Model.Models.Notification;
using CloudQ.UitilityTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Service.Interfaces.Notifications
{
    public interface IEmailNotification
    {
        Result SentEmailNotification(SentEmail sentEmail);
    }
}
