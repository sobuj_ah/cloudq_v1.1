﻿using CloudQ.Core.Model.Models.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Service.Interfaces.Notifications
{
    public interface ISmsNotificationService
    {
        string Send(SMSSent sms);
    }
}
