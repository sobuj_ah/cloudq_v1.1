﻿using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Core
{
    public interface IAuthorizationService : IService<CoreUsers>
    {
        Result AuthorizeLogin(CoreUsersDto coreUser);
    }
}