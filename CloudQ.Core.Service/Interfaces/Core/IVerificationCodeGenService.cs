﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Service.Interfaces.Core
{
    public interface IVerificationCodeGenService
    {
        string GenerateCode(string mobileNo);
    }
}
