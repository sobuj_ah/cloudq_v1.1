﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Core
{
    public interface IRoleService : IService<CoreRole>
    {
        string CreateRole(CoreRoleDto objGroup);

        GridEntity<CoreRoleDto> GetRoleGridData(GridOptions options);

        List<CoreRolePermissionDto> GetRolePermisionById(int roleId);

        //List<Group> GetRoleByCompanyId(int companyId);
                                                      
       // List<AccessControl> GetAllAccess();

        List<CoreRolePermissionDto> GetAccessPermisionForCurrentUser(int moduleId, int userId);

       // Group GetGroupByCondition(string condition);

        CoreMenusDto CheckMenuPermission(string rawUrl, CurrentUserDto objUser);
     //   List<Group> GetAllGroupName();

        //List<CoreRolePermissionDto> GetAccessPermisionForCanteen(int moduleId, int p);
    }


}
