﻿using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Core
{
    public interface IUserInfoService : IService<CoreUsers>
    {
        GridEntity<CoreUsersDto> UsersGridDataSourceAsync(GridOptions options);

        CoreUsersDto GetUserByLoginId(string loginId);
        CoreUsersDto GetUserByEmailAddress(string emailaddress);
    }
}
