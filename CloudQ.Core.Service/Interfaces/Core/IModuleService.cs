﻿using System.Collections.Generic;
using System.Linq;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Core
{
    public interface IModuleService : IService<CoreModules>
    {
        GridEntity<CoreModulesDto> GetModuleSummary(GridOptions options);
        List<CoreModulesDto> GetModule();
        string SaveModule(CoreModulesDto module);
    }
}