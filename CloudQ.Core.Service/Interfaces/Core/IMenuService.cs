﻿using System.Collections.Generic;
using System.Linq;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Interfaces.Core
{
    public interface IMenuService : IService<CoreMenus>
    {
        GridEntity<CoreMenusDto> GetMenuSummary(GridOptions options);
        List<CoreMenusDto> SelectAllMenu();
        string SaveMenu(CoreMenusDto menu);
        List<CoreMenusDto> SelectAllMenuByModuleId(int moduleId);
        List<CoreMenusDto> GetMenuByUserPermission(int userId);
        string UpdateMenuSorting(List<CoreMenusDto> menuList);
        //List<CoreMenusDto> GetParentMenuByMenu(int parentMenuId);
        CoreMenusDto GetMenuByMenuId(int menuId);
        List<CoreMenusDto> GetHierarckyMenuByMenuId(int menuId);
        List<CoreMenusDto> GetMenu();
        List<CoreMenusDto> GetMenuByParentId(int menuId);
    }

    
}
