﻿using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Org;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;

//using CloudQ.ProductQ.Model.ViewModels;

namespace CloudQ.Core.Service.Services.Org
{
    public class CompanyService : BaseService<OrgCompany>, ICompanyService
    {
        // private CoreUnitOfWork _coreUoW = new CoreUnitOfWork();

        public CompanyService(ICoreUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        GridEntity<CompanyDto> ICompanyService.GridDataSource(GridOptions options)
        {
            string query = "Select * from CQM.OrgCompany";
            return Data<CompanyDto>.Grid.DataSource(options,query,"CompanyName");
        }
    }
}
