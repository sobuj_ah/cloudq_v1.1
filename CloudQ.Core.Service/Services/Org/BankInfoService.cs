﻿using CloudQ.Core.Model.Models.Org;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;

namespace CloudQ.Core.Service.Services.Org
{
    public class BankInfoService : BaseService<OrgBank>, IBankInfoService
    {
        public BankInfoService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }



    }
}
