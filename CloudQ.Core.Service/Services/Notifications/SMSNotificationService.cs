﻿using CloudQ.Core.Model.Models.Notification;
using CloudQ.Core.Service.Interfaces.Notifications;
using CloudQ.UitilityTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Service.Services.Notifications
{
    public class SMSNotificationService : ISmsNotificationService
    {
        public string Send(SMSSent sms)
        {
            sms.RequestDateTime = DateTime.Now;
            sms.SimNumber = "0";
            sms.Status = 0;
            sms.NoOfTry = 0;
            string sql = string.Format(@"INSERT INTO [CQM].[SMSSent]
                           ([SMSText]
                           ,[MobileNumber]
                           ,[RequestDateTime]          
                           ,[Status]
                           ,[ReplyFor]
                           ,[SimNumber] )        
                     VALUES('{0}','{1}','{2}','{3}','{4}','{5}')", sms.SMSText, sms.MobileNumber, sms.RequestDateTime, 0,
               0, sms.SimNumber);

            var commonConnection = new CommonConnection();
            commonConnection.ExecuteNonQuery(sql);
            return Operation.Success.ToString();
        }
    }
}
