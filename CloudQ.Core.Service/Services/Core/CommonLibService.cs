﻿using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.Infrastructure.Base;
using CloudQ.UitilityTools;
using CloudQ.UitilityTools.Utilities;

namespace CloudQ.Core.Service.Services.Core
{
    public class CommonService : ICommonService
    {
        public object GetCommonComboData(string peram)
        {
            CommonLibDataService dataService = new CommonLibDataService();
            return dataService.GetCommonComboData(peram);
        }

        public Result CreateNextCode(string pre, string field, string table)
        {
            Result oResult = new Result();
            ManipulateData manipulate = new ManipulateData();
            var code = manipulate.CreateNextCode(pre, 10, field, table);
            oResult.Data = code;
            oResult.Operation = Operation.Success;
            oResult.Status = true;
            return oResult;
        }
    }
}
