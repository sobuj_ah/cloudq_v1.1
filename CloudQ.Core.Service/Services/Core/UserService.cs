﻿using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;
using System.Linq;

namespace CloudQ.Core.Service.Services.Core
{
    public class UserInfoService : BaseService<CoreUsers>, IUserInfoService
    {
        public UserInfoService(ICoreUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public CoreUsersDto GetUserByEmailAddress(string emailaddress)
        {
            string query = string.Format(@"Select a.*,b.UserType from CQM.CoreUsers a
left join CQM.CoreUserTypes b on b.UserTypeId = a.UserTypeId where (a.Email='{0}' or LoginId='{0}')", emailaddress);
            return Data<CoreUsersDto>.SingleData(query);

        }

        public CoreUsersDto GetUserByLoginId(string loginId)
        {

            string query = string.Format(@"Select [UserId] ,[LoginId] ,[UserName] ,[LastUpdateTime] ,[LastLoginTime] ,[FailedLoginCount] ,[IsExpired] ,a.[Id] ,a.[IsActive] ,a.[CreateDate] ,a.[UpdateDate] ,a.[CreateBy] ,a.[UpdateBy] ,[Email] ,[Phone] ,a.[UserTypeId]
,b.UserType from CQM.CoreUsers a
left join CQM.CoreUserTypes b on b.UserTypeId = a.UserTypeId where a.LoginId='{0}'", loginId);
            return Data<CoreUsersDto>.SingleData(query);


            //var user = base.Get(s => s.LoginId == loginId);
            //return user.FirstOrDefault();

        }

        public GridEntity<CoreUsersDto> UsersGridDataSourceAsync(GridOptions options)
        {
            string query = @"Select a.*,b.UserType from CQM.CoreUsers a
left join CQM.CoreUserTypes b on b.UserTypeId = a.UserTypeId";
            return Data<CoreUsersDto>.Grid.DataSource(options, query, "UserName");
        }
    }
}
