﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Services.Core
{
    public class ModuleService : BaseService<CoreModules>, IModuleService
    {
        public ModuleService(ICoreUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public GridEntity<CoreModulesDto> GetModuleSummary(GridOptions options)
        {
            return Data<CoreModulesDto>.Grid.DataSource(options,"Select * from CQM.CoreModules", "ModuleName");

        }

        public List<CoreModulesDto> GetModule()
        {
            return Data<CoreModulesDto>.DataSource("Select * from CQM.CoreModules");
        }

      

        public string SaveModule(CoreModulesDto module)
        {
            CoreModules obj = new CoreModules();
            Mapper.Map(module, obj);
            base.Save(obj);
            return "Success";

        }
    }
}
