﻿using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;

namespace CloudQ.Core.Service.Services.Core
{
    public class WorkflowService : BaseService<CoreWorkflowState>, IWorkflowService
    {
        public WorkflowService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }

    
}
