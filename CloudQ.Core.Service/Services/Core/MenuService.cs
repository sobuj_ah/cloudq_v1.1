﻿using System.Collections.Generic;
using System.Linq;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Services.Core
{
    public class MenuService : BaseService<CoreMenus>, IMenuService
    {
        public MenuService(ICoreUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public GridEntity<CoreMenusDto> GetMenuSummary(GridOptions options)
        {
            return Data<CoreMenusDto>.Grid.DataSource(options, @"Select MenuId,Menu.ModuleId, MenuName, menuPath, ParentId,ModuleName,Sequence,
(Select MenuName  from CQM.CoreMenus mn where mn.MenuId = menu.ParentId) as ParentMenuName 
from CQM.CoreMenus as Menu left outer join CQM.CoreModules module on module.ModuleId = menu.ModuleId", "Sequence");
        }

        List<CoreMenusDto> IMenuService.SelectAllMenu()
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<CoreMenusDto> SelectAllMenu()
        {
            throw new System.NotImplementedException();
        }

        public string SaveMenu(CoreMenusDto menu)
        {
            throw new System.NotImplementedException();
        }

        List<CoreMenusDto> IMenuService.SelectAllMenuByModuleId(int moduleId)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<CoreMenusDto> SelectAllMenuByModuleId(int moduleId)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<CoreMenusDto> SelectMenuByUserPermission(int userId)
        {
            throw new System.NotImplementedException();
        }

        public string UpdateMenuSorting(List<CoreMenusDto> menuList)
        {
            throw new System.NotImplementedException();
        }

        public List<CoreMenusDto> GetParentMenuByMenu(int parentMenuId)
        {
            throw new System.NotImplementedException();
        }

        public CoreMenusDto GetMenuByMenuId(int menuId)
        {
            throw new System.NotImplementedException();
        }

        public List<CoreMenusDto> GetHierarckyMenuByMenuId(int menuId)
        {
            throw new System.NotImplementedException();
        }

        public List<CoreMenusDto> GetMenu()
        {
            string query = string.Format(@"Select * from cqm.CoreMenus");

            return Data<CoreMenusDto>.DataSource(query);
        }

        public List<CoreMenusDto> GetMenuByParentId(int menuId)
        {
            string query = string.Format(@"Select * from cqm.CoreMenus where MenuID = {0}", menuId);

            return Data<CoreMenusDto>.DataSource(query);
        }

        public List<CoreMenusDto> GetMenuByUserPermission(int userId)
        {
            string query = string.Format(@"SELECT DISTINCT Menu.MenuId,Menu.ModuleId, member.UserId, perm.PermissionTableName, Menu.MenuName, Menu.MenuPath, Menu.ParentId,Sequence 
 FROM cqm.CoreUserRole member
 INNER JOIN cqm.CoreRole role ON role.RoleId = member.RoleId
  INNER JOIN cqm.CoreRolePermission perm ON role.RoleId = perm.CoreRoles_RoleId
   INNER JOIN cqm.CoreMenus menu ON perm.ReferenceID = Menu.MenuId 
   WHERE (member.UserId ={0}) AND (perm.PermissionTableName = 'Menu') 
   order by Sequence", userId);
            return Data<CoreMenusDto>.DataSource(query);
        }
    }


}
