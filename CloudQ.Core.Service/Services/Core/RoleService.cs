﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.DTOs.Org;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Service.Services.Core
{
    public class RoleService : BaseService<CoreRole>, IRoleService
    {
        ICoreUnitOfWork coreUnitOfWork;
        public RoleService(ICoreUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

            coreUnitOfWork = unitOfWork;
        }

        public List<CoreRoleDto> GetRoles(GridOptions options)
        {
            return Data<CoreRoleDto>.DataSource("Select * from CQM.CoreRole", "RoleName");

        }

        public List<CoreRolePermissionDto> GetRolePermisionById(int roleId)
        {
            return Data<CoreRolePermissionDto>.DataSource("Select * from CQM.CoreRolePermission where RoleId = {0}", roleId);
        }

        public GridEntity<CoreRole> GetRoleGridData(GridOptions options)
        {
            string query = "Select * from CQM.CoreRole";
            return Data<CoreRole>.Grid.DataSource(options, query, "RoleName");
        }

        public string CreateRole(CoreRoleDto objGroup)
        {
            throw new System.NotImplementedException();

        }

        GridEntity<CoreRoleDto> IRoleService.GetRoleGridData(GridOptions options)
        {
            throw new System.NotImplementedException();
        }

        public List<CoreRolePermissionDto> GetAccessPermisionForCurrentUser(int moduleId, int userId)
        {
            throw new System.NotImplementedException();
        }

        public CoreMenusDto CheckMenuPermission(string rawUrl, CurrentUserDto objUser)
        {
            throw new System.NotImplementedException();
        }
    }
}
