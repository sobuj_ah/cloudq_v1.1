﻿using CloudQ.Core.Service.Interfaces.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudQ.Core.Service.Services.Core
{
    public class VerificationCodeGenService : IVerificationCodeGenService
    {
        public string GenerateCode(string mobileNo)
        {
            Random generator = new Random();
            String r = generator.Next(0, 999999).ToString("D6");
            return r;
        }
    }
}
