﻿using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.Core.Service.Services.Core;

namespace CloudQ.Core.Controllers.Core
{
    public class CommonServiceController : BaseController
    {
        //
        // GET: /CommonLib/
        private ICommonService _repository;

        public CommonServiceController()
        {
            _repository = new CommonService();
        }
        public ActionResult GetCommonComboData(string peram)
        {
            var data = _repository.GetCommonComboData(peram);
            
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateNextCode(string pre,string field,string table)
        {
            
            var data = _repository.CreateNextCode(pre, field, table);

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
