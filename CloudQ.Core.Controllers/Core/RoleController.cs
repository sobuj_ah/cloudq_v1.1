﻿using System.Web.Mvc;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Controllers.Core
{
    public class RoleController : Controller
    {
        //
        // GET: /Role/
        private readonly IRoleService _roleService;
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        public ActionResult Index()
        {
            return View("RoleIndex"); //View("RoleIndex");
        }
        public ActionResult SaveRole(CoreRole formData)
        {

            var result = _roleService.Save(formData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteRole(CoreRole formData)
        {

           _roleService.Delete(formData);

            return Json("", JsonRequestBehavior.AllowGet);
        }
       


        public ActionResult RoleGridDataSource(GridOptions options)
        {

            var result = _roleService.GetRoleGridData(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRolePermisionById(int roleId)
        {
            var groupPermisionList = _roleService.GetRolePermisionById(roleId);
            return Json(groupPermisionList, JsonRequestBehavior.AllowGet);
        }
    }
}