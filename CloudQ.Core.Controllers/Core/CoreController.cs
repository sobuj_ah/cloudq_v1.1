﻿using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;

namespace CloudQ.Core.Controllers.Core
{
    public class CoreController : BaseController
    {

      
        public ActionResult Users()
        {
            return View("UserInfo/UserInfoIndex");
        }
        public ActionResult Menu()
        {
            return View("Menu/MenuIndex");
        }
        public ActionResult Role()
        {
            return View("Role/RoleIndex");
        }
        public ActionResult UserType()
        {
            return View("UserType/UserTypeIndex");
        }
        public ActionResult ChangePassword()
        {
            return View("UserInfo/ChangePasswod");
        }
        public ActionResult UserProfile()
        {
            return View("UserInfo/Profile");
        }
    }
}