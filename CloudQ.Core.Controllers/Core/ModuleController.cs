﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Service.Interfaces.Core;

namespace CloudQ.Core.Controllers.Core
{
    public class ModuleController:BaseController
    {
        private IModuleService _service;
        public ModuleController(IModuleService service)
        {
            _service = service;
        }


        public JsonResult GetModule()
        {

            var result = _service.GetModule();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
