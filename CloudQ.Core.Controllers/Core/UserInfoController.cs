﻿using System;
using System.Web.Mvc;
using AutoMapper;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Infrastructure.UOW;
using CloudQ.UitilityTools;
using CloudQ.Base.Controllers;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Service.Interfaces.Org;

namespace CloudQ.Core.Controllers.Core
{

    //[System.Web.Http.RoutePrefix("UserInfo")]
    public class UserInfoController : Controller
    {
        //
        // GET: /UserInfo/

        private readonly IUserInfoService _userService;
        private readonly IAuthorizationService _authorizationService;
        public UserInfoController(IUserInfoService userService, IAuthorizationService authorizationService)

        {
            _userService = userService;
            _authorizationService = authorizationService;
        }

        //
        // GET: /Company/

        [HttpPost]
        // [System.Web.Http.Route("SaveUserInfo")]
        public JsonResult SaveUserInfo(CoreUsersDto formData)
        {
            var oResult = new Result();
            try
            {


                CoreUsers user = new CoreUsers();

                Mapper.Map(formData, user);
                user.CreateBy = 1;
                user.LastLoginTime = DateTime.Now;
                user.LastUpdateTime = DateTime.Now;
                user.Password = EncryptDecryptHelper.Encrypt(user.Password);
                if (!ModelState.IsValid)
                {
                    //return BadRequest(ModelState);
                }
                oResult.Data = _userService.Save(user);
                oResult.Status = true;
                oResult.Operation = Operation.Success;
            }
            catch (Exception ex)
            {
                oResult.Status = false;
                oResult.Operation = Operation.Failed;
                oResult.Message = ex.Message;
            }

            return Json(oResult, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UsersGridDataSource(GridOptions options)
        {
            var result = _userService.UsersGridDataSourceAsync(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // [System.Web.Http.Route("SaveUserInfo")]
        public JsonResult ChangePassword(ChangePasswordDto formData)
        {
            var oResult = new Result();
            try
            {
                if (!ModelState.IsValid)
                {
                    //return BadRequest(ModelState);
                }
                var currentUser = (CoreUsersDto)Session["CurrentUser"];


                currentUser.LoginId = currentUser.LoginId;
                currentUser.Password = formData.OldPassword;
                var result = _authorizationService.AuthorizeLogin(currentUser);

                if (result.Status && formData.NewPassword == formData.ConfirmPassword)
                {
                    var password = EncryptDecryptHelper.Encrypt(formData.ConfirmPassword);
                    var user = _userService.GetUserByLoginId(currentUser.LoginId);
                    CoreUsers newUser = new CoreUsers();

                    Mapper.Map(user, newUser);
                    newUser.Password = password;
                    oResult.Data = _userService.Update(newUser);
                    oResult.Status = true;
                    oResult.Operation = Operation.Success;

                }

                if (result.Status)
                {
                    oResult.Status = false;
                    oResult.Operation = Operation.Failed;
                    oResult.Message ="Password Reset";
                }
                else
                {
                    oResult.Status = false;
                    oResult.Operation = Operation.Failed;
                    oResult.Message = "Reset Failed";

                }

            }
            catch (Exception ex)
            {
                oResult.Status = false;
                oResult.Operation = Operation.Failed;
                oResult.Message = ex.Message;
            }

            return Json(oResult, JsonRequestBehavior.AllowGet);
        }

    }
}