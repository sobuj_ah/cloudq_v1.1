﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Model.DTOs.Core;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Core.Service.Services.Core;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Controllers.Core
{
    public class AuthorizationController : BaseController
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IUserInfoService _userService;

        // GET: Authorization
        public AuthorizationController(IAuthorizationService authorizationService, IUserInfoService userService)
        {
            _authorizationService = authorizationService;
            _userService = userService;

        }

        public ActionResult AuthorizeLogin(string loginId, string password, bool IsKeepLogged, string loginType)
        {
            CoreUsersDto coreUser = new CoreUsersDto();
            coreUser.LoginId = loginId;
            coreUser.Password = password;
            coreUser.UserType = loginType;
            var result = _authorizationService.AuthorizeLogin(coreUser);


            if (result.Status)
            {
                Session["CurrentUser"] = result.Data;
                ViewBag.UserName = ((CoreUsersDto)result.Data).UserName;


                // return RedirectToAction("Index", "Home");
            }
            //else
            //{
            //    return RedirectToAction("Login", "Home");

            //}
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ForgetPasswordEmail(string emailaddress)
        {
            var result = "Success";
            try
            {

                var MailServer = "smtp.gmail.com";
                var smtpPort = "587";
                var Sender = "tanjilcse32@gmail.com";
                var emailpassword = "cse09032";

                var EmailTo = "";


                var objUser = _userService.GetUserByEmailAddress(emailaddress);


                if (objUser != null)
                {
                    const string Pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
                    if (!string.IsNullOrEmpty(objUser.Email) && Regex.IsMatch(objUser.Email, Pattern))
                    {

                        EmailTo = objUser.Email;

                        var newPass = EncryptDecryptHelper.Decrypt(objUser.Password);

                        var Mailbody = "Dear " + objUser.UserName + ", <br> <br> Login ID : <b>" +
                                       objUser.LoginId + "</b> <br> Password : <b>" + newPass +
                                       "</b>.<br><br>Thank You";
                        var SmtpServer = new SmtpClient();
                        // MailMessage mail = new MailMessage();

                        //SmtpServer = emailpassword != string.Empty
                        //    ? new SmtpClient(MailServer, int.Parse(smtpPort))
                        //    {
                        //        Credentials = new NetworkCredential(Sender, emailpassword)
                        //    }
                        //    : new SmtpClient(MailServer);

                        MailMessage mail = new MailMessage();
                        mail.Body = Mailbody;
                        mail.From = new MailAddress(Sender,"Forgot Password"); 
                        mail.Sender = new MailAddress(Sender);
                        mail.Subject = "Forgot password for the system";
                        mail.To.Add(new MailAddress(EmailTo));

                        // SmtpServer.Credentials = new NetworkCredential(AppConfig.From, AppConfig.Password);
                        SmtpServer.Port = int.Parse(smtpPort);
                        SmtpServer.Host = MailServer;
                        SmtpServer.Timeout = 1000 * 30;

                        SmtpServer.Credentials = new NetworkCredential(Sender, emailpassword);
                        mail.IsBodyHtml = true;
                        SmtpServer.EnableSsl = true;

                        ServicePointManager.ServerCertificateValidationCallback =
                            delegate (object s, X509Certificate certificate,
                                X509Chain chain, SslPolicyErrors sslPolicyErrors)

                            { return true; };

                        SmtpServer.Send(mail);
                        result = "Success";
                    }
                    else
                    {
                        result = "Invalid or nullable Email Address";
                    }
                }

                else
                {
                    result = "Invalid User Information";
                }


            }
            catch (Exception ex)
            {
                result = ex.Message; // "Failed";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}