﻿using System.Web.Mvc;
using AutoMapper;
using CloudQ.Core.Model.Models.Org;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Controllers.Core
{

   
    public class CompanyController : Controller
    {

        private readonly ICompanyService _companyService;
        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }
        public ActionResult SaveCompany(OrgCompany formData)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            OrgCompany company = new OrgCompany();

            Mapper.Map(formData, company);

            // company.ApplicationUserId = User.Identity.GetUserId();
            if (formData.CompanyId > 0)
            {
                _companyService.Update(formData);
            }
            else
            {
                _companyService.Save(formData);

            }

            //  var result = _companyService.Add(formData);

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteCompany(OrgCompany formData)
        {

              _companyService.Delete(formData);

            return Json("", JsonRequestBehavior.AllowGet);
        }


        public ActionResult CompanyGridDataSource(GridOptions options)
        {

            var result = _companyService.GridDataSource(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}