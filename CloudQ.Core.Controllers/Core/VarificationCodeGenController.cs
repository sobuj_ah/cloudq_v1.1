﻿using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Model.Models.Notification;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.Core.Service.Interfaces.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudQ.Core.Controllers
{
    public class VarificationCodeGenController : BaseController
    {
        IVerificationCodeGenService _service;
        ISmsNotificationService _smsService;
        public VarificationCodeGenController(IVerificationCodeGenService service, ISmsNotificationService smsService)
        {
            _service = service;
            _smsService = smsService;
        }

        public ActionResult GenerateCode(string mobileNo)
        {

            var res = _service.GenerateCode(mobileNo);
            if (!string.IsNullOrEmpty(res))
            {
                var sms = new SMSSent();
                sms.MobileNumber ="+8801"+ mobileNo;
                sms.SMSText = string.Format("Dear Sir, Your verification code is " + res);
                _smsService.Send(sms);
            }
            return Json(res, JsonRequestBehavior.AllowGet);
        }



    }
}
