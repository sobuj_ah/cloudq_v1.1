﻿using System.Web.Mvc;
using System.Web.UI.WebControls;
using CloudQ.Base.Controllers.Base;
using CloudQ.Core.Model.Models.Core;
using CloudQ.Core.Service.Interfaces.Core;
using CloudQ.UitilityTools;

namespace CloudQ.Core.Controllers.Core
{
    public class MenuController : BaseController
    {
        //
        // GET: /Menu/
        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        public ActionResult Index()
        {
            return View("MenuIndex");
        }
        public ActionResult SaveMenu(CoreMenus formData)
        {

            var result = _menuService.Save(formData);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteMenu(CoreMenus formData)
        {

            _menuService.Delete(formData);

            return Json("", JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult MenuGridDataSource(GridOptions options)
        {

            var result = _menuService.GetMenuSummary(options);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMenuByUserPermission()
        {

            var result = _menuService.GetMenuByUserPermission(1);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMenuByParentId(int menuId)
        {

            var result = _menuService.GetMenuByParentId(menuId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }


	}
}