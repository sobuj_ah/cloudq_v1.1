﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;

namespace CloudQ.Core.Controllers.Notification
{
    public class NotificationController : BaseController
    {
        public ActionResult MailInbox()
        {
            return View("Mail/_mail_inbox");
        }
        public ActionResult MailCompose()
        {
            return View("Mail/_mail_compose");
        }
        public ActionResult MailSingle()
        {
            return View("Mail/_mail_single");
        }
    }
}
