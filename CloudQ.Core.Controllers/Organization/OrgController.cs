﻿using System.Web.Mvc;
using CloudQ.Base.Controllers.Base;

namespace CloudQ.Core.Controllers.Organization
{
    public class OrgController : BaseController
    {
        public ActionResult Company()
        {
            return View("Company/CompanyIndex");
        }

    }
}
