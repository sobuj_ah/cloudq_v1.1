﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{
    public interface ISerialBookingService : IService<Appointment>
    {
        string GetSerial(DateTime date);

        GridEntity<AppointmentDto> GridDataSource(UitilityTools.GridOptions options);
        object BookSerial(AppointmentDto formData);

        GridEntity<PatientQueueDto> GetPatientWaiting(UitilityTools.GridOptions options);

     
    }
}
