﻿using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{
    public interface ITestParameterService
    {
        Result SaveParameter(TestParameter formData);

        Result DeleteParameter(TestParameter formData);

        Result UpdateParameter(TestParameter formData);

        GridEntity<TestParameter> ParameterGridDataSource(GridOptions options);

        
    }
}
