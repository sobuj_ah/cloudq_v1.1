﻿using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{


    public interface ITestService : IService<TestInfo>
    {
        GridEntity<TestInfoDto> GridDataSource(GridOptions options);
        List<TestInfoDto> GetReportData();
        List<TestInfoDto> GetTestComboData();
       

    }
}
