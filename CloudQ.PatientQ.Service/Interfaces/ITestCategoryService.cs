﻿using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{


    public interface ITestCategoryService : IService<TestCategory>
    {
        GridEntity<TestCategoryDto> GridDataSource(GridOptions options);
        object SaveTestCategory(TestCategory formData);
    }
}
