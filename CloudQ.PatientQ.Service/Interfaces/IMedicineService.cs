﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Model.ViewModels;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{
    public interface IMedicineService : IService<Medicine>
    {
        List<DrugFormulation> GetFormulation();
        List<DrugGeneric> GetGeneric();

        GridEntity<MedicineVm> GetMedicine(GridOptions option, int formulationId, int genericeId, int categoryId, List<int> symptomIds, string searchKey);

        List<DrugCategoryDto> GetCategory();
        List<SymptomDto> GetSymptoms();
        List<DrugDosage> GetDosages();


        List<DrugStrengths> GetDrugStrengths();
    }

}
