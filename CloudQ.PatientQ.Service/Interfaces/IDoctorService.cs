﻿using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{
    public interface IDoctorService : IService<Doctor>
    {

        GridEntity<DoctorDto> GridDataSource(GridOptions options);
        List<Doctor> GetDoctorReportData();
        List<DoctorDto> GetDoctors();

        void Delete(int doctorId);

        Result SaveDoctorInfo(Doctor formData);
    }
}
