﻿using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;
using CloudQ.PatientQ.Model.ViewModels;
using CloudQ.PatientQ.Model.RTOs;

namespace CloudQ.PatientQ.Service.Interfaces
{


    public interface IPatientTestService : IService<PatientTest>
    {
        GridEntity<PatientTestVm> GridDataSource(GridOptions options);

        GridEntity<PatientTestDetails> GetProductTestDetails(int salesId, GridOptions options);
        Result SavePatientTest(PatientTestDto formData);

        List<InvoiceReportRto> GetInvoiceData(string invoiceNo);

        Result DeletePatientTest(int patientTestId);

        PatientTestDto GetPatientTestById(int patientTestId);

        List<DailySalesStatementSummaryRto> GetDailyStatement(System.DateTime fromDate, System.DateTime toDate);
        List<DailySalesStatementSummaryRto> GetDailyStatementDetails(System.DateTime fromDate, System.DateTime toDate);


        PatientTestDto GetPatientTestByInvoiceNo(string invoiceNo);
    }

}