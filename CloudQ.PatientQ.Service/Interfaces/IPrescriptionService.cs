﻿using System;
using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Model.RTOs;
using CloudQ.PatientQ.Model.ViewModels;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{


    public interface IPrescriptionService : IService<Prescription>
    {
        List<PrescriptionDto> GetReportData();
        object SavePrescription(PrescriptionDto prescription);
        GridEntity<PrescriptionVm> GetPrescription(GridOptions options, int appointmentId);
        GridEntity<PrescriptionVm> GetPrescriptionByUser(GridOptions options, int appointmentId);

        List<PrescriptionRto> GetPrescription(int prescriptionId);
    }
}
