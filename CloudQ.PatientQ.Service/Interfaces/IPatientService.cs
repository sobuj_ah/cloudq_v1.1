﻿using System;
using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Interfaces
{


    public interface IPatientService : IService<PatientInfo>
    {
        GridEntity<PatientInfoDto> GridDataSource(GridOptions options);


        PatientInfoDto GetPatientById(System.Guid guid);





        Result SavePatient(PatientInfo objPatient);

        PatientInfoDto GetPatientByPhone(string phoneNo);
    }
}
