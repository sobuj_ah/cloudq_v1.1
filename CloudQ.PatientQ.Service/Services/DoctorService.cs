﻿using System.Collections.Generic;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class DoctorService : BaseService<Doctor>, IDoctorService
    {
        public DoctorService(IPatientQUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public GridEntity<DoctorDto> GridDataSource(GridOptions options)
        {
            string query = "Select * from PQM.Doctors";
            return Data<DoctorDto>.Grid.DataSource(options, query, "DoctorName");
        }
        public List<DoctorDto> GetDoctors()
        {
            string query = "Select * from PQM.Doctors";
            return Data<DoctorDto>.DataSource(query);
        }

        public List<Doctor> GetDoctorReportData()
        {
            throw new System.NotImplementedException();
        }


        public void Delete(int doctorId)
        {
            var sql = "Delete PQM.Doctors where DoctorId=" + doctorId;
            var conn = new CommonConnection();
            conn.ExecuteNonQuery(sql);
        }


        public Result SaveDoctorInfo(Doctor formData)
        {
            if (formData.DoctorID == 0)
            {
                var sql = string.Format(@"INSERT INTO [PQM].[Doctors]([DoctorName],[Degree],[Address],[ContactNo],[IsActive])
     VALUES('{0}','{1}','{2}','{3}','{4}')", formData.DoctorName, formData.Degree, formData.Address, formData.ContactNo, 1);
                var conn = new CommonConnection();
                conn.ExecuteNonQuery(sql);
                return new Result() { Message = "Success" };
            }
            else {
                var sql = string.Format(@"Update [PQM].[Doctors] set [DoctorName]='{0}',[Degree]='{1}',[Address]='{2}',[ContactNo]='{3}' where doctorId='{4}'", formData.DoctorName, formData.Degree, formData.Address, formData.ContactNo, formData.DoctorID);
                var conn = new CommonConnection();
                conn.ExecuteNonQuery(sql);
                return new Result() { Message = "Success" };
            }
          
        }
    }
}
