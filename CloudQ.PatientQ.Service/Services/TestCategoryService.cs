﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class TestCategoryService : BaseService<TestCategory>, ITestCategoryService
    {
        public TestCategoryService(IPatientQUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public GridEntity<TestCategoryDto> GridDataSource(GridOptions options)
        {
            string query = "select * from pqm.TestCategory";
            return Data<TestCategoryDto>.Grid.DataSource(options, query, "CategoryName");
        }

        public object SaveTestCategory(TestCategory formData)
        {
            return base.Save(formData);
        }
    }
}
