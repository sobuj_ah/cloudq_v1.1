﻿using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class TestParameterService : BaseService<TestParameter>, ITestParameterService
    {
        public TestParameterService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Result SaveParameter(TestParameter formData)
        {
            throw new System.NotImplementedException();
        }

        public Result DeleteParameter(TestParameter formData)
        {
            throw new System.NotImplementedException();
        }

        public Result UpdateParameter(TestParameter formData)
        {
            throw new System.NotImplementedException();
        }

        public GridEntity<TestParameter> ParameterGridDataSource(GridOptions options)
        {
            throw new System.NotImplementedException();
        }
    }
}
