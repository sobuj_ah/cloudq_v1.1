﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Model.ViewModels;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class MedicineService : BaseService<Medicine>, IMedicineService
    {
        public MedicineService(IPatientQUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }


        public List<DrugFormulation> GetFormulation()
        {
            string query = "Select * from [PQM].[DrugFormulations]";
            return Data<DrugFormulation>.DataSource(query);
        }

        public List<DrugGeneric> GetGeneric()
        {
            string query = "Select * from [PQM].[DrugGenerics]";
            return Data<DrugGeneric>.DataSource(query);
        }

        public GridEntity<MedicineVm> GetMedicine(GridOptions option, int formulationId, int genericeId, int categoryId, List<int> symptomIds, string searchKey)
        {
            string condition = "";
            if (formulationId > 0)
            {
                condition += " m.FormulationID=" + formulationId;
            }
            if (genericeId > 0)
            {
                if (condition != "")
                {
                    condition += " and ";
                }
                condition += " m.GenericId=" + genericeId;

            }
            if (categoryId > 0)
            {
                if (condition != "")
                {
                    condition += " and ";
                }
                condition += " m.CategoryId=" + categoryId;
            }
            string symps = "";
            if (symptomIds != null)
            {
                symps = symptomIds.Aggregate(symps, (current, symptomId) => current + string.Format("{0},", symptomId));
            }

            if (symps != "")
            {
                symps = symps.Remove(symps.Length - 1, 1);

                if (condition != "")
                {
                    condition += string.Format(" and ");

                }
                condition += string.Format(" s.symptomId in ({0})", symps);

            }
            if (!string.IsNullOrEmpty(searchKey))
            {
                if (condition != "")
                    condition += " and ";
                condition +=
                    string.Format(" (MedicineName like '%{0}%') ", searchKey);
            }


            if (condition != "")
                condition = " where " + condition;


            string query = string.Format(@" Select distinct MedicineName,Formulation,CategoryName,GenericName,p.PharmaCompanyName,m.FormulationID,m.MedicineId,m.GenericId,m.PharmaCompanyId from [PQM].[Medicines] m
left join [PQM].DrugFormulations f on f.FormulationID=m.FormulationID
left join PQM.DrugCategories c on c.CategoryId=m.CategoryId
left join pqm.DrugGenerics g on g.GenericId=g.GenericId
left join pqm.PharmaCompanies  p on p.PharmaCompanyId=m.PharmaCompanyId 
left join PQM.DrugWithSymptoms ds on ds.MedicineId= m.MedicineId
left join PQM.Symptoms s on s.SymptomId=ds.SymptomId
                        {0}", condition);


            return Data<MedicineVm>.Grid.DataSource(option, query, "MedicineName");

        }

        public List<DrugCategoryDto> GetCategory()
        {
            string query = "  Select * from [PQM].[DrugCategories]";
            return Data<DrugCategoryDto>.DataSource(query);
        }

        public List<DrugDosage> GetDosages()
        {
            string query = @"  Select * from pqm.DrugDosages ";
            return Data<DrugDosage>.DataSource(query);
        }

        public List<DrugStrengths> GetDrugStrengths()
        {
            string query = @"Select * from PQM.DrugStrengths where IsActive=1";
            return Data<DrugStrengths>.DataSource(query);
        }

        public List<SymptomDto> GetSymptoms()
        {
            string query = @"Select * from CloudQ.PQM.symptoms order by SymptomTitle";
            return Data<SymptomDto>.DataSource(query);
        }
    }
}
