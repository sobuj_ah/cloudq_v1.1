﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;
using CloudQ.PatientQ.Model.ViewModels;
using System.Data;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.PatientQ.Model.RTOs;

namespace CloudQ.PatientQ.Service.Services
{
    public class PatientTestService : BaseService<PatientTest>, IPatientTestService
    {
        private IPatientQUnitOfWork _unitOfWork;
        ICommonService _commonSvc;
        public PatientTestService(IPatientQUnitOfWork unitOfWork, ICommonService commonSvc)
            : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _commonSvc = commonSvc;
        }

        public GridEntity<PatientTestVm> GridDataSource(GridOptions options)
        {
            string query = @"Select pt.PatientTestId, pt.InvoiceNo,pt.SalesDate,pt.PaidAmount,pt.DueAmount, p.PatientName,p.PatientNo,p.Sex,p.Age,p.ContactNo,
d.DoctorName as RefdBy,DeliveryDate  from  [PQM].[PatientTest] pt 
left join [PQM].PatientInfo p on p.PatientId=pt.PatientId
left join [PQM].Doctors d on d.DoctorID=pt.ReferredBy";
            return Data<PatientTestVm>.Grid.DataSource(options, query, "SalesDate desc");
        }

        public GridEntity<PatientTestDetails> GetProductTestDetails(int patientTestId, GridOptions options)
        {
            string query = "select * from pqm.PatientTestDetails where patientTestId=" + patientTestId;
            return Data<PatientTestDetails>.Grid.DataSource(options, query, "PatientTestId");
        }

        public Result SavePatientTest(PatientTestDto formData)
        {
            // return base.Save(formData);
            var oResult = new Result();
            //PatientTest obj = new PatientTest();
            //var patientRepo=_unitOfWork.Repository<PatientInfo>();
            //patientRepo.Add(formData.Patient);
            //_unitOfWork.Commit();

            //obj = formData.PatientTest;
            //obj.Id = Guid.NewGuid();
            //obj.PrescriptionId = 0;
            //obj.PatientId = formData.Patient.PatientId;
            //obj.PatientTestDetails = formData.PatientTestDetails;
            //oResult = base.Save(obj);
            var comm = new CommonConnection(IsolationLevel.ReadCommitted);
            try
            {
                comm.BeginTransaction();

                int pId = SavePatinet(formData.Patient, comm);
                formData.PatientTest.PatientId = pId;

                int pTestId = SavePatinetTest(formData.PatientTest, comm);
                if (pTestId > 0)
                {

                    SavePatinetTestDetails(formData.PatientTestDetails, comm, pTestId);
                }
                comm.CommitTransaction();
                oResult.Status = true;
                oResult.Message = "Saved Successfully";

            }
            catch (Exception)
            {

                comm.RollBack();
            }
            finally
            {
                comm.Close();
            }



            return oResult;
        }

        private void SavePatinetTestDetails(ICollection<PatientTestDetailsDto> collection, CommonConnection conn, int id)
        {
            string query = "";
            conn.ExecuteNonQuery("Delete [PQM].[PatientTestDetails] where PatientTestId=" + id);
            foreach (var item in collection)
            {


                query += string.Format(@"INSERT INTO [PQM].[PatientTestDetails]
           ([Quantity],[DiscountAmount],[DiscountRate],[TestId],[PatientTestId],[Price])VALUES
                       ('{0}','{1}','{2}','{3}','{4}','{5}');", item.Quantity, item.DiscountAmount, item.DiscountRate, item.TestId, id, item.Price);



            }
            if (query != "")
            {
                conn.ExecuteNonQuery(query);
            }


        }

        private int SavePatinetTest(PatientTest pt, CommonConnection conn)
        {
            if (pt.PatientTestId == 0)
            {
                pt.Id = Guid.NewGuid();
                string query = string.Format(@" 
            INSERT INTO [PQM].[PatientTest] ([InvoiceNo],[SalesDate],[DiscountAmount],[DiscountRate],[CommissionAmount],[CommissionRate],[TaxAmount]
                       ,[ReferredBy],[PatientId],[Id],[IsActive],[CreateDate],[CreateBy],[PaidAmount],[DueAmount],[Remarks],[DeliveryDate],[PaymentType]
                       ,[NetAmount],TotalQuantity,TotalAmount)
                 VALUES
                       ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}')", pt.InvoiceNo,
      pt.SalesDate, pt.DiscountAmount, pt.DiscountRate, pt.CommissionAmount, pt.CommissionRate, pt.TaxAmount, pt.ReferredBy, pt.PatientId, pt.Id, pt.IsActive, pt.CreateDate, pt.CreateBy, pt.PaidAmount,
      pt.DueAmount, pt.Remarks, pt.DeliveryDate, pt.PaymentType, pt.NetAmount, pt.TotalQuantity, pt.TotalAmount);
                return conn.ExecuteAfterReturnId(query, "");
            }
            else
            {
                string query = string.Format(@" Update [PQM].[PatientTest] set [PaidAmount]='{0}',[DueAmount]='{1}',UpdateBy='{2}',UpdateDate='{3}' ,[DiscountAmount]='{5}'
                where PatientTestId={4}", pt.PaidAmount, pt.DueAmount, pt.UpdateBy, DateTime.Now, pt.PatientTestId, pt.DiscountAmount);
                conn.ExecuteNonQuery(query);
                return pt.PatientTestId;
            }
        }

        private int SavePatinet(PatientInfo pt, CommonConnection conn)
        {
            if (pt.PatientId == 0)
            {
                pt.PatientNo = _commonSvc.CreateNextCode("P-", "PatientNo", "pqm.PatientInfo").Data.ToString();
                string query = string.Format(@"INSERT INTO [PQM].[PatientInfo] ([PatientNo] ,[PatientName],[Age] ,[Sex] ,[ContactNo] ,[Id] ,[IsActive] ,[CreateDate] ,[CreateBy])
                 VALUES
                       ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", pt.PatientNo, pt.PatientName, pt.Age, pt.Sex, pt.ContactNo, pt.Id, pt.IsActive, pt.CreateDate, pt.CreateBy);
                return conn.ExecuteAfterReturnId(query, "");
            }
            return pt.PatientId;

        }


        public List<Model.RTOs.InvoiceReportRto> GetInvoiceData(string invoiceNo)
        {
            string query = string.Format(@"Select pt.InvoiceNo,pt.SalesDate as BillDate,pd.Price as SalesPrice,pt.PaidAmount,pt.DueAmount, pt.DiscountAmount as DiscountedPrice,p.PatientName as CustomerName,p.DateOfBirth,p.Sex,p.Age,p.ContactNo,
                    d.DoctorName+'-'+d.Degree as RefdBy,u.UserName as SalesByName,ti.TestName as ProductName, Ti.TestCode as ProductCode,DeliveryDate,Remarks  from [PQM].[PatientTestDetails] pd
                    inner join [PQM].[PatientTest] pt on pt.PatientTestId=pd.PatientTestId
                    left join [PQM].PatientInfo p on p.PatientId=pt.PatientId
                    left join [PQM].[TestInfoes] TI on ti.TestId=pd.TestId
                    left join [PQM].Doctors d on d.DoctorID=pt.ReferredBy
                    left join [CQM].[CoreUsers] u on u.UserId=pt.CreateBy
                    where pt.InvoiceNo='{0}' order by pd.DetailsId ", invoiceNo);
            return Data<Model.RTOs.InvoiceReportRto>.DataSource(query);
        }


        public Result DeletePatientTest(int patientTestId)
        {
            var comm = new CommonConnection();
            string query = string.Format(@"delete [PQM].[PatientTest] where PatientTestId={0}
                            delete [PQM].[PatientTestDetails] where PatientTestId={0}", patientTestId);
            comm.ExecuteNonQuery(query);
            return new Result();

        }


        public PatientTestDto GetPatientTestById(int patientTestId)
        {
            var objTest = new PatientTestDto();
            objTest.PatientTest = Data<PatientTest>.SingleData("Select * from [PQM].[PatientTest] where PatientTestId=" + patientTestId);
            objTest.PatientTestDetails = Data<PatientTestDetailsDto>.GenericDataSource(@"Select pd.*,ti.TestCode,ti.TestName from [PQM].[PatientTestDetails] pd
left join PQM.TestInfoes ti on pd.TestId=ti.TestId
 where PatientTestId=" + patientTestId);
            objTest.Patient = Data<PatientInfo>.SingleData("Select * from pqm.PatientInfo where PatientId=" + objTest.PatientTest.PatientId);
            return objTest;
        }


        public List<Model.RTOs.DailySalesStatementSummaryRto> GetDailyStatement(DateTime fromDate, DateTime toDate)
        {
            string query = string.Format(@"Select cast(SalesDate as date)SalesDate,sum(pt.TotalAmount)Price,sum(pt.TotalQuantity)Quantity,sum(pt.DiscountAmount)DiscountAmount,sum(pt.DueAmount)DueAmount,sum(pt.NetAmount)NetAmount,sum(pt.PaidAmount)PaidAmount from PQM.PatientTest pt
                                    where cast(SalesDate as date) between '{0}' and '{1}'
                                    group by cast(SalesDate as date)
                                    order by SalesDate", fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));
            return Data<DailySalesStatementSummaryRto>.DataSource(query);
        }
        public List<Model.RTOs.DailySalesStatementSummaryRto> GetDailyStatementDetails(DateTime fromDate, DateTime toDate)
        {
            string query = string.Format(@"Select pt.InvoiceNo,cast(SalesDate as date)SalesDate,sum(pt.TotalAmount)Price,sum(pt.TotalQuantity)Quantity,sum(pt.DiscountAmount)DiscountAmount,sum(pt.DueAmount)DueAmount,sum(pt.NetAmount)NetAmount,sum(pt.PaidAmount)PaidAmount from PQM.PatientTest pt
                                    where cast(SalesDate as date) between '{0}' and '{1}'
                                    group by pt.InvoiceNo,cast(SalesDate as date)
                                    order by SalesDate", fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));
            return Data<DailySalesStatementSummaryRto>.DataSource(query);
        }


        public PatientTestDto GetPatientTestByInvoiceNo(string invoiceNo)
        {
            var objTest = new PatientTestDto();
            objTest.PatientTest = Data<PatientTest>.SingleData(string.Format("Select top 1 *  from [PQM].[PatientTest] where  InvoiceNo   like '%{0}'", invoiceNo));
            objTest.PatientTestDetails = Data<PatientTestDetailsDto>.GenericDataSource(@"Select pd.*,ti.TestCode,ti.TestName,pd.Price as amount  from [PQM].[PatientTestDetails] pd
left join PQM.TestInfoes ti on pd.TestId=ti.TestId
 where PatientTestId=" + objTest.PatientTest.PatientTestId);
            objTest.Patient = Data<PatientInfo>.SingleData("Select * from pqm.PatientInfo where PatientId=" + objTest.PatientTest.PatientId);
            return objTest;
        }
    }
}
