﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class TestService : BaseService<TestInfo>, ITestService
    {
        public TestService(IPatientQUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public GridEntity<TestInfoDto> GridDataSource(GridOptions options)
        {
            string query = @"select [TestId],[Barcode],[TestName],t.[TestCategoryId],t.[Id]
      ,t.[IsActive]      ,t.[CreateDate]      ,t.[UpdateDate]
      ,t.[CreateBy]      ,t.[UpdateBy]      ,t.[TestCode]
      ,t.[Description]      ,t.[Amount],c.CategoryName from pqm.TestInfoes t
left join pqm.TestCategories c on c.TestCategoryId=t.TestCategoryId";
            return Data<TestInfoDto>.Grid.DataSource(options, query, "TestName");
        }

        public List<TestInfoDto> GetReportData()
        {
            string query = "select * from pqm.TestInfoes";
            return Data<TestInfoDto>.DataSource(query);
        }

        public List<TestInfoDto> GetTestComboData()
        {
            string query = @"select TestId,TestCode,TestName,Amount from pqm.TestInfoes
order by TestName";
            return Data<TestInfoDto>.DataSource(query);
        }
    }
}
