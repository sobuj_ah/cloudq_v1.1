﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CloudQ.Core.DataService.DB;
using CloudQ.Core.Model.Models.Notification;
using CloudQ.Core.Service.Interfaces.Org;
using CloudQ.Core.Service.Services.Core;
using CloudQ.Infrastructure.Services;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class SerialBookingService : BaseService<Appointment>, ISerialBookingService
    {

        private ICommonService _commonSvc;
        private IPatientQUnitOfWork _unitOfWork;
        private IPatientService _patientService;

        public SerialBookingService(IPatientQUnitOfWork unitOfWork, ICommonService commonSvc, IPatientService patientService)
            : base(unitOfWork)
        {
            _commonSvc = commonSvc;
            _unitOfWork = unitOfWork;
            _patientService = patientService;

        }

        public string GetSerial(DateTime date)
        {
            string newSerial = "";
            var sql = string.Format("Select Max(SerialNo)SerialNo from  pqm.Appointments where cast(SerialDate as date)='{0}'", date.ToString("yyyy-MM-dd"));
            var objSerial = Data<AppointmentDto>.SingleData(sql);
            if (string.IsNullOrEmpty(objSerial.SerialNo))
            {
                newSerial = "001";
            }
            else
            {
                var b = Convert.ToInt32(objSerial.SerialNo) + 1;
                if (b <= 9)
                {
                    newSerial = "00" + b.ToString();
                }
                else if (b <= 99)
                {
                    newSerial = "0" + b.ToString();
                }
                else if (b <= 999)
                {
                    newSerial = b.ToString();
                }
            }

            return newSerial;
        }

        public GridEntity<AppointmentDto> GridDataSource(GridOptions options)
        {


            string query = @"Select  s.[DoctorId] ,s.[SerialDate],s.[SerialNo],p.[ContactNo],p.[PatientName],d.[DoctorName]
                         from  pqm.Appointments s
                         left join pqm.Doctors d on d.DoctorID=s.DoctorId
                         left join pqm.PatientInfo as p on p.PatientId=s.PatientId";
            return Data<AppointmentDto>.Grid.DataSource(options, query, "SerialNo");
        }

        public object BookSerial(AppointmentDto formData)
        {
            try
            {
                // Mapper.CreateMap(formData, oAppointment);

                Appointment oAppointment = new Appointment();
                Mapper.Map<AppointmentDto, Appointment>(formData, oAppointment);
                PatientInfo oPatient = new PatientInfo();
                oPatient.Id = Guid.NewGuid();
                oPatient.PatientName = formData.PatientName;
                oPatient.PatientNo = _commonSvc.CreateNextCode("P-", "PatientNo", "pqm.PatientInfo").Data.ToString();
                oPatient.ContactNo = formData.ContactNo;
                oPatient.DateOfBirth = DateTime.Now;
                oPatient.EmailAddress = "";
                oPatient.Description = "";
                oPatient.PhoneNo = "";




                //formData.Patient = new PatientInfo
                //{
                //    PatientNo = _commonSvc.CreateNextCode("P-", "PatientNo", "PatientInfo").Data.ToString(),
                //    PatientName = formData.PatientName,
                //    ContactNo = formData.ContactNo
                //};
                //ICoreUnitOfWork core = new CoreUnitOfWork();
                //var repo = core.Repository<SMSSent>();
                //var pRepo = _unitOfWork.Repository<PatientInfo>();
                //pRepo.Add(oPatient);
                //var objPatient = _unitOfWork.Repository<PatientInfo>().Get(s => s.PatientNo == oPatient.PatientNo);
                //_unitOfWork.Commit();
                //  var pm = _patientService.GetById(oPatient.Id);

                _patientService.Save(oPatient);
                var objPtn = _patientService.GetPatientById(oPatient.Id);

                oAppointment.PatientId = objPtn.PatientId;


                base.Save(oAppointment);
                SendAppointmentNotification(formData);
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            //repo.Add(sms);

        }

        public GridEntity<PatientQueueDto> GetPatientWaiting(GridOptions options)
        {
            string query = @"Select s.AppointmentId, p.PatientId, s.[DoctorId] ,s.[SerialDate],s.[SerialNo],p.[ContactNo],p.[PatientName],d.[DoctorName],Status
                         from  pqm.Appointments s
                         left join pqm.Doctors d on d.DoctorID=s.DoctorId
                         left join pqm.PatientInfo as p on p.PatientId=s.PatientId";
            return Data<PatientQueueDto>.Grid.DataSource(options, query, "SerialDate");
        }

     

        private static void SendAppointmentNotification(AppointmentDto formData)
        {
            var doctor = Data<Doctor>.SingleData("Select * from pqm.Doctors where DoctorId=" + formData.DoctorId);

            var sms = new SMSSent();
            sms.SMSText =
                string.Format("Welcome {0}, your serial no is #{1}. Please wait to next call of appointed doctor Mr/Ms {2}",
                    formData.PatientName, formData.SerialNo, doctor.DoctorName);
            sms.MobileNumber = formData.ContactNo;
            sms.RequestDateTime = DateTime.Now;
            sms.SimNumber = "0";
            sms.Status = 0;
            sms.NoOfTry = 0;

            string sql = string.Format(@"INSERT INTO [CQM].[SMSSent]
                           ([SMSText]
                           ,[MobileNumber]
                           ,[RequestDateTime]          
                           ,[Status]
                           ,[ReplyFor]
                           ,[SimNumber] )        
                     VALUES('{0}','{1}','{2}','{3}','{4}','{5}')", sms.SMSText, sms.MobileNumber, sms.RequestDateTime, 0,
                0, sms.SimNumber);

            var commonConnection = new CommonConnection();
            commonConnection.ExecuteNonQuery(sql);
        }

      
    }
}
