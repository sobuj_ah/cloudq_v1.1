﻿using System;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;
using CloudQ.Core.Service.Interfaces.Org;

namespace CloudQ.PatientQ.Service.Services
{
    public class PatientService : BaseService<PatientInfo>, IPatientService
    {
        ICommonService _commonSvc;
        public PatientService(IPatientQUnitOfWork unitOfWork,ICommonService commonSvc)
            : base(unitOfWork)
        {
            _commonSvc=  commonSvc;
        }

        public GridEntity<PatientInfoDto> GridDataSource(GridOptions options)
        {
            string query = "Select * from PQM.PatientInfo";
            return Data<PatientInfoDto>.Grid.DataSource(options, query, "PatientName");
        }

        public PatientInfoDto GetPatientById(Guid guid)
        {
            string query = string.Format("Select * from PQM.PatientInfo where Id='{0}'", guid);
            return Data<PatientInfoDto>.SingleData(query);
        }

        public PatientInfoDto GetPatientByPhone(string phoneNo)
        {
            string query = string.Format("Select * from PQM.PatientInfo where ContactNo='{0}'", phoneNo);
            return Data<PatientInfoDto>.SingleData(query);
        }


        public Result SavePatient(PatientInfo objPatient)
        {
            objPatient.PatientNo = _commonSvc.CreateNextCode("P-", "PatientNo", "pqm.PatientInfo").Data.ToString();
            return base.Save(objPatient);
        }
    }
}
