﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CloudQ.Infrastructure.Services;
using CloudQ.Infrastructure.UOW;
using CloudQ.PatientQ.DataService.DB;
using CloudQ.PatientQ.Model.DTOs;
using CloudQ.PatientQ.Model.Models;
using CloudQ.PatientQ.Model.RTOs;
using CloudQ.PatientQ.Model.ViewModels;
using CloudQ.PatientQ.Service.Interfaces;
using CloudQ.UitilityTools;

namespace CloudQ.PatientQ.Service.Services
{
    public class PrescriptionService : BaseService<Prescription>, IPrescriptionService
    {
        private IPatientQUnitOfWork _unitOfWork;
        public PrescriptionService(IPatientQUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public GridEntity<PrescriptionDto> GridDataSource(GridOptions options)
        {
            string query = "select * from pqm.Prescription";
            return Data<PrescriptionDto>.Grid.DataSource(options, query, "PrescribDate desc");
        }

        public List<PrescriptionDto> GetReportData()
        {
            string query = "select * from pqm.Prescription";
            return Data<PrescriptionDto>.DataSource(query);
        }
        public PrescriptionDto GetPresciptionById(Guid id)
        {
            string query = string.Format("select * from pqm.Prescription where Id='{0}'", id);
            return Data<PrescriptionDto>.SingleData(query);
        }

        public object SavePrescription(PrescriptionDto prescription)
        {
            Result result = new Result();
            try
            {


                var objPress = new Prescription();
                //    objPress.PrescriptionId = 0;
                //    objPress.Id = Guid.NewGuid();
                //    objPress.DoctorId = prescription.DoctorId;
                //    objPress.PatientId = prescription.PatientId;
                //    _unitOfWork.Repository<Prescription>().Add(objPress);
                //    _unitOfWork.Commit();

                //var obj = GetPresciptionById(objPress.Id);

                //foreach (var medi in pressMedicine)
                //{

                //}
                //pressMedicine.MedicineId=    prescription.PrescribeMedicines

                //_unitOfWork.Repository<PrescribeMedicine>().Add(objPress);

                Mapper.Map(prescription, objPress);
                objPress.PrescribDate = DateTime.Now;
                objPress.Id = Guid.NewGuid();
                //if (objPress.PrescribeMedicines != null)
                //{
                //    foreach (var medi in objPress.PrescribeMedicines)
                //    {
                //        medi.Id = Guid.NewGuid();
                //    }
                //}

               
              //  pMedicines.ForEach(s => s.PrescriptionId = Guid.NewGuid);


           
                

                base.Save(objPress);

                var pMedicines = new List<PrescribeMedicine>();
                Mapper.Map(prescription.PrescribeMedicines, pMedicines);
                pMedicines.ForEach(s => s.Id = Guid.NewGuid());
                pMedicines.ForEach(s => s.PrescriptionId = objPress.PrescriptionId);
               
                var pSympts= new List<PrescribeSymptom>();
                Mapper.Map(prescription.PrescribeSymptoms, pSympts);
                pSympts.ForEach(s => s.Id = Guid.NewGuid());
                pSympts.ForEach(s => s.PrescriptionId = objPress.PrescriptionId);


                _unitOfWork.BeginTransaction();

                foreach (var item in pMedicines)
                {
                    _unitOfWork.Repository<PrescribeMedicine>().Add(item);

                }
                foreach (var item in pSympts)
                {
                    _unitOfWork.Repository<PrescribeSymptom>().Add(item);

                }
                _unitOfWork.Commit();


                result.Status = true;
                result.Message = "Success";
                result.Data = objPress;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message;
            }

            return result;


        }

        public GridEntity<PrescriptionVm> GetPrescriptionByUser(GridOptions options, int userId)
        {
            return Data<PrescriptionVm>.Grid.GenericDataSource(options, string.Format(@"Select p.*,pm.MaxDays,pm.NumberOfDosage,pm.DosageId,d.DoctorName from pqm.Prescriptions p
left join pqm.PrescribeMedicines pm on pm.PrescriptionId=p.PrescriptionId
left join pqm.Medicines m on m.MedicineId=pm.MedicineId
left join pqm.Doctors d on d.DoctorID=p.DoctorId
where p.PatientId={0}", userId), "DoctorId");
        }
        

        public List<PrescriptionRto> GetPrescription(int prescriptionId)
        {
            return Data<PrescriptionRto>.DataSource(string.Format(@"Select p.*,pm.MaxDays,pm.NumberOfDosage,pm.DosageId from pqm.Prescriptions p
left join pqm.PrescribeMedicines pm on pm.PrescriptionId=p.PrescriptionId
left join pqm.Medicines m on m.MedicineId=pm.MedicineId
 where p.PrescriptionId={0}", prescriptionId));

        }

        public GridEntity<PrescriptionVm> GetPrescription(GridOptions options, int appointmentId)
        {
            return Data<PrescriptionVm>.Grid.GenericDataSource(options, string.Format(@"Select p.*,pm.MaxDays,pm.NumberOfDosage,pm.DosageId from pqm.Prescriptions p
left join pqm.PrescribeMedicines pm on pm.PrescriptionId=p.PrescriptionId
left join pqm.Medicines m on m.MedicineId=pm.MedicineId
 where AppointmentId={0}", appointmentId), "DoctorId");
        }
    }
}
